import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

// Получение параметров url
def paramMap = javax.faces.context.FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap()

activeSection = paramMap.get('code')
OrganizationParamId = paramMap.get('id')

class Section {
    public String name;
    public String link;
    public children;
    public String type;
    public String code;
    public boolean active;

    Section(String code, String name) {
        this.name = name
        this.code = code
        this.children = []
    }

    void setLink(String link) {
        this.link = link
    }

    void setActive(boolean active) {
        this.active = active
    }

}

String createLink( obj, section) {
    def type = section.fields.Type.value.fields.Code?.value

    String result
    switch (type) {
        case 'Link':
            result = obj.fields.Link?.value ? obj.fields.Link?.value : ''
            break;
        case 'Text':
            result = '?id=' + OrganizationParamId + "&code=" +  obj.id
            break;
        case 'File':
            result =  obj.fields.File?.value  ? '/platform/dmsfile/'+obj.fields.File?.value?.getAt(0)?.uid  : ''
            break;
        case 'List':
            result = ''
            break;
        default :
            result = ""
    }

    return result
}


Section traverseSection(Object obj, Object secObject, Section children = null) {

    def section = new Section(secObject.fields.Code.value, secObject.fields.Description.value)

    // Устанавливаем ссылку на контент раздела
    section.setLink(createLink(obj, secObject))


    // Если у секции есть потомки
    if (children) {
        section.children.push(children)
    } else {
        section.setActive(activeSection.equals(obj.id))
    }
    // Если есть родитель, то добавляем данный пункт к родителю и возвращаем,
    // если нет возраващем этот
    def parent = secObject.fields.ParentSection.value
    if (parent) {
        return traverseSection(obj, parent, section)
    }
    return section;
}

// Соеденяем с родителями, если есть такие
void createSection(Object obj, HashMap<String, Section> menu) {
    // Создаем отдельные секции
    def section = traverseSection(obj, obj.fields.Section.value)

    // Склеиваем их вместе
    def sectionMenu = menu.get(section.code) as Section
    if (sectionMenu) {
        for (children in section.children) {
            sectionMenu.children.add(children)
        }
    } else {
        menu.put(section.code, section)
    }
}

if (dashboardContext.context['tehprisRSO_PortalOrganization'] == null && (paramMap.get('id') != null)) {

    def textMap = [:]
    // Получение данных

    def portalObjectList = externalDataSource.getObject('tehprisRSO_CatalogueOrganizations', OrganizationParamId)

    if (portalObjectList) {

        // Контент микропортала
        def contentList = portalObjectList.fields.Contents.value

        // Интро
        dashboardContext.context['tehprisRSO_intro'] = portalObjectList.fields.Intro?.value

        // Древовидное меню: код раздела/ раздел
        def HashMap<String, Section> menu = new HashMap<String, Section>()

        // Построение меню: Проходимся по всем разделам контена
        // Заполняем навигационное меню
        for (content in contentList) {
            createSection(content, menu)

            if (activeSection.equals(content.id)) {
                dashboardContext.context['tehprisRSO_content'] = content
            }
        }

        Gson gson = new GsonBuilder().disableHtmlEscaping().create();



        dashboardContext.context['tehprisRSO_menu'] = gson.toJson(menu);
        dashboardContext.context['tehprisRSO_organization'] = portalObjectList.fields.Tenant.value
    }
}