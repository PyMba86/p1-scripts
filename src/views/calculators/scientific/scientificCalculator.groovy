import ru.osslabs.model.datasource.DataObject;
import ru.osslabs.model.datasource.DataObjectField;
import ru.osslabs.model.datasource.DataObjectList;
import java.util.LinkedHashMap;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Comparator;
import java.util.Locale;

/////////////////////////////////////////////////////////////////////////////////////////////////
// Калькулятор стоимости подключения электричество
/////////////////////////////////////////////////////////////////////////////////////////////////

//Муниципальные образования
public class Municipality {
    private String id;
    private String code;
    private String description;

    public String getId() {
        return id
    }

    public void setId(String id) {
        this.id = id
    }

    public String getCode() {
        return code
    }

    public void setCode(String code) {
        this.code = code
    }

    public String getDescription() {
        return description
    }

    public void setDescription(String description) {
        this.description = description
    }
}

// Насселенные пункты
public class Locality {
    private String id;
    private String code;
    private String description;
    private Municipality municipality;
    private String type;

    public String getId() {
        return id
    }

    public void setId(String id) {
        this.id = id
    }

    public String getCode() {
        return code
    }

    public void setCode(String code) {
        this.code = code
    }

    public String getDescription() {
        return description
    }

    public void setDescription(String description) {
        this.description = description
    }
}

// Тип деятельности
class TypeOfActivity {
    public String id;
    public String description;
}

public class TariffZone {

    public String id;

    public String description;

    public String message;

    public String policyType;

    public boolean levelSpecified;

    // Участие Ростехнадзора
    public String participation;

    public Organization organization;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}

// Организация
class Organization {
    public String id;
    public String description;
    public Locality locality;


    public String getDescription() {
        return description
    }

    public void setDescription(String description) {
        this.description = description
    }
}


class RateType {
    public String code;
    public String description;
    public Double min;
    public Double max;
    public String minSign;
    public String maxSign;

}

class Rate {
    public String id;
    public String code;
    public String description;
    public String objectType;
    public Double multiplier;
    public TariffZone tariffZone;
    public RateType type;
    public Double min;
    public Double max;
    public String minSign;
    public String maxSign;
    public Double powerCurrent;
    public Double powerYear;
    public Double standCurrent;
    public Double standYear;
    public Double change;

}


public class CostEvent {
    public String code;

    public String description;

    public String rateTypeCode;

    public String objectTypeCode;

    //Индексы изменения сметной стоимости
    public Double rateChange;

    //Стандартизированная ставка в ценах 2001
    public Double rateStandYear;

    //Стандартизированная ставка в текущих ценах
    public Double rateStandCurrent;

    //Ставка за единицу мощности в ценах 2001
    public Double ratePowerYear;

    //Ставка за единицу мощности в текущих ценах
    public Double ratePowerCurrent;

    public Double ratePower;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getRateTypeCode() {
        return rateTypeCode;
    }

    public void setRateTypeCode(String rateTypeCode) {
        this.rateTypeCode = rateTypeCode;
    }

    public String getObjectTypeCode() {
        return objectTypeCode;
    }

    public void setObjectTypeCode(String objectTypeCode) {
        this.objectTypeCode = objectTypeCode;
    }

    public Double getRateChange() {
        return rateChange;
    }

    public void setRateChange(Double rateChange) {
        this.rateChange = rateChange;
    }

    public Double getRateStandYear() {
        return rateStandYear;
    }

    public void setRateStandYear(Double rateStandYear) {
        this.rateStandYear = rateStandYear;
    }

    public Double getRateStandCurrent() {
        return rateStandCurrent;
    }

    public void setRateStandCurrent(Double rateStandCurrent) {
        this.rateStandCurrent = rateStandCurrent;
    }

    public Double getRatePowerYear() {
        return ratePowerYear;
    }

    public void setRatePowerYear(Double ratePowerYear) {
        this.ratePowerYear = ratePowerYear;
    }

    public Double getRatePowerCurrent() {
        return ratePowerCurrent;
    }

    public void setRatePowerCurrent(Double ratePowerCurrent) {
        this.ratePowerCurrent = ratePowerCurrent;
    }

    public Double getRatePower() {
        return ratePower;
    }

    public void setRatePower(Double ratePower) {
        this.ratePower = ratePower;
    }

}

public class LastMile {

    private String code;

    private String description;

    private String objectType;

    private List<LastMileRow> rows;

    private boolean checked;

    private boolean addRow;

    public LastMile(String code, String description, String objectType) {
        this.code = code;
        this.description = description;
        this.objectType = objectType;
        this.rows = new ArrayList<>();
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getObjectType() {
        return objectType;
    }

    public void setObjectType(String objectType) {
        this.objectType = objectType;
    }

    public List<LastMileRow> getRows() {
        return rows;
    }

    public void setRows(List<LastMileRow> rows) {
        this.rows = rows;
    }

    public boolean isChecked() {
        return checked;
    }

    public boolean getAddRow() {
        return addRow;
    }

    public void setAddRow(boolean checked) {
        this.addRow = checked
    }

    public void setChecked(boolean checked) {
        this.checked = checked;
    }

}

public class LastMileRow {

    private String code;

    private LinkedHashMap<String, CostEvent> costEventMap = new HashMap<>();

    private Double firstFactor = 0.0;

    private Double secondFactor = 0.0;

    private Double buildingCost = 0.0;

    private boolean dirty = false;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public LinkedHashMap<String, CostEvent> getCostEventMap() {
        return costEventMap;
    }

    public void setCostEventMap(LinkedHashMap<String, CostEvent> costEventMap) {
        this.costEventMap = costEventMap;
    }

    public Double getFirstFactor() {
        return firstFactor;
    }

    public void setFirstFactor(Double firstFactor) {
        this.firstFactor = CalcUtils.round(firstFactor, 2);
    }

    public Double getSecondFactor() {
        return secondFactor;
    }

    public void setSecondFactor(Double secondFactor) {
        this.secondFactor = CalcUtils.round(secondFactor, 2);
    }

    public Double getBuildingCost() {
        return buildingCost;
    }

    public void setBuildingCost(Double buildingCost) {
        this.buildingCost = CalcUtils.round(buildingCost, 2);
    }


    boolean getDirty() {
        return dirty
    }

    void setDirty(boolean dirty) {
        this.dirty = dirty
    }
}


public final class CalcUtils {
    private CalcUtils() {
    }

    public static final Comparator<CostEvent> COST_EVENT_COMPARATOR = new Comparator<CostEvent>() {
        @Override
        public int compare(final CostEvent o1, final CostEvent o2) {
            return o1.getDescription().compareToIgnoreCase(o2.getDescription());
        }
    };

    public static final Comparator<LastMile> LAST_MILE_COMPARATOR = new Comparator<LastMile>() {
        @Override
        public int compare(final LastMile o1, final LastMile o2) {
            return o1.getDescription().compareToIgnoreCase(o2.getDescription());
        }
    };

    public static double round(double value, int places) {
        if (places < 0) throw new IllegalArgumentException();

        BigDecimal bd = new BigDecimal(value);
        bd = bd.setScale(places, RoundingMode.HALF_UP);
        return bd.doubleValue();
    }

    public static String format(double value) {
        DecimalFormat df = (DecimalFormat) NumberFormat.getNumberInstance(Locale.FRANCE);
        df.applyPattern("###,##0.00");
        return df.format(value);
    }
}

/////////////////////////////////////////////////////////////////////////////////////////////////

// Обработчик по получению данных из DataObject
// Получает обьект, Заполняет новый обьект по определенным полям
abstract class DataHandler<T> {

    private final Callback<T> callback;
    private DataProvider dataProvider;
    private int total;
    private int filtered;
    private LinkedHashMap<String, DataObjectField> fields;
    private String id;

    DataHandler(Callback<T> callback) {
        this.callback = callback;
    }

    void fill(LinkedHashMap<String, DataObjectField> fields) {
        this.fields = fields;

        T object = data();

        if (object != null) {
            callback.data(object)
        } else {
            filtered++
        }
        total++;
    }

    protected abstract T data();

    protected DataObjectField get(String name) {
        return fields.get(name)
    }

    String getId() {
        return id
    }

    void setId(String id) {
        this.id = id
    }

    void setDataProvider(DataProvider dataProvider) {
        this.dataProvider = dataProvider
    }

    DataProvider getDataProvider() {
        return dataProvider
    }

    static interface Callback<T> {
        void data(T object)
    }
}

// Обработчик по организациям
class OrganizationDataHandler extends DataHandler<Organization> {

    private Locality locality

    OrganizationDataHandler(Callback<Organization> callback, Locality locality) {
        super(callback)
        this.locality = locality
    }

    boolean containsLocality(DataObjectList localityDataObjectList) {
        for (localityDataObject in localityDataObjectList) {
            if (locality.id.equals(localityDataObject.id)) return true;
        }
        return false;
    }

    Organization data() {
        // Принимает ActivityOfOrganization
        DataObject orgDataObject = get('Tenant').getValue()
        DataObjectList localityDataObject = get('Locality').getValue()
        if (orgDataObject && containsLocality(localityDataObject)) {
            Organization organization = new Organization()
            organization.id = orgDataObject.id;
            organization.description = orgDataObject.getFields()
                    .get('Description').getValue()
            organization.locality = locality
            return organization

        } else {
            return null
        }

    }
}

// Обработчик по населенным пунктам
class LocalityDataHandler extends DataHandler<Locality> {

    private Municipality municipality

    LocalityDataHandler(Callback<Locality> callback, Municipality municipality) {
        super(callback)
        this.municipality = municipality
    }

    Locality data() {
        Locality locality = new Locality()
        locality.id = id;
        locality.code = get('Code').getValue() != null ? get('Code').getValue() : ''
        locality.description = get('Description').getValue()
        locality.municipality = municipality
        DataObject typeDataObject = get('TypeOfMunicipalFormation').getValue()
        locality.type = typeDataObject != null ? typeDataObject.getFields().get('Code').getValue() : ''
        return locality
    }
}

// Обработчик по типом ставок
class RateTypeDataHandler extends DataHandler<RateType> {


    RateTypeDataHandler(Callback<RateType> callback) {
        super(callback)
    }

    RateType data() {
        RateType type = new RateType()
        type.code = get('Code').getValue() != null ? get('Code').getValue() : ''
        type.description = get('Description').getValue()
        type.min = convertStringToDouble(get('RateTypeRangeMin').getValue())
        type.max = convertStringToDouble(get('RateTypeRangeMax').getValue())

        DataObject typeMinSign = get('RateTypeRangeMinSign').getValue()
        type.minSign = typeMinSign != null ? typeMinSign.getFields().get('Code').getValue() : ''

        DataObject typeMaxSign = get('RateTypeRangeMaxSign').getValue()
        type.maxSign = typeMaxSign != null ? typeMaxSign.getFields().get('Code').getValue() : ''

        return type
    }

    Double convertStringToDouble(String value) {
        return (StringUtils.isNotBlank(value)
                && NumberUtils.isNumber(value)) ? Double.parseDouble(value) : null
    }
}

// Обработчик по ставкам платы
class RateDataHandler extends DataHandler<Rate> {

    TariffZone tariffZone;
    Double value;
    Double voltage;
    String method;


    RateDataHandler(Callback<Rate> callback, TariffZone tariffZone, Double value,
                    Double voltage, String method) {
        super(callback)
        this.tariffZone = tariffZone;
        this.value = value;
        this.voltage = voltage;
        this.method = method;
    }

    // Проверяем на принадлежность диапазону
    boolean inRange(Double firstValue, String sign, Double secondValue) {
        if (firstValue == null || secondValue == null) return true;

        switch (sign) {
            case "moreSrictly":
                return (firstValue > secondValue);
            case "moreEqual":
                return (firstValue >= secondValue);
            case "equalSrictly":
                final double epsilon = 0.001;
                return (Math.abs(firstValue - secondValue) < epsilon);
            case "lessSrictly":
                return (firstValue < secondValue);
            case "lessEqual":
                return (firstValue <= secondValue);
            default:
                return false;
        }
    }

    Rate data() {

        // Ставка платы
        Rate rate = new Rate()
        rate.id = id;
        rate.code = get('Code').getValue() != null ? get('Code').getValue() : ''
        rate.description = get('Description').getValue()
        rate.min = convertStringToDouble(get('RateRangeMin').getValue())
        rate.max = convertStringToDouble(get('RateRangeMax').getValue())

        DataObject minSign = get('RateRangeMinSign').getValue()
        rate.minSign = minSign != null ? minSign.getFields().get('Code').getValue() : ''

        DataObject maxSign = get('RateRangeMaxSign').getValue()
        rate.maxSign = maxSign != null ? maxSign.getFields().get('Code').getValue() : ''

        DataObject objectType = get('ObjectType').getValue()
        rate.objectType = objectType != null ? objectType.getFields().get('Code').getValue() : ''

        rate.tariffZone = tariffZone;

        rate.powerCurrent = convertStringToDouble(get('RatePowerCurrent').getValue())
        rate.change = convertStringToDouble(get('RateChange').getValue())
        rate.powerYear = convertStringToDouble(get('RatePowerYear').getValue())
        rate.standCurrent = convertStringToDouble(get('RateStandCurrent').getValue())
        rate.standYear = convertStringToDouble(get('RateStandYear').getValue())
        rate.multiplier = get('Multiplier').getValue()

        // Тип ставки
        DataObject rateType = get('RateType').getValue()
        if (rateType) {
            RateType type = new RateType()
            // Заполняем тип ставки
            dataProvider.getData(rateType,
                    new RateTypeDataHandler({ rateTypeObject -> type = rateTypeObject }))
            rate.type = type
        } else {
            return null
        }


        if (rate.type.code.contains("orgEvents") || method.equals("methodPower")
                && tariffZone.levelSpecified) {
            return (inRange(value, rate.minSign, rate.min)
                    && inRange(value, rate.maxSign, rate.max)) ? rate : null
        }
        return inRange(value, rate.maxSign, rate.max) ? rate : null
    }

    Double convertStringToDouble(String value) {
        return (StringUtils.isNotBlank(value)
                && NumberUtils.isNumber(value)) ? Double.parseDouble(value) : null
    }
}

// Обработчик по муниципальным организациям
class MunicipalityDataHandler extends DataHandler<Municipality> {

    MunicipalityDataHandler(Callback<Municipality> callback) {
        super(callback)
    }

    Municipality data() {
        Municipality municipality = new Municipality()
        municipality.id = id;
        municipality.code = get('Code').getValue() != null ? get('Code').getValue() : ''
        municipality.description = get('Description').getValue()
        return municipality
    }
}

// Обработчик по муниципальным организациям
class TariffZoneDataHandler extends DataHandler<TariffZone> {

    public Organization organization;

    TariffZoneDataHandler(Callback<TariffZone> callback, Organization organization) {
        super(callback)
        this.organization = organization;
    }

    TariffZone data() {
        TariffZone tariffZone = new TariffZone()
        tariffZone.id = id;
        tariffZone.description = Objects.toString(get('TarifSubZone').getValue(), "")
        tariffZone.message = Objects.toString(get('TarifSubZoneDescription').getValue(), "")
        tariffZone.levelSpecified = get('LevelSpecified').getValue()

        DataObject participation = get('RostechParticipation').getValue()
        tariffZone.participation = participation != null ? participation.getFields().get('Code').getValue() : ''

        DataObject policyType = get('ACCPolicyType').getValue()
        tariffZone.policyType = policyType != null ? policyType.getFields().get('Code').getValue() : ''

        tariffZone.organization = organization
        return tariffZone
    }
}

/////////////////////////////////////////////////////////////////////////////////////////////////

// Поставшик данных
class DataProvider {

    def externalDatasource;
    def integrationsDataProvider;

    void getData(DataObject dataObject, DataHandler handler) {
        // Передаем поставшика для того чтобы можно было получать данные из других сущностей
        handler.setDataProvider(this)
        // Добоавляем id записи
        handler.setId(dataObject.getId())
        // Вызывает кэлбэк после заполнения обьекта
        handler.fill(dataObject.getFields())
    }

    // Получаем данные из списка обьектов с обработкой их в хэндлере
    void getData(List<DataObject> dataObjectList, DataHandler handler) {
        for (DataObject dataObject : dataObjectList) {
            getData(dataObject, handler)
        }
    }

    // Получаем данные из сущности с обработкой их в хэнделере
    void getData(String entityName, DataHandler handler) {
        this.getData(this.externalDataSource.getObjectList(entityName, null)
                as List<DataObject>, handler)
    }

    void getData(LinkedHashMap<String, Object> entity, DataHandler handler) {
        // Получаем обьекты по фильтру
        List<DataObject> objectList = integrationsDataProvider
                .runSavedScript('tehprisRSO_getObjectEntity', [entity: entity])
        // Заполняем список
        this.getData(objectList, handler)
    }

    void setExternalDataSource(externalDatasource) {
        this.externalDatasource = externalDatasource
    }

    def getExternalDataSource() {
        return this.externalDatasource;
    }

    void setIntegrationsDataProvider(integrationsDataProvider) {
        this.integrationsDataProvider = integrationsDataProvider
    }

}

/////////////////////////////////////////////////////////////////////////////////////////////////

// Пользовательская команда
interface Command<T> {
    // Выполнить команду
    void execute(DataProvider dataProvider);

    // Проверить выполение команды
    boolean check(MessageHandler handler);

    // Очистить выполнение команды
    void clear();

    // Получить обьект команды
    T get();
}

// Обработчик сообщений
interface MessageHandler {
    // Вывести сообщение
    void print(String status)

    // Очистить сообщение
    void clear();
}

// Последовательно выполняет заданные команды
class CommandInvoker {
    private List<Command> commands;
    DataProvider dataProvider;
    HashMap dashboardContext;
    boolean valid = true;
    private final MessageHandler handler;

    CommandInvoker(List<Command> commands, DataProvider dataProvider, HashMap dashboardContext,
                   MessageHandler messageHandler) {
        this.commands = commands
        this.dataProvider = dataProvider
        this.dashboardContext = dashboardContext
        this.handler = messageHandler;

    }

    void execute(boolean checked) {
        for (Command command : commands) {

            if (valid) {
                handler.clear();
                command.execute(dataProvider);
                if (checked && !command.check(handler)) {
                    valid = false;
                }
            } else {
                command.clear();
            }
        }
    }
}

// Команда выбора из списка
abstract class MenuDataCommand<T> implements Command<T> {

    final String selectItem;
    final String selectItems;
    HashMap dashboardContext;


    MenuDataCommand(String selectItem, String selectItems, HashMap dashboardContext) {
        this.selectItem = selectItem
        this.selectItems = selectItems
        this.dashboardContext = dashboardContext
    }

    void clear() {
        dashboardContext[selectItems] = null
        dashboardContext[selectItem] = null
    }

    T get() {
        def items = dashboardContext[selectItems]
        String id = dashboardContext[selectItem];
        return items.get(id)
    }

    public boolean isNotValue(Object value) {
        if (value == null) {
            return true
        } else if (value instanceof HashMap) {
            return value.isEmpty()
        } else if (value instanceof String) {
            return value.isEmpty()
        }
        return false
    }
}

// Команда ввода данных
abstract class InputDataCommand implements Command<String> {
    final String item;
    HashMap dashboardContext;

    InputDataCommand(String item, HashMap dashboardContext) {
        this.item = item
        this.dashboardContext = dashboardContext
    }

    void execute(DataProvider dataProvider) {
        // нет реализации
    }

    void clear() {
        dashboardContext[item] = null
    }

    String get() {
        return dashboardContext[item];
    }
}

/////////////////////////////////////////////////////////////////////////////////////////////////

// Получить муниципальные образования
class MunicipalityCommand extends MenuDataCommand<Municipality> {


    MunicipalityCommand(String selectItem, String selectItems, HashMap dashboardContext) {
        super(selectItem, selectItems, dashboardContext)
        dashboardContext[selectItems] = new LinkedHashMap<>();
    }

    void execute(DataProvider dataProvider) {
        dashboardContext[selectItems] = getMunicipalities(dataProvider)

    }

    private LinkedHashMap<String, Municipality> getMunicipalities(DataProvider dataProvider) {
        final LinkedHashMap<String, Municipality> municipalities = new LinkedHashMap<>();
        dataProvider.getData("tehprisRSO_Municipalities", new MunicipalityDataHandler(
                { municipality -> municipalities.put(municipality.id, municipality) }));
        return municipalities;
    }

    boolean check(MessageHandler handler) {
        if (isNotValue(dashboardContext[selectItems])) {
            handler.print("Муниципальные образования не найдены");
            return false;
        }
        if (isNotValue(dashboardContext[selectItem])) {
            handler.print("Выберите муниципальное образование");
            return false;
        }
        return true;
    }
}

// Получить населенные пункты
class LocalityCommand extends MenuDataCommand<Locality> {

    Command<Municipality> municipalityCommand;

    LocalityCommand(String selectItem, String selectItems, Command<Municipality> municipalityCommand, HashMap dashboardContext) {
        super(selectItem, selectItems, dashboardContext)
        this.municipalityCommand = municipalityCommand;
        dashboardContext[selectItems] = new LinkedHashMap<>();
    }

    void execute(DataProvider dataProvider) {
        if (municipalityCommand.get() != null) {
            dashboardContext[selectItems] = getLocality(dataProvider, municipalityCommand.get())
        } else {
            dashboardContext[selectItems] = null
        }

    }

    LinkedHashMap<String, Locality> getLocality(DataProvider dataProvider, Municipality municipality) {
        final LinkedHashMap<String, Locality> localities = new LinkedHashMap<>();

        // Создаем описание для фильтра по муниципалитету
        LinkedHashMap<String, Object> entityLocality = [
                link: 'tehprisRSO_Locality',
                data: [
                        Municipality: [link: 'tehprisRSO_Municipalities',
                                       data: [Id: municipality.id]]
                ]]

        // Получаем обьекты по фильтру с помощью сторонего скрипта
        List<DataObject> dataLocalityObjectList = dataProvider.integrationsDataProvider
                .runSavedScript('tehprisRSO_getObjectEntity', [entity: entityLocality])

        // Заполняем список населенных пунктов
        dataProvider.getData(dataLocalityObjectList,
                new LocalityDataHandler(
                        { locality -> localities.put(locality.id, locality) }, municipality))
        return localities;
    }


    boolean check(MessageHandler handler) {
        if (isNotValue(dashboardContext[selectItems])) {
            handler.print("Населенные пункты не найдены");
            return false;
        }
        if (isNotValue(dashboardContext[selectItem])) {
            handler.print("Выберите населенный пункт");
            return false;
        } else {

            def localities = dashboardContext[selectItems]
            def selectLocality = dashboardContext[selectItem]
            if (!localities.containsKey(selectLocality)) {
                return false;
            }
        }
        return true;
    }
}

// Получить организации по теплоснабжению
class OrganizationCommand extends MenuDataCommand<Organization> {

    Command<Locality> localityCommand;

    OrganizationCommand(String selectItem, String selectItems, Command<Locality> localityCommand, HashMap dashboardContext) {
        super(selectItem, selectItems, dashboardContext)
        this.localityCommand = localityCommand;
        dashboardContext[selectItems] = new LinkedHashMap<>();
    }

    void execute(DataProvider dataProvider) {
        if (localityCommand.get() != null) {
            dashboardContext[selectItems] = getOrganization(dataProvider, localityCommand.get())
        } else {
            dashboardContext[selectItems] = null
        }

    }

    LinkedHashMap<String, Organization> getOrganization(DataProvider dataProvider, Locality locality) {
        final LinkedHashMap<String, Organization> organizations = new LinkedHashMap<>();

        // Создаем описание для фильтра по муниципалитету
        LinkedHashMap<String, Object> activityOfOrganizationEntity = [
                link: 'tehprisRSO_ActivityOfOrganization',
                data: [
                        TypeOfActivity:
                                [link: 'tehprisRSO_TypeOfActivity',
                                 data: [Code: 'ES']]
                ]]

        // Получаем обьекты по фильтру с помощью сторонего скрипта
        List<DataObject> dataOrganizationObjectList = dataProvider.integrationsDataProvider
                .runSavedScript('tehprisRSO_getObjectEntity', [entity: activityOfOrganizationEntity])

        // Заполняем список населенных пунктов
        dataProvider.getData(dataOrganizationObjectList,
                new OrganizationDataHandler(
                        { organization -> organizations.put(organization.tenantId, organization) }, locality))
        return organizations;
    }


    boolean check(MessageHandler handler) {
        if (isNotValue(dashboardContext[selectItems])) {
            handler.print("Организации не найдены");
            return false;
        }
        if (isNotValue(dashboardContext[selectItem])) {
            handler.print("Выберите организацию");
            return false;
        }
        return true;
    }
}

// Получить тарифные зоны по организации
class TarifZonesCommand extends MenuDataCommand<Organization> {

    Command<Organization> organizationCommand;

    TarifZonesCommand(String selectItem, String selectItems, Command<Organization> organizationCommand, HashMap dashboardContext) {
        super(selectItem, selectItems, dashboardContext)
        this.organizationCommand = organizationCommand;
        dashboardContext[selectItems] = new LinkedHashMap<>();
    }

    void execute(DataProvider dataProvider) {
        if (localityCommand.get() != null) {
            dashboardContext[selectItems] = getTariffZones(dataProvider, organizationCommand.get())
        } else {
            dashboardContext[selectItems] = null
        }

    }

    LinkedHashMap<String, TariffZone> getTariffZones(DataProvider dataProvider, Organization organization) {
        final LinkedHashMap<String, TariffZone> tariffZones = new LinkedHashMap<>();

        // Создаем описание для фильтра по муниципалитету
        LinkedHashMap<String, Object> tariffZonesEntity = [
                link: 'tehprisRSO_TariffZonesScientificCalculator',
                data: [
                        Tenant:
                                [link: 'Tenant',
                                 data: [Id: organization.id]]
                ]]

        // Получаем обьекты по фильтру с помощью сторонего скрипта
        List<DataObject> dataTariffZonesEntityObjectList = integrationsDataProvider
                .runSavedScript('tehprisRSO_getObjectEntity', [entity: tariffZonesEntity])

        // Заполняем список населенных пунктов
        dataProvider.getData(dataTariffZonesEntityObjectList,
                new TariffZoneDataHandler(
                        { tariffZone -> tariffZones.put(tariffZone.id, tariffZone) }, organization))
        return tariffZones;
    }

    boolean check(MessageHandler handler) {
        if (isNotValue(dashboardContext[selectItems])) {
            handler.print("Тарифные зоны для данной организации не найдены");
            return false;
        }
        if (isNotValue(dashboardContext[selectItem])) {
            handler.print("Выберите тарифную зону");
            return false;
        }
        return true;
    }
}

// Получить тепловую нагрузку
class MaxCapacityCommand extends InputDataCommand {

    MaxCapacityCommand(String item, HashMap dashboardContext) {
        super(item, dashboardContext)
    }

    boolean check(MessageHandler handler) {
        if (!dashboardContext[this.item]) {
            handler.print("Введите значение максимальной присоединяемой мощности");
            return false;
        }

        if (convertToDouble(dashboardContext[this.item]) < 0) {
            handler.print("Значение максимальной присоединяемой мощности не может быть отрицательным");
            return false;
        }

        return true
    }

    Double convertToDouble(Object value) {
        return Double.parseDouble(value)
    }
}

// Получить уровень напряжения
class VoltageCommand extends InputDataCommand {

    VoltageCommand(String item, HashMap dashboardContext) {
        super(item, dashboardContext)
    }

    boolean check(MessageHandler handler) {
        if (!dashboardContext[this.item]) {
            handler.print("Выберите уровень напряжения");
            return false;
        }
        return true
    }

}

// Категория надежности
class ReliabilityCommand extends InputDataCommand {

    ReliabilityCommand(String item, HashMap dashboardContext) {
        super(item, dashboardContext)
    }

    boolean check(MessageHandler handler) {
        if (!dashboardContext[this.item]) {
            handler.print("Выберите категорию надежности");
            return false;
        }

        return true
    }

}

// Способ расчета
class MethodCommand extends InputDataCommand {

    MethodCommand(String item, HashMap dashboardContext) {
        super(item, dashboardContext)
    }

    boolean check(MessageHandler handler) {
        if (!dashboardContext[this.item]) {
            handler.print("Выберите способ расчета");
            return false;
        }

        return true
    }

}

// Необходимо строительство
class ConnectionCommand extends InputDataCommand {

    ConnectionCommand(String item, HashMap dashboardContext) {
        super(item, dashboardContext)
    }

    boolean check(MessageHandler handler) {
        if (!dashboardContext[this.item]) {
            handler.print("Необходимо строительство ?");
            return false;
        }

        return true
    }

}

// Рассчитать
class CalculateCommand implements Command<List<String>> {
    HashMap dashboardContext
    OrganizationCommand organizationCommand;
    WayOfLayingCommand wayOfLayingCommand;
    CapacityCommand capacityCommand
    DiameterCommand diameterCommand;

    final String summary;
    final String summaryVAT;
    final String summaryWithVAT;

    CalculateCommand(String summary, String summaryVAT, String summaryWithVAT, HashMap dashboardContext,
                     OrganizationCommand organizationCommand, WayOfLayingCommand wayOfLayingCommand,
                     CapacityCommand capacityCommand, DiameterCommand diameterCommand) {
        this.dashboardContext = dashboardContext
        this.organizationCommand = organizationCommand
        this.wayOfLayingCommand = wayOfLayingCommand
        this.diameterCommand = diameterCommand
        this.capacityCommand = capacityCommand

        this.summary = summary
        this.summaryVAT = summaryVAT
        this.summaryWithVAT = summaryWithVAT
    }

    void execute(DataProvider dataProvider) {

        boolean ndsOrganization = false


        if (organizationCommand.get()) {
            def uSubjectName = FilterGroup.single('Tenant', FilterElement.FilterOperator.equals,
                    [organizationCommand.get().tenantId])
            def org = dataProvider.externalDataSource.getObjectList("tehprisRSO_Organization", null,
                    uSubjectName, null, null, null, null).getAt(0)
            if (org) {
                ndsOrganization = org.value.fields.NDS ? org.value.fields.NDS.value : false
            }

        }

        List<Rate> rates = new ArrayList<>();

        // Получаем ставки платы
        dataProvider.getData([
                link: 'tehprisRSO_CalcRateTSScientificCalculator',
                data: [
                        Tenant:
                                [link: 'Tenant',
                                 data: [Id: organizationCommand.get().tenantId]]
                ]],
                new RateDataHandler(
                        { rate -> rates.add(rate) }, wayOfLayingCommand.get(),
                        diameterCommand.get(), capacityCommand.get()))



        def price = 0
        def nds = 0
        def result = 0
        if (rates.size() > 0 && rates.getAt(0) != null) {
            org.primefaces.context.RequestContext.getCurrentInstance().execute("console.log('" + rates + "');");
            if (ndsOrganization) {
                price = rates.getAt(0).powerCurrent
                nds = price - price / 1.18
                result = price / 1.18

                dashboardContext[summary] = round(result, 2)
                dashboardContext[summaryVAT] = round(nds, 2)
                dashboardContext[summaryWithVAT] = round(price, 2)

            } else {
                price = rates.getAt(0).powerCurrent
                nds = price * 0.18
                result = price + nds

                dashboardContext[summary] = round(price, 2)
                dashboardContext[summaryVAT] = round(nds, 2)
                dashboardContext[summaryWithVAT] = round(result, 2)
            }
        } else {
            clear();
        }

        // Получаем политику
        if (StringUtils.isEmpty(tariffZone.policyType)) return;

        Double value = 0;
        // Получаем введенное значение в  зависимости от политики
        if (tariffZone.policyType.equals("policyPower")) {
            value = maxCapacity;
        } else if (tariffZone.policyType.equals("policyVoltage")) {
            value = voltage;
        }

        // Получить мощность ставки платы по тарифной зоне
        dataProvider.getData([
                link: 'tehprisRSO_CalcRateScientificCalculator',
                data: [
                        TariffZone:
                                [link: 'tehprisRSO_TariffZonesScientificCalculator',
                                 data: [Id: tariffZone.id]]
                ]],
                new RateDataHandler(
                        { rate -> rates.add(rate) },
                        tariffZone, value, voltage, method))

        // Сортируем мероприятия по типу события
        for (Rate rate in rates) {
            if (rate.type.code.contains("orgEvents")) {
                ratesEvents.add(rate)
            } else {
                if ("methodPower".equals(method)) {
                    if (rate.powerCurrent != null || rate.powerYear != null) {
                        ratesEventsOthers.add(rate)
                    }
                } else if ("methodStand".equals(method)) {
                    if (rate.standCurrent != null || rate.standYear != null) {
                        ratesEventsOthers.add(rate)
                    }
                }
            }
        }

        // Заполняем события
        for (Rate rate : ratesEvents) {
            CostEvent costEvent = createCostEvent(rate);
            if (!findCostEvent(costEvent.getCode())) {
                costEvents.add(costEvent);
            }
        }

        if (lastMiles == null || lastMiles.isEmpty()) {
            // Если новое подключение
            if ("connectNew".equals(connection)) {
                lastMiles = fetchLastMiles();
                costEventsOthers = fetchCostEventsOthers();
            } else {
                lastMiles = new ArrayList<>();
                costEventsOthers = new ArrayList<>();
            }
        }



        for (def lastMile in lastMiles) {

            if (lastMile.isChecked()) {

                if (lastMile.getAddRow()) {
                    addRow(lastMile)
                }

                lastMile.setAddRow(false)

                if (lastMile.getRows().isEmpty()) {
                    addRow(lastMile)
                } else {
                    // Проверяем на удаленные обьекты, если они "грязные" то удаляем
                    def rows =
                            lastMile.getRows()
                                    .stream()
                                    .filter { row -> row.getDirty() == false }
                                    .collect()

                    // Обновляем список
                    lastMile.setRows(rows)

                    if (lastMile.getRows().isEmpty()) {
                        lastMile.setChecked(false);
                    } else {
                        for (def row in lastMile.getRows()) {

                            def costEvent = row.getCostEventMap().get(row.getCode());
                            if (costEvent) {
                                row.setFirstFactor(costEvent.getRatePower());
                                if (!"methodStand".equals(method) || !"objectLine".equals(costEvent.getObjectTypeCode())) {
                                    row.setSecondFactor(maxCapacity);
                                }
                                calculateBuildCost(row);
                            }
                        }
                    }
                }
            } else {
                lastMile.getRows().clear()
            }
        }


        calculate();

        costEventsResult = round(costEventsResult);
        costEventsResultVAT = round(costEventsResult * 0.18);
        costEventsResultWithVAT = round(costEventsResult + costEventsResultVAT);

        costEventsLastMileResult = round(costEventsLastMileResult);
        costEventsLastMileResultVAT = round(costEventsLastMileResult * 0.18);
        costEventsLastMileResultWIthVAT = round(costEventsLastMileResult + costEventsLastMileResultVAT);

        dashboardContext[summary] = round(costEventsResult + costEventsLastMileResult);
        summaryVAT = round(costEventsResultVAT + costEventsLastMileResultVAT);
        summaryWithVAT = round(costEventsResultWithVAT + costEventsLastMileResultWIthVAT);

    }

    boolean check(MessageHandler handler) {
        if (isNotValue(dashboardContext[summary])) {
            handler.print("Ставки платы для данной организации не найдены");
            return false;
        }
        return true;
    }


    void clear() {
        dashboardContext[summary] = null
        dashboardContext[summaryVAT] = null
        dashboardContext[summaryWithVAT] = null
    }

    List<String> get() {
        return null
    }

    double round(double value, int places) {
        BigDecimal bd = new BigDecimal(value);
        bd = bd.setScale(places, RoundingMode.HALF_UP);
        return bd.doubleValue();
    }

    boolean isNotValue(Double value) {
        if (value == null) {
            return true
        }
        return false
    }
}

/////////////////////////////////////////////////////////////////////////////////////////////////
// Переменные



costEvents = new ArrayList<>();

costEventsOthers = new ArrayList<>();

lastMiles = null;

costEventsResult = 0.0;

costEventsResultVAT = 0.0;

costEventsResultWithVAT = 0.0;

costEventsLastMileResult = 0.0;

costEventsLastMileResultVAT = 0.0;

costEventsLastMileResultWIthVAT = 0.0;

rates = new ArrayList<>();
ratesEvents = new ArrayList<>();
ratesEventsOthers = new ArrayList<>();


static def filterLastMile(def lastMile) {
    def result = [];
    for (def mile : lastMile) {
        if (mile.isChecked()) {
            result.add(mile);
        }
    }
    return result;
}

boolean findCostEvent(String code) {
    for(def event: costEvents) {
        if(event.getCode().equals(code)) {
            return true;
        }
    }
    return false;
}


private void calculateBuildCost(def lastMileRow) {
    def costEvent = lastMileRow.getCostEventMap().get(lastMileRow.getCode());

    Double buildingCost = 0.0;
    if ("methodStand".equals(method) && "objectLine".equals(costEvent.getObjectTypeCode())) {
        buildingCost = lastMileRow.getFirstFactor() * (lastMileRow.getSecondFactor() / 1000);
    } else {
        buildingCost = lastMileRow.getFirstFactor() * lastMileRow.getSecondFactor();
    }

    lastMileRow.setBuildingCost(buildingCost);
}

void addRow(def lastMile) {
    LastMileRow lastMileRow = new LastMileRow();
    lastMileRow.setCostEventMap(miracle(costEventsOthers, lastMile.getCode()));

    lastMile.getRows().add(lastMileRow);
}

void deleteRow(LastMile lastMile, LastMileRow lastMileRow) {
    lastMile.getRows().remove(lastMileRow);

    if (lastMile.getRows().isEmpty()) {
        lastMile.setChecked(false);
    }
}

LinkedHashMap<String, CostEvent> miracle(List<CostEvent> costEvents, String code) {
    LinkedHashMap<String, CostEvent> result = new LinkedHashMap<>();

    for (def costEvent : costEvents) {
        if (Objects.equals(costEvent.rateTypeCode, code)) {
            produceRatePower(costEvent);
            result.put(costEvent.code, costEvent);
        }
    }

    return result;
}

double round(double value) {
    return CalcUtils.round(value, 2);
}


CostEvent createCostEvent(Rate rate) {
    CostEvent costEvent = new CostEvent()
    costEvent.code = rate.code
    costEvent.description = rate.description
    costEvent.rateTypeCode = rate.type.code
    costEvent.objectTypeCode = rate.objectType;
    costEvent.rateChange = rate.change
    costEvent.rateStandYear = rate.standYear != null ? rate.standYear * rate.multiplier : null
    costEvent.rateStandCurrent = rate.standCurrent != null ? rate.standCurrent * rate.multiplier : null
    costEvent.ratePowerYear = rate.powerYear != null ? rate.powerYear * rate.multiplier : null
    costEvent.ratePowerCurrent = rate.powerCurrent != null ? rate.powerCurrent * rate.multiplier : null
    return costEvent;
}

List<CostEvent> fetchCostEventsOthers() {
    List<CostEvent> result = new ArrayList<>();

    for (Rate rate : ratesEventsOthers) {
        CostEvent costEvent = createCostEvent(rate);

        result.add(costEvent);
    }

    return result;
}

List<LastMile> fetchLastMiles() {
    Set<LastMile> result = new HashSet<>();

    for (Rate rate : ratesEventsOthers) {
        result.add(new LastMile(rate.type.code, rate.type.description, rate.objectType));
    }

    List<LastMile> lastMiles = new ArrayList<>(result);
    return lastMiles;
}

// Вычесление по формулам
void calculate() {
    if ("byCondition".equals(tariffZone.participation)) {
        if (reliability == 3) {
            if (maxCapacity > 670) {
                costEventsResult = secondFormula();
            } else if (maxCapacity <= 150) {
                costEventsResult = firstFormula();
            } else if (maxCapacity > 150 && maxCapacity <= 670) {
                if (voltage > 10) {
                    costEventsResult = secondFormula();
                } else if (voltage <= 10) {
                    costEventsResult = firstFormula();
                }
            }
        } else {
            costEventsResult = secondFormula();
        }
    } else if ("participates".equals(tariffZone.participation)) {
        costEventsResult = secondFormula();
    } else {
        costEventsResult = firstFormula();
    }

    costEventsLastMileResult = 0.0;
    for (def lastMile : lastMiles) {
        for (def lastMileRow : lastMile.getRows()) {
            if (!lastMileRow.getDirty()) {
                costEventsLastMileResult += lastMileRow.getBuildingCost();
            }
        }
    }
}


Double firstFormula() {
    Double result = 0.0d;

    for (def costEvent : costEvents) {
        if (costEvent.code.contains("inspectDevices"))
            continue;

        produceRatePower(costEvent);
        result += costEvent.ratePower;
    }

    return result * maxCapacity;
}

Double secondFormula() {
    Double result = 0.0d;
    for (def costEvent : costEvents) {
        produceRatePower(costEvent);
        result += costEvent.ratePower;
    }

    return result * maxCapacity;
}

void produceRatePower(def costEvent) {
    Double ratePower = 0.0;
    Double firstFactor = 0.0;
    Double secondFactor = 0.0;
    if ("methodPower".equals(method)) {
        if (costEvent.ratePowerCurrent != null) {
            ratePower = costEvent.ratePowerCurrent;
        } else {
            firstFactor = costEvent.ratePowerYear != null ? costEvent.ratePowerYear : 0.0;
            secondFactor = costEvent.rateChange != null ? costEvent.rateChange : 0.0;

            ratePower = firstFactor * secondFactor;
        }
    } else if ("methodStand".equals(method)) {
        if (costEvent.rateStandCurrent != null) {
            ratePower = costEvent.rateStandCurrent
        } else {
            firstFactor = costEvent.rateStandYear != null ? costEvent.rateStandYear : 0.0;
            secondFactor = costEvent.rateChange != null ? costEvent.rateChange : 0.0;

            ratePower = firstFactor * secondFactor;
        }
    }

    costEvent.ratePower = ratePower
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////

// Создаем поставшика и добавляем источник данных
DataProvider dataProvider = new DataProvider()
dataProvider.setExternalDataSource(externalDataSource)
dataProvider.setIntegrationsDataProvider(integrationsDataProvider)




// Пробуем расчитать
if (dashboardContext.context['tehprisRSO_ELCalc_selectConnection'] != null) {


    rates = dashboardContext.context['rtehprisRSO_ELCalc_rates'] != null ?
            dashboardContext.context['tehprisRSO_ELCalc_rates'] : new ArrayList<>()

    ratesEvents = dashboardContext.context['tehprisRSO_ELCalc_ratesEvents'] != null ?
            dashboardContext.context['tehprisRSO_ELCalc_ratesEvents'] : new ArrayList<>()

    ratesEventsOthers = dashboardContext.context['tehprisRSO_ELCalc_ratesEventsOthers'] != null ?
            dashboardContext.context['tehprisRSO_ELCalc_ratesEventsOthers'] : new ArrayList<>()

    costEvents = dashboardContext.context['tehprisRSO_ELCalc_costEvents'] != null ?
            dashboardContext.context['tehprisRSO_ELCalc_costEvents'] : new ArrayList<>()

    costEventsOthers = dashboardContext.context['tehprisRSO_ELCalc_costEventsOthers'] != null ?
            dashboardContext.context['tehprisRSO_ELCalc_costEventsOthers'] : new ArrayList<>()

    lastMiles = dashboardContext.context['tehprisRSO_ELCalc_lastMiles'] != null ?
            dashboardContext.context['tehprisRSO_ELCalc_lastMiles'] : null


    reInit(dataProvider)


    dashboardContext.context['tehprisRSO_ELCalc_costEventsResult'] = costEventsResult
    dashboardContext.context['tehprisRSO_ELCalc_costEventsResultVAT'] = costEventsResultVAT
    dashboardContext.context['tehprisRSO_ELCalc_costEventsResultWithVAT'] = costEventsResultWithVAT

    dashboardContext.context['tehprisRSO_ELCalc_costEventsLastMileResult'] = costEventsLastMileResult
    dashboardContext.context['tehprisRSO_ELCalc_costEventsLastMileResultVAT'] = costEventsLastMileResultVAT
    dashboardContext.context['tehprisRSO_ELCalc_costEventsLastMileResultWIthVAT'] = costEventsLastMileResultWIthVAT

    dashboardContext.context['tehprisRSO_ELCalc_costEvents'] = costEvents
    dashboardContext.context['tehprisRSO_ELCalc_costEventsOthers'] = costEventsOthers
    dashboardContext.context['tehprisRSO_ELCalc_lastMiles'] = lastMiles
}