package ru.osslabs.portaltpext.portlet.calculator.scientific.controller;

import ru.osslabs.portaltpext.controller.portlet.theme.RegionProvider;
import ru.osslabs.portaltpext.portlet.calculator.CalcUtils;
import ru.osslabs.portaltpext.portlet.calculator.scientific.data.NewScientificCalculatorDataProvider;
import ru.osslabs.portaltpext.portlet.calculator.scientific.model.*;

import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.Serializable;
import java.util.*;

@Named("sciCalc")
@ViewScoped
public class ScientificCalculator implements Serializable {

    @Inject
    private NewScientificCalculatorDataProvider calculatorDataProvider;

    @Inject
    private RegionProvider regionProvider;

    private String regionId;
    private Map<String, String> regions;

    private String organizationId;
    private Map<String, Organization> organizations;
    private Organization currentOrganization;

    private String tariffZoneId;
    private Map<String, TariffZone> tariffZones;
    private TariffZone currentTariffZone;

    private Double maxCapacity;

    private Double voltage;

    private Integer reliability;

    private String method;

    private String connection;

    private List<CostEvent> costEvents;

    private List<CostEvent> costEventsOthers;

    private List<LastMile> lastMiles;

    private Double costEventsResult = 0.0;

    private Double costEventsResultVAT = 0.0;

    private Double costEventsResultWithVAT = 0.0;

    private Double costEventsLastMileResult = 0.0;

    private Double costEventsLastMileResultVAT = 0.0;

    private Double costEventsLastMileResultWIthVAT = 0.0;

    @PostConstruct
    public void init() {
       regions = calculatorDataProvider.fetchRegions();

        //Getting region from portal aka Theme Portlet
        if (regions.containsKey(regionProvider.getRegionId())) {
            regionId = regionProvider.getRegionId();
            changeRegionAction();
        }
    }

    private void reInit() {
        if (hasAllParameters()) {
            clearResults();

            calculatorDataProvider.initCalculationData(tariffZoneId, currentOrganization.isLevelSpecified(), maxCapacity, voltage, method);

            costEvents = calculatorDataProvider.fetchCostEvents();

            if ("connectNew".equals(connection)) {
                lastMiles = calculatorDataProvider.fetchLastMiles();
                costEventsOthers = calculatorDataProvider.fetchCostEventsOthers();
            } else {
                lastMiles = new ArrayList<>();
                costEventsOthers = new ArrayList<>();
            }

            calculate();
        }
    }

    public void changeRegionAction() {
        organizations = calculatorDataProvider.fetchOrganizations(regionId);

        organizationId = null;
        tariffZoneId = null;
        currentOrganization = null;
        maxCapacity = null;
        voltage = null;
        reliability = null;
        method = null;
        connection = null;
    }

    public void changeOrganizationAction() {
        tariffZoneId = null;
        maxCapacity = null;
        voltage = null;
        reliability = null;
        method = null;
        connection = null;

        currentOrganization = organizations.get(organizationId);
        tariffZones = calculatorDataProvider.fetchTariffZones(regionId, organizationId);

        if (isTariffZonesSizeSingle()) {
            tariffZoneId = tariffZones.keySet().iterator().next();
            //make auto step instead of user
            changeTariffZoneAction();
        }
    }

    public void changeTariffZoneAction() {
        maxCapacity = null;
        voltage = null;
        reliability = null;
        method = null;
        connection = null;

        currentTariffZone = tariffZones.get(tariffZoneId);
        reInit();
    }

    public void changeMaxCapacityAction() {
        reInit();
    }

    public void changeVoltageAction() {
        reInit();
    }

    public void changeReliabilityAction() {
        reInit();
    }

    public void changeConnectionAction() {
        reInit();
    }

    public void changeLastMileAction(LastMile lastMile) {
        if (lastMile.isChecked()) {
            addRow(lastMile);
        } else {
            lastMile.getRows().clear();
            calculate();
        }
    }

    public void changeLastMileRowAction(LastMileRow lastMileRow) {
        CostEvent costEvent = lastMileRow.getCostEventMap().get(lastMileRow.getCode());
        lastMileRow.setFirstFactor(costEvent.getRatePower());
//        lastMileRow.setSecondFactor(maxCapacity);
        if (!"methodStand".equals(method) || !"objectLine".equals(costEvent.getObjectTypeCode())) {
            lastMileRow.setSecondFactor(maxCapacity);
        }

        calculateBuildCost(lastMileRow);
        calculate();
    }

    private void calculateBuildCost(LastMileRow lastMileRow) {
        CostEvent costEvent = lastMileRow.getCostEventMap().get(lastMileRow.getCode());

        Double buildingCost = 0.0;
        if ("methodStand".equals(method) && "objectLine".equals(costEvent.getObjectTypeCode())) {
            buildingCost = lastMileRow.getFirstFactor() * (lastMileRow.getSecondFactor() / 1000);
        } else {
            buildingCost = lastMileRow.getFirstFactor() * lastMileRow.getSecondFactor();
        }

        lastMileRow.setBuildingCost(buildingCost);
    }

    public void addRow(LastMile lastMile) {
        LastMileRow lastMileRow = new LastMileRow();
        lastMileRow.setCostEventMap(miracle(costEventsOthers, lastMile.getCode()));

        lastMile.getRows().add(lastMileRow);
    }

    public void deleteRow(LastMile lastMile, LastMileRow lastMileRow) {
        lastMile.getRows().remove(lastMileRow);

        if (lastMile.getRows().isEmpty()) {
            lastMile.setChecked(false);
        }
    }

    private Map<String, CostEvent> miracle(List<CostEvent> costEvents, String code) {
        Collections.sort(costEvents, CalcUtils.COST_EVENT_COMPARATOR);
        Map<String, CostEvent> result = new LinkedHashMap<>();

        for (CostEvent costEvent : costEvents) {
            if (Objects.equals(costEvent.getRateTypeCode(), code)) {
                produceRatePower(costEvent);
                result.put(costEvent.getCode(), costEvent);
            }
        }

        return result;
    }

    private void produceRatePower(CostEvent costEvent) {
        Double ratePower = 0.0;
        Double firstFactor = 0.0;
        Double secondFactor = 0.0;
        if ("methodPower".equals(method)) {
            if (costEvent.getRatePowerCurrent() != null) {
                ratePower = costEvent.getRatePowerCurrent();
            } else {
                firstFactor = costEvent.getRatePowerYear() != null ? costEvent.getRatePowerYear() : 0.0;
                secondFactor = costEvent.getRateChange() != null ? costEvent.getRateChange() : 0.0;

                ratePower = firstFactor * secondFactor;
            }
        } else if ("methodStand".equals(method)) {
            if (costEvent.getRateStandCurrent() != null) {
                ratePower = costEvent.getRateStandCurrent();
            } else {
                firstFactor = costEvent.getRateStandYear() != null ? costEvent.getRateStandYear() : 0.0;
                secondFactor = costEvent.getRateChange() != null ? costEvent.getRateChange() : 0.0;

                ratePower = firstFactor * secondFactor;
            }
        }

        costEvent.setRatePower(ratePower);
    }

    public void calculate() {
        if ("byCondition".equals(currentOrganization.getParticipation())) {
            if (reliability == 3) {
                if (maxCapacity > 670) {
                    costEventsResult = secondFormula();
                } else if (maxCapacity <= 150) {
                    costEventsResult = firstFormula();
                } else if (maxCapacity > 150 && maxCapacity <= 670) {
                    if (voltage > 10) {
                        costEventsResult = secondFormula();
                    } else if (voltage <= 10) {
                        costEventsResult = firstFormula();
                    }
                }
            } else {
                costEventsResult = secondFormula();
            }
        } else if ("participates".equals(currentOrganization.getParticipation())) {
            costEventsResult = secondFormula();
        } else {
            costEventsResult = firstFormula();
        }

        costEventsLastMileResult = 0.0;
        for (LastMile lastMile : lastMiles) {
            for (LastMileRow lastMileRow : lastMile.getRows()) {
                costEventsLastMileResult += lastMileRow.getBuildingCost();
            }
        }
    }

    private Double firstFormula() {
        Double result = 0.0d;

        for (CostEvent costEvent : costEvents) {
            if (costEvent.getCode().contains("inspectDevices"))
                continue;

            produceRatePower(costEvent);
            result += costEvent.getRatePower();
        }

        return result * maxCapacity;
    }

    private Double secondFormula() {
        Double result = 0.0d;

        for (CostEvent costEvent : costEvents) {
            produceRatePower(costEvent);
            result += costEvent.getRatePower();
        }

        return result * maxCapacity;
    }

    private boolean hasAllParameters() {
        return (regionId != null)
                && (organizationId != null)
                && (maxCapacity != null)
                && (voltage != null)
                && (reliability != null)
                && (method != null)
                && (connection != null);
    }

    private void clearResults() {
        costEventsResult = 0.0;
        costEventsResultVAT = 0.0;
        costEventsResultWithVAT = 0.0;
        costEventsLastMileResult = 0.0;
        costEventsLastMileResultVAT = 0.0;
        costEventsLastMileResultWIthVAT = 0.0;
    }

    public static String formatter(double value) {
        return CalcUtils.format(value);
    }

    public static double round(double value) {
        return CalcUtils.round(value, 2);
    }

    public String getRegionId() {
        return regionId;
    }

    public void setRegionId(String regionId) {
        this.regionId = regionId;
    }

    public Map<String, String> getRegions() {
        return regions;
    }

    public String getOrganizationId() {
        return organizationId;
    }

    public void setOrganizationId(String organizationId) {
        this.organizationId = organizationId;
    }

    public Map<String, Organization> getOrganizations() {
        return organizations;
    }

    public Organization getCurrentOrganization() {
        return currentOrganization;
    }

    public String getTariffZoneId() {
        return tariffZoneId;
    }

    public void setTariffZoneId(String tariffZoneId) {
        this.tariffZoneId = tariffZoneId;
    }

    public Map<String, TariffZone> getTariffZones() {
        return tariffZones;
    }

    public TariffZone getCurrentTariffZone() {
        return currentTariffZone;
    }

    public boolean isTariffZonesSizeSingle() {
        return tariffZones.size() == 1;
    }

    public Double getMaxCapacity() {
        return maxCapacity;
    }

    public void setMaxCapacity(Double maxCapacity) {
        this.maxCapacity = maxCapacity;
    }

    public Double getVoltage() {
        return voltage;
    }

    public void setVoltage(Double voltage) {
        this.voltage = voltage;
    }

    public Integer getReliability() {
        return reliability;
    }

    public void setReliability(Integer reliability) {
        this.reliability = reliability;
    }

    public String getMethod() {
        return method;
    }

    public void setMethod(String method) {
        this.method = method;
    }

    public String getConnection() {
        return connection;
    }

    public void setConnection(String connection) {
        this.connection = connection;
    }

    public List<CostEvent> getCostEvents() {
        return costEvents;
    }

    public List<LastMile> getLastMiles() {
        return lastMiles;
    }

    public Double getCostEventsResult() {
        costEventsResult = round(costEventsResult);
        return costEventsResult;
    }

    public Double getCostEventsResultVAT() {
        costEventsResultVAT = round(costEventsResult * 0.18);

        return costEventsResultVAT;
    }

    public Double getCostEventsResultWithVAT() {
        costEventsResultWithVAT = round(costEventsResult + costEventsResultVAT);

        return costEventsResultWithVAT;
    }

    public Double getCostEventsLastMileResult() {
        costEventsLastMileResult = round(costEventsLastMileResult);
        return costEventsLastMileResult;
    }

    public Double getCostEventsLastMileResultVAT() {
        costEventsLastMileResultVAT = round(costEventsLastMileResult * 0.18);

        return costEventsLastMileResultVAT;
    }

    public Double getCostEventsLastMileResultWIthVAT() {
        costEventsLastMileResultWIthVAT = round(costEventsLastMileResult + costEventsLastMileResultVAT);

        return costEventsLastMileResultWIthVAT;
    }

    public Double getSummary() {
        return round(costEventsResult + costEventsLastMileResult);
    }

    public Double getSummaryVAT() {
        return round(costEventsResultVAT + costEventsLastMileResultVAT);
    }

    public Double getSummaryWithVAT() {
        return round(costEventsResultWithVAT + costEventsLastMileResultWIthVAT);
    }
}
