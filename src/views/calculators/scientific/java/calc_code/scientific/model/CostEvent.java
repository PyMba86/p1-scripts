package ru.osslabs.portaltpext.portlet.calculator.scientific.model;

public class CostEvent {
    private String code;

    private String description;

    private String rateTypeCode;

    private String objectTypeCode;

    //Индексы изменения сметной стоимости
    private Double rateChange;

    //Стандартизированная ставка в ценах 2001
    private Double rateStandYear;

    //Стандартизированная ставка в текущих ценах
    private Double rateStandCurrent;

    //Ставка за единицу мощности в ценах 2001
    private Double ratePowerYear;

    //Ставка за единицу мощности в текущих ценах
    private Double ratePowerCurrent;

    private Double ratePower;

    public CostEvent(String code, String description, String rateTypeCode, String objectTypeCode, Double rateChange, Double rateStandYear, Double rateStandCurrent, Double ratePowerYear, Double ratePowerCurrent) {
        this.code = code;
        this.description = description;
        this.rateTypeCode = rateTypeCode;
        this.objectTypeCode = objectTypeCode;
        this.rateChange = rateChange;
        this.rateStandYear = rateStandYear;
        this.rateStandCurrent = rateStandCurrent;
        this.ratePowerYear = ratePowerYear;
        this.ratePowerCurrent = ratePowerCurrent;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getRateTypeCode() {
        return rateTypeCode;
    }

    public void setRateTypeCode(String rateTypeCode) {
        this.rateTypeCode = rateTypeCode;
    }

    public String getObjectTypeCode() {
        return objectTypeCode;
    }

    public void setObjectTypeCode(String objectTypeCode) {
        this.objectTypeCode = objectTypeCode;
    }

    public Double getRateChange() {
        return rateChange;
    }

    public void setRateChange(Double rateChange) {
        this.rateChange = rateChange;
    }

    public Double getRateStandYear() {
        return rateStandYear;
    }

    public void setRateStandYear(Double rateStandYear) {
        this.rateStandYear = rateStandYear;
    }

    public Double getRateStandCurrent() {
        return rateStandCurrent;
    }

    public void setRateStandCurrent(Double rateStandCurrent) {
        this.rateStandCurrent = rateStandCurrent;
    }

    public Double getRatePowerYear() {
        return ratePowerYear;
    }

    public void setRatePowerYear(Double ratePowerYear) {
        this.ratePowerYear = ratePowerYear;
    }

    public Double getRatePowerCurrent() {
        return ratePowerCurrent;
    }

    public void setRatePowerCurrent(Double ratePowerCurrent) {
        this.ratePowerCurrent = ratePowerCurrent;
    }

    public Double getRatePower() {
        return ratePower;
    }

    public void setRatePower(Double ratePower) {
        this.ratePower = ratePower;
    }
}
