package ru.osslabs.portaltpext.portlet.calculator.scientific.model;

import java.util.ArrayList;
import java.util.List;

public class LastMile {

    private String code;

    private String description;

    private String objectType;

    private List<LastMileRow> rows;

    private boolean checked;

    public LastMile(String code, String description, String objectType) {
        this.code = code;
        this.description = description;
        this.objectType = objectType;
        this.rows = new ArrayList<>();
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getObjectType() {
        return objectType;
    }

    public void setObjectType(String objectType) {
        this.objectType = objectType;
    }

    public List<LastMileRow> getRows() {
        return rows;
    }

    public void setRows(List<LastMileRow> rows) {
        this.rows = rows;
    }

    public boolean isChecked() {
        return checked;
    }

    public void setChecked(boolean checked) {
        this.checked = checked;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        LastMile lastMile = (LastMile) o;

        return !(code != null ? !code.equals(lastMile.code) : lastMile.code != null);

    }

    @Override
    public int hashCode() {
        return code != null ? code.hashCode() : 0;
    }
}
