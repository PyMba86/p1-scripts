package ru.osslabs.portaltpext.portlet.calculator.scientific.model;

import ru.osslabs.portaltpext.portlet.calculator.scientific.controller.ScientificCalculator;

import java.util.Map;

public class LastMileRow {

    private String code;

    private Map<String, CostEvent> costEventMap;

    private Double firstFactor = 0.0;

    private Double secondFactor = 0.0;

    private Double buildingCost = 0.0;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Map<String, CostEvent> getCostEventMap() {
        return costEventMap;
    }

    public void setCostEventMap(Map<String, CostEvent> costEventMap) {
        this.costEventMap = costEventMap;
    }

    public Double getFirstFactor() {
        return firstFactor;
    }

    public void setFirstFactor(Double firstFactor) {
        this.firstFactor = ScientificCalculator.round(firstFactor);
    }

    public Double getSecondFactor() {
        return secondFactor;
    }

    public void setSecondFactor(Double secondFactor) {
        this.secondFactor = ScientificCalculator.round(secondFactor);
    }

    public Double getBuildingCost() {
        return buildingCost;
    }

    public void setBuildingCost(Double buildingCost) {
        this.buildingCost = ScientificCalculator.round(buildingCost);
    }
}
