package ru.osslabs.portaltpext.portlet.calculator.scientific.model;

public class TariffZone {

    private String id;

    private String description;

    private String message;

    public TariffZone(String id, String description, String message) {
        this.id = id;
        this.description = description;
        this.message = message;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
