package ru.osslabs.portaltpext.portlet.calculator.scientific.model;

public class Organization {

    private String id;

    private String description;

    private String message;

    private String participation;

    private boolean levelSpecified;

    public Organization(String id, String description, String message, String participation, boolean levelSpecified) {
        this.id = id;
        this.description = description;
        this.message = message;
        this.participation = participation;
        this.levelSpecified = levelSpecified;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getParticipation() {
        return participation;
    }

    public void setParticipation(String participation) {
        this.participation = participation;
    }

    public boolean isLevelSpecified() {
        return levelSpecified;
    }

    public void setLevelSpecified(boolean levelSpecified) {
        this.levelSpecified = levelSpecified;
    }
}
