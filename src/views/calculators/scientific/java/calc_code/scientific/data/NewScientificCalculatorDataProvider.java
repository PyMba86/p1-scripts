package ru.osslabs.portaltpext.portlet.calculator.scientific.data;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import ru.osslabs.model.datasource.DataObject;
import ru.osslabs.portaltpext.data.CMDataProvider;
import ru.osslabs.portaltpext.portlet.calculator.CalcUtils;
import ru.osslabs.portaltpext.portlet.calculator.scientific.model.CostEvent;
import ru.osslabs.portaltpext.portlet.calculator.scientific.model.LastMile;
import ru.osslabs.portaltpext.portlet.calculator.scientific.model.Organization;
import ru.osslabs.portaltpext.portlet.calculator.scientific.model.TariffZone;

import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.Serializable;
import java.util.*;

import static ru.osslabs.portaltpext.controller.portlet.calculator.CalculatorsConstants.CLASS_TYPE_REGIONS;
import static ru.osslabs.portaltpext.controller.portlet.calculator.CalculatorsConstants.FIELD_DESCRIPTION;

@Named
@ViewScoped
public class NewScientificCalculatorDataProvider implements Serializable {

    @Inject
    private CMDataProvider dataProvider;

    private List<DataObject> rateRangeRegion;

    private List<DataObject> rateRangeRegionPower;

    private List<DataObject> rateRangeRegionPowerVoltage;

    private List<DataObject> rateRangeRegionPowerVoltageEvents;

    private List<DataObject> rateRangeRegionPowerVoltageEventsOthers;

    @PostConstruct
    public void init() {
        rateRangeRegion = new ArrayList<>();
        rateRangeRegionPower = new ArrayList<>();
        rateRangeRegionPowerVoltage = new ArrayList<>();
        rateRangeRegionPowerVoltageEvents = new ArrayList<>();
        rateRangeRegionPowerVoltageEventsOthers = new ArrayList<>();
    }

    public Map<String, String> fetchRegions() {
        Map<String, String> result = new LinkedHashMap<>();

        for (DataObject dataObject : dataProvider.getClasses().get(CLASS_TYPE_REGIONS)) {
            List<DataObject> objects = dataProvider.getRegion_tarifzone().get(dataObject.getId());
            if (!objects.isEmpty()) {
                String regionName = dataObject.getFields().get(FIELD_DESCRIPTION).getValue().toString();
                result.put(dataObject.getId(), regionName);
            }
        }

        return result;
    }

    public Map<String, Organization> fetchOrganizations(String regionId) {
        Map<String, Organization> result = new LinkedHashMap<>();

        List<DataObject> dataObjects = dataProvider.getRegion_tarifzone().get(regionId);
        for (DataObject dataObject : dataObjects) {
            DataObject organization = (DataObject) dataObject.getFields().get("organization").getValue();
            if (organization == null) continue;

            String code = organization.getId();
            String name = Objects.toString(organization.getFields().get(FIELD_DESCRIPTION).getValue(), "");

            String message = Objects.toString(dataObject.getFields().get("userMessage").getValue(), "");
            DataObject participationObject = ((DataObject) dataObject.getFields().get("rostechParticipation").getValue());
            String participation = participationObject != null ? Objects.toString(participationObject.getFields().get("oldCode").getValue(), "") : "";
            boolean levelSpecified = Boolean.valueOf(Objects.toString(dataObject.getFields().get("levelSpecified").getValue(), "false"));

            result.put(code, new Organization(code, name, message, participation, levelSpecified));
        }

        return result;
    }

    public Map<String, TariffZone> fetchTariffZones(String regionId, String organizationId) {
        Map<String, TariffZone> result = new LinkedHashMap<>();

        List<DataObject> dataObjects = dataProvider.getRegion_tarifzone().get(regionId);
        for (DataObject dataObject : dataObjects) {
            DataObject organization = (DataObject) dataObject.getFields().get("organization").getValue();
            if (organization == null || !Objects.equals(organization.getId(), organizationId)) continue;

            String code = dataObject.getId();
            String name = Objects.toString(dataObject.getFields().get("tarifSubZone").getValue(), "");
            String message = Objects.toString(dataObject.getFields().get("tarifSubZoneDescription").getValue(), "");

            result.put(code, new TariffZone(code, name, message));
        }

        return result;
    }

    public void initCalculationData(String tariffZoneId, boolean levelSpecified, Double maxCapacity, Double voltage, String method) {
        rateRangeRegion = fetchRateRangeRegion(tariffZoneId);

        String accPolicy = fetchAccPolicy();
        if (StringUtils.isEmpty(accPolicy)) return;

        if ("policyPower".equals(accPolicy)) {
            rateRangeRegionPower = fetchRateRangeRegionPower(maxCapacity);
        } else if ("policyVoltage".equals(accPolicy)) {
            rateRangeRegionPower = fetchRateRangeRegionPower(voltage);
        }

        rateRangeRegionPowerVoltage = fetchRateRangeRegionPowerVoltage(voltage, method, levelSpecified);

        splitRateRangeRegionPowerVoltage();

        rateRangeRegionPowerVoltageEventsOthers = fetchRateRangeRegionPowerVoltageEventsOthers(method);
    }

    private List<DataObject> fetchRateRangeRegion(String organizationId) {
        return dataProvider.fetchJDBCData(organizationId, "calcRate", "tariffZone");
    }

    private String fetchAccPolicy() {
        DataObject regionDataObject = null;
        if (!rateRangeRegion.isEmpty()) {
            String tariffZoneId = dataProvider.retrieveValue(rateRangeRegion.get(0).getFields().get("tariffZone"));
            regionDataObject = dataProvider.fetchJDBCSingleData("tariffZones", new Long(tariffZoneId));
        }
        DataObject accPolicyDataObject = null;
        if (regionDataObject != null) {
            String accPolicyId = dataProvider.retrieveValue(regionDataObject.getFields().get("accPolicy"));
            accPolicyDataObject = dataProvider.getCachedLookup("accPolicyType", accPolicyId);
        }
        return (accPolicyDataObject != null)
                ? Objects.toString(accPolicyDataObject.getFields().get("oldCode").getValue(), null)
                : null;
    }

    private List<DataObject> fetchRateRangeRegionPower(Double value) {
        List<DataObject> result = new ArrayList<>();

        for (DataObject dataObject : rateRangeRegion) {
            String rateRangeMin = Objects.toString(dataObject.getFields().get("rateRangeMin").getValue(), "");
            String rateRangeMax = Objects.toString(dataObject.getFields().get("rateRangeMax").getValue(), "");

            Double rateRangeMinDouble = StringUtils.isNotBlank(rateRangeMin) && NumberUtils.isNumber(rateRangeMin) ? Double.parseDouble(rateRangeMin) : null;
            String rateRangeMinSignId = dataProvider.retrieveValue(dataObject.getFields().get("rateRangeMinSign"));
            DataObject sign = dataProvider.getCachedLookup("rangeMinSign", rateRangeMinSignId);
            String rateRangeMinSign = sign != null ? sign.getFields().get("oldCode").getValue().toString() : "";

            Double rateRangeMaxDouble = StringUtils.isNotBlank(rateRangeMax) && NumberUtils.isNumber(rateRangeMax) ? Double.parseDouble(rateRangeMax) : null;
            String rateRangeMaxSignId = dataProvider.retrieveValue(dataObject.getFields().get("rateRangeMaxSign"));
            sign = dataProvider.getCachedLookup("rangeMaxSign", rateRangeMaxSignId);
            String rateRangeMaxSign = sign != null ? sign.getFields().get("oldCode").getValue().toString() : "";

            if (inRange(value, rateRangeMinSign, rateRangeMinDouble) && inRange(value, rateRangeMaxSign, rateRangeMaxDouble)) {
                result.add(dataObject);
            }
        }

        return result;
    }

    private List<DataObject> fetchRateRangeRegionPowerVoltage(Double voltage, String method, boolean levelSpecified) {
        List<DataObject> result = new ArrayList<>();

        for (DataObject dataObject : rateRangeRegionPower) {
            String rateTypeId = dataProvider.retrieveValue(dataObject.getFields().get("rateType"));
            DataObject rateType = dataProvider.fetchJDBCSingleData("RateType", new Long(rateTypeId));
            if (rateType != null) {
                String rateRangeMin = Objects.toString(rateType.getFields().get("rateTypeRangeMin").getValue(), "");
                String rateRangeMax = Objects.toString(rateType.getFields().get("rateTypeRangeMax").getValue(), "");

                Double rateRangeMinDouble = StringUtils.isNotBlank(rateRangeMin) && NumberUtils.isNumber(rateRangeMin) ? Double.parseDouble(rateRangeMin) : null;
                String rateTypeRangeMinSignId = dataProvider.retrieveValue(rateType.getFields().get("rateTypeRangeMinSign"));
                DataObject sign = dataProvider.getCachedLookup("rangeMinSign", rateTypeRangeMinSignId);
                String rateRangeMinSign = sign != null ? sign.getFields().get("oldCode").getValue().toString() : "";

                Double rateRangeMaxDouble = StringUtils.isNotBlank(rateRangeMax) && NumberUtils.isNumber(rateRangeMax) ? Double.parseDouble(rateRangeMax) : null;
                String rateTypeRangeMaxSignId = dataProvider.retrieveValue(rateType.getFields().get("rateTypeRangeMaxSign"));
                sign = dataProvider.getCachedLookup("rangeMaxSign", rateTypeRangeMaxSignId);
                String rateRangeMaxSign = sign != null ? sign.getFields().get("oldCode").getValue().toString() : "";

                String code = Objects.toString(rateType.getFields().get("Code").getValue(), "");
                if ((code != null && code.contains("orgEvents")) || ("methodPower".equals(method) && levelSpecified)) {
                    if (inRange(voltage, rateRangeMinSign, rateRangeMinDouble) && inRange(voltage, rateRangeMaxSign, rateRangeMaxDouble)) {
                        result.add(dataObject);
                    }
                } else if (inRange(voltage, rateRangeMaxSign, rateRangeMaxDouble)) {
                    result.add(dataObject);
                }
            }
        }

        return result;
    }

    private void splitRateRangeRegionPowerVoltage() {
        rateRangeRegionPowerVoltageEvents.clear();
        rateRangeRegionPowerVoltageEventsOthers.clear();

        for (DataObject dataObject : rateRangeRegionPowerVoltage) {
            String rateTypeId = dataProvider.retrieveValue(dataObject.getFields().get("rateType"));
            DataObject rateType = dataProvider.fetchJDBCSingleData("RateType", new Long(rateTypeId));
            if (rateType != null) {
                String code = Objects.toString(rateType.getFields().get("Code").getValue(), "");
                if (code != null && code.contains("orgEvents")) {
                    rateRangeRegionPowerVoltageEvents.add(dataObject);
                } else {
                    rateRangeRegionPowerVoltageEventsOthers.add(dataObject);
                }
            }
        }
    }

    private List<DataObject> fetchRateRangeRegionPowerVoltageEventsOthers(String method) {
        List<DataObject> result = new ArrayList<>();

        String firstField = "";
        String secondField = "";
        if ("methodPower".equals(method)) {
            firstField = "ratePowerCurrent";
            secondField = "ratePowerYear";
        } else if ("methodStand".equals(method)) {
            firstField = "rateStandCurrent";
            secondField = "rateStandYear";
        }

        for(DataObject dataObject : rateRangeRegionPowerVoltageEventsOthers){
            if (dataObject.getFields().get(firstField).getValue() != null || dataObject.getFields().get(secondField).getValue() != null) {
                result.add(dataObject);
            }
        }

        return result;
    }

    public List<LastMile> fetchLastMiles() {
        Set<LastMile> result = new HashSet<>();

        for (DataObject dataObject : rateRangeRegionPowerVoltageEventsOthers) {
            String rateTypeId = dataProvider.retrieveValue(dataObject.getFields().get("rateType"));
            DataObject rateType = dataProvider.fetchJDBCSingleData("RateType", new Long(rateTypeId));
            if (rateType != null) {
                String code = Objects.toString(rateType.getFields().get("Code").getValue(), "");
                String description = Objects.toString(rateType.getFields().get("Description").getValue(), "");

                String objectTypeId = dataProvider.retrieveValue(dataObject.getFields().get("objectType"));
                DataObject objectType = dataProvider.getCachedLookup("calcObjectType", objectTypeId);
                String objectTypeCode = objectType != null ? Objects.toString(objectType.getFields().get("oldCode").getValue(), "") : "";

                result.add(new LastMile(code, description, objectTypeCode));
            }
        }

        List<LastMile> lastMiles = new ArrayList<>(result);
        Collections.sort(lastMiles, CalcUtils.LAST_MILE_COMPARATOR);
        return lastMiles;
    }

    public List<CostEvent> fetchCostEvents() {
        List<CostEvent> result = new ArrayList<>();

        for (DataObject dataObject : rateRangeRegionPowerVoltageEvents) {
            CostEvent costEvent = createCostEvent(dataObject);

            result.add(costEvent);
        }

        return result;
    }

    public List<CostEvent> fetchCostEventsOthers() {
        List<CostEvent> result = new ArrayList<>();

        for (DataObject dataObject : rateRangeRegionPowerVoltageEventsOthers) {
            CostEvent costEvent = createCostEvent(dataObject);

            result.add(costEvent);
        }

        return result;
    }

    private CostEvent createCostEvent(DataObject dataObject) {
        String rateTypeId = dataProvider.retrieveValue(dataObject.getFields().get("rateType"));
        DataObject rateType = dataProvider.fetchJDBCSingleData("RateType", new Long(rateTypeId));
        String rateTypeCode = rateType != null ? Objects.toString(rateType.getFields().get("Code").getValue(), "") : "";

        String objectTypeId = dataProvider.retrieveValue(dataObject.getFields().get("objectType"));
        DataObject objectType = dataProvider.getCachedLookup("calcObjectType", objectTypeId);
        String objectTypeCode = objectType != null ? Objects.toString(objectType.getFields().get("oldCode").getValue(), "") : "";

        String code = Objects.toString(dataObject.getFields().get("Code").getValue(), "");
        String description = Objects.toString(dataObject.getFields().get("Description").getValue(), "");

        String rateChange = Objects.toString(dataObject.getFields().get("rateChange").getValue(), "");
        String rateStandYear = Objects.toString(dataObject.getFields().get("rateStandYear").getValue(), "");
        String rateStandCurrent = Objects.toString(dataObject.getFields().get("rateStandCurrent").getValue(), "");
        String ratePowerYear = Objects.toString(dataObject.getFields().get("ratePowerYear").getValue(), "");
        String ratePowerCurrent = Objects.toString(dataObject.getFields().get("ratePowerCurrent").getValue(), "");

        Double rateChangeDouble = StringUtils.isNotBlank(rateChange) && NumberUtils.isNumber(rateChange) ? Double.parseDouble(rateChange) : null;
        Double rateStandYearDouble = StringUtils.isNotBlank(rateStandYear) && NumberUtils.isNumber(rateStandYear) ? Double.parseDouble(rateStandYear) : null;
        Double rateStandCurrentDouble = StringUtils.isNotBlank(rateStandCurrent) && NumberUtils.isNumber(rateStandCurrent) ? Double.parseDouble(rateStandCurrent) : null;
        Double ratePowerYearDouble = StringUtils.isNotBlank(ratePowerYear) && NumberUtils.isNumber(ratePowerYear) ? Double.parseDouble(ratePowerYear) : null;
        Double ratePowerCurrentDouble = StringUtils.isNotBlank(ratePowerCurrent) && NumberUtils.isNumber(ratePowerCurrent) ? Double.parseDouble(ratePowerCurrent) : null;

        if (dataObject.getFields().get("multiplier").getValue() != null) {
            String multiplier = Objects.toString(dataObject.getFields().get("multiplier").getValue(), "");
            Double multiplierDouble = StringUtils.isNotBlank(multiplier) && NumberUtils.isNumber(multiplier) ? Double.parseDouble(multiplier) : 1.0;

            rateStandYearDouble = rateStandYearDouble != null ? rateStandYearDouble * multiplierDouble : null;
            rateStandCurrentDouble = rateStandCurrentDouble != null ? rateStandCurrentDouble * multiplierDouble : null;
            ratePowerYearDouble = ratePowerYearDouble != null ? ratePowerYearDouble * multiplierDouble : null;
            ratePowerCurrentDouble = ratePowerCurrentDouble != null ? ratePowerCurrentDouble * multiplierDouble : null;
        }

        return new CostEvent(code, description, rateTypeCode, objectTypeCode, rateChangeDouble, rateStandYearDouble, rateStandCurrentDouble, ratePowerYearDouble, ratePowerCurrentDouble);
    }

    private boolean inRange(Double firstValue, String sign, Double secondValue) {
        if (firstValue == null || secondValue == null) return true;

        switch (sign) {
            case "moreSrictly":
                return (firstValue > secondValue);
            case "moreEqual":
                return (firstValue >= secondValue);
            case "equalSrictly":
                final double epsilon = 0.001;
                return (Math.abs(firstValue - secondValue) < epsilon);
            case "lessSrictly":
                return (firstValue < secondValue);
            case "lessEqual":
                return (firstValue <= secondValue);
            default:
                return false;
        }
    }

}
