package ru.osslabs.portaltpext.portlet.calculator;

import ru.osslabs.portaltpext.portlet.calculator.scientific.model.CostEvent;
import ru.osslabs.portaltpext.portlet.calculator.scientific.model.LastMile;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Comparator;
import java.util.Locale;

public final class CalcUtils {
    private CalcUtils() {
    }

    public static final Comparator<CostEvent> COST_EVENT_COMPARATOR = new Comparator<CostEvent>() {
        @Override
        public int compare(final CostEvent o1, final CostEvent o2) {
            return o1.getDescription().compareToIgnoreCase(o2.getDescription());
        }
    };

    public static final Comparator<LastMile> LAST_MILE_COMPARATOR = new Comparator<LastMile>() {
        @Override
        public int compare(final LastMile o1, final LastMile o2) {
            return o1.getDescription().compareToIgnoreCase(o2.getDescription());
        }
    };

    public static double round(double value, int places) {
        if (places < 0) throw new IllegalArgumentException();

        BigDecimal bd = new BigDecimal(value);
        bd = bd.setScale(places, RoundingMode.HALF_UP);
        return bd.doubleValue();
    }

    public static String format(double value) {
        DecimalFormat df = (DecimalFormat) NumberFormat.getNumberInstance(Locale.FRANCE);
        df.applyPattern("###,##0.00");
        return df.format(value);
    }
}
