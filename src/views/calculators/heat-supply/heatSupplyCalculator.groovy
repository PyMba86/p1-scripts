import ru.osslabs.model.datasource.DataObject;
import ru.osslabs.model.datasource.DataObjectField;
import ru.osslabs.model.datasource.DataObjectList;
import java.util.LinkedHashMap;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import java.math.BigDecimal;
import java.math.RoundingMode;
import ru.osslabs.model.common.filter.FilterGroup
import ru.osslabs.model.common.filter.FilterElement


/////////////////////////////////////////////////////////////////////////////////////////////////
// Калькулятор стоимости подключения теплоснабжение
/////////////////////////////////////////////////////////////////////////////////////////////////

//Муниципальные образования
public class Municipality {
    private String id;
    private String code;
    private String description;

    public String getId() {
        return id
    }

    public void setId(String id) {
        this.id = id
    }

    public String getCode() {
        return code
    }

    public void setCode(String code) {
        this.code = code
    }

    public String getDescription() {
        return description
    }

    public void setDescription(String description) {
        this.description = description
    }
}

// Насселенные пункты
public class Locality {
    private String id;
    private String code;
    private String description;
    private Municipality municipality;
    private String type;

    public String getId() {
        return id
    }

    public void setId(String id) {
        this.id = id
    }

    public String getCode() {
        return code
    }

    public void setCode(String code) {
        this.code = code
    }

    public String getDescription() {
        return description
    }

    public void setDescription(String description) {
        this.description = description
    }
}

// Способ прокладки
public class WayOfLaying {
    private String id;
    private String code;
    private String description;

    public String getId() {
        return id
    }

    public void setId(String id) {
        this.id = id
    }

    public String getCode() {
        return code
    }

    public void setCode(String code) {
        this.code = code
    }

    public String getDescription() {
        return description
    }

    public void setDescription(String description) {
        this.description = description
    }
}

// Диаметр
public class Diameter {
    private String id;
    private String code;
    private String description;

    public String getId() {
        return id
    }

    public void setId(String id) {
        this.id = id
    }

    public String getCode() {
        return code
    }

    public void setCode(String code) {
        this.code = code
    }

    public String getDescription() {
        return description
    }

    public void setDescription(String description) {
        this.description = description
    }
}

// Ставка платы
class Rate {
    public String id;
    public String code;
    public String description;
    public Double min;
    public Double max;
    public String minSign;
    public String maxSign;
    public Double powerCurrent;
    public Double standCurrent;
}

// Тип деятельности
class TypeOfActivity {
    public String id;
    public String description;
}

// Организация
class Organization {
    public String id;
    public String description;
    public Locality locality;
    public String tenantId;
    public boolean nds;


    public String getDescription() {
        return description
    }

    public void setDescription(String description) {
        this.description = description
    }
}

/////////////////////////////////////////////////////////////////////////////////////////////////

// Обработчик по получению данных из DataObject
// Получает обьект, Заполняет новый обьект по определенным полям
abstract class DataHandler<T> {

    private final Callback<T> callback;
    private DataProvider dataProvider;
    private int total;
    private int filtered;
    private LinkedHashMap<String, DataObjectField> fields;
    private String id;

    DataHandler(Callback<T> callback) {
        this.callback = callback;
    }

    void fill(LinkedHashMap<String, DataObjectField> fields) {
        this.fields = fields;

        T object = data();

        if (object != null) {
            callback.data(object)
        } else {
            filtered++
        }
        total++;
    }

    protected abstract T data();

    protected DataObjectField get(String name) {
        return fields.get(name)
    }

    String getId() {
        return id
    }

    void setId(String id) {
        this.id = id
    }

    void setDataProvider(DataProvider dataProvider) {
        this.dataProvider = dataProvider
    }

    DataProvider getDataProvider() {
        return dataProvider
    }

    static interface Callback<T> {
        void data(T object)
    }
}

// Обработчик по организациям
class OrganizationDataHandler extends DataHandler<Organization> {

    private Locality locality

    OrganizationDataHandler(Callback<Organization> callback, Locality locality) {
        super(callback)
        this.locality = locality
    }

    boolean containsLocality(DataObjectList localityDataObjectList) {
        for (localityDataObject in localityDataObjectList) {
            if (locality.id.equals(localityDataObject.id)) return true;
        }
        return false;
    }

    Organization data() {
        // Принимает ActivityOfOrganization
        DataObject orgDataObject = get('Tenant').getValue()
        DataObjectList localityDataObject = get('Locality').getValue()
        if (orgDataObject && containsLocality(localityDataObject)) {
            Organization organization = new Organization()
            organization.id = id;
            organization.tenantId = orgDataObject.id;
            organization.description = orgDataObject.getFields()
                    .get('Description').getValue()
            organization.locality = locality
            return organization

        } else {
            return null
        }

    }
}

// Обработчик по населенным пунктам
class LocalityDataHandler extends DataHandler<Locality> {

    private municipality

    LocalityDataHandler(Callback<Locality> callback, Municipality municipality) {
        super(callback)
        this.municipality = municipality
    }

    Locality data() {
        Locality locality = new Locality()
        locality.id = id;
        locality.code = get('Code').getValue() != null ? get('Code').getValue() : ''
        locality.description = get('Description').getValue()
        DataObject typeDataObject = get('TypeOfMunicipalFormation').getValue()
        locality.type = typeDataObject != null ? typeDataObject.getFields().get('Code').getValue() : ''
        return locality
    }
}

// Обработчик по муниципальным организациям
class MunicipalityDataHandler extends DataHandler<Municipality> {

    MunicipalityDataHandler(Callback<Municipality> callback) {
        super(callback)
    }

    Municipality data() {
        Municipality municipality = new Municipality()
        municipality.id = id;
        municipality.code = get('Code').getValue() != null ? get('Code').getValue() : ''
        municipality.description = get('Description').getValue()
        return municipality
    }
}

// Обработчик по способу прокладки
class WayOfLayingDataHandler extends DataHandler<WayOfLaying> {

    WayOfLayingDataHandler(Callback<WayOfLaying> callback) {
        super(callback)
    }

    WayOfLaying data() {
        WayOfLaying wayOfLaying = new WayOfLaying()
        wayOfLaying.id = id;
        wayOfLaying.code = get('Code').getValue() != null ? get('Code').getValue() : ''
        wayOfLaying.description = get('Description').getValue()
        return wayOfLaying
    }
}

// Обработчик по способу прокладки
class DiameterDataHandler extends DataHandler<Diameter> {

    DiameterDataHandler(Callback<Diameter> callback) {
        super(callback)
    }

    Diameter data() {
        Diameter diameter = new Diameter()
        diameter.id = id;
        diameter.code = get('Code').getValue() != null ? get('Code').getValue() : ''
        diameter.description = get('Description').getValue()
        return diameter
    }
}

// Обработчик по ставкам платы
class RateDataHandler extends DataHandler<Rate> {

    Diameter selectDiameter;
    WayOfLaying selectWayOfLaying;
    Double selectCapacity;

    RateDataHandler(Callback<Rate> callback, WayOfLaying selectWayOfLaying, Diameter selectDiameter, String selectCapacity) {
        super(callback)
        this.selectDiameter = selectDiameter
        this.selectWayOfLaying = selectWayOfLaying
        this.selectCapacity = convertStringToDouble(selectCapacity)

    }

    // Проверяем на принадлежность диапазону
    boolean inRange(Double firstValue, String sign, Double secondValue) {
        if (firstValue == null || secondValue == null) return true;

        switch (sign) {
            case "moreSrictly":
                return (firstValue > secondValue);
            case "moreEqual":
                return (firstValue >= secondValue);
            case "equalSrictly":
                final double epsilon = 0.001;
                return (Math.abs(firstValue - secondValue) < epsilon);
            case "lessSrictly":
                return (firstValue < secondValue);
            case "lessEqual":
                return (firstValue <= secondValue);
            default:
                return false;
        }
    }

    // id полей должны совпадать
    boolean check(String id, String nameField) {
        return id.equals(get(nameField).getValue().getId())
    }

    Rate data() {
        // Ставка платы
        Rate rate = new Rate()
        rate.id = id;
        rate.code = get('Code').getValue() != null ? get('Code').getValue() : ''
        rate.description = get('Description').getValue()

        rate.min = convertStringToDouble(get('RateRangeMin').getValue())
        rate.max = convertStringToDouble(get('RateRangeMax').getValue())

        DataObject minSign = get('RateRangeMinSign').getValue()
        rate.minSign = minSign != null ? minSign.getFields().get('Code').getValue() : ''

        DataObject maxSign = get('RateRangeMaxSign').getValue()
        rate.maxSign = maxSign != null ? maxSign.getFields().get('Code').getValue() : ''

        rate.powerCurrent = convertStringToDouble(get('RatePowerCurrent').getValue())
        rate.standCurrent = convertStringToDouble(get('RateStandCurrent').getValue())

        def wayOfLaying = get("WayOfLaying").getValue()
        def diameter = get("Diameter").getValue()

        if (wayOfLaying && diameter) {
            if (!wayOfLaying.id.equals(selectWayOfLaying.id) || !selectDiameter.id.equals(diameter.id)) {
                return null
            }
        }

        return inRange(selectCapacity, rate.minSign, rate.min) && inRange(selectCapacity, rate.maxSign, rate.max) ? rate : null
    }

    Double convertStringToDouble(String value) {
        return (StringUtils.isNotBlank(value)
                && NumberUtils.isNumber(value)) ? Double.parseDouble(value) : null
    }
}

/////////////////////////////////////////////////////////////////////////////////////////////////

// Поставшик данных
class DataProvider {

    def externalDatasource;
    def integrationsDataProvider;

    void getData(DataObject dataObject, DataHandler handler) {
        // Передаем поставшика для того чтобы можно было получать данные из других сущностей
        handler.setDataProvider(this)
        // Добоавляем id записи
        handler.setId(dataObject.getId())

        // Вызывает кэлбэк после заполнения обьекта
        handler.fill(dataObject.getFields())
    }

    // Получаем данные из списка обьектов с обработкой их в хэндлере
    void getData(List<DataObject> dataObjectList, DataHandler handler) {
        for (DataObject dataObject : dataObjectList) {
            getData(dataObject, handler)
        }
    }

    // Получаем данные из сущности с обработкой их в хэнделере
    void getData(String entityName, DataHandler handler) {
        this.getData(this.externalDataSource.getObjectList(entityName, null)
                as List<DataObject>, handler)
    }

    void getData(LinkedHashMap<String, Object> entity, DataHandler handler) {
        // Получаем обьекты по фильтру
        List<DataObject> objectList = integrationsDataProvider
                .runSavedScript('tehprisRSO_getObjectEntity', [entity: entity])
        // Заполняем список
        this.getData(objectList, handler)
    }

    void setExternalDataSource(externalDatasource) {
        this.externalDatasource = externalDatasource
    }

    def getExternalDataSource() {
        return this.externalDatasource;
    }

    void setIntegrationsDataProvider(integrationsDataProvider) {
        this.integrationsDataProvider = integrationsDataProvider
    }

}

/////////////////////////////////////////////////////////////////////////////////////////////////

// Пользовательская команда
interface Command<T> {
    // Выполнить команду
    void execute(DataProvider dataProvider);

    // Проверить выполение команды
    boolean check(MessageHandler handler);

    // Очистить выполнение команды
    void clear();

    // Получить обьект команды
    T get();
}

// Обработчик сообщений
interface MessageHandler {
    // Вывести сообщение
    void print(String status)

    // Очистить сообщение
    void clear();
}

// Последовательно выполняет заданные команды
class CommandInvoker {
    private List<Command> commands;
    DataProvider dataProvider;
    HashMap dashboardContext;
    boolean valid = true;
    private final MessageHandler handler;

    CommandInvoker(List<Command> commands, DataProvider dataProvider, HashMap dashboardContext,
                   MessageHandler messageHandler) {
        this.commands = commands
        this.dataProvider = dataProvider
        this.dashboardContext = dashboardContext
        this.handler = messageHandler;

    }

    void execute(boolean checked) {
        for (Command command : commands) {

            if (valid) {
                handler.clear();
                command.execute(dataProvider);
                if (checked && !command.check(handler)) {
                    valid = false;
                }
            } else {
                command.clear();
            }
        }
    }
}

// Команда выбора из списка
abstract class MenuDataCommand<T> implements Command<T> {

    final String selectItem;
    final String selectItems;
    HashMap dashboardContext;


    MenuDataCommand(String selectItem, String selectItems, HashMap dashboardContext) {
        this.selectItem = selectItem
        this.selectItems = selectItems
        this.dashboardContext = dashboardContext
    }

    void clear() {
        dashboardContext[selectItems] = null
        dashboardContext[selectItem] = null
    }

    T get() {
        def items = dashboardContext[selectItems]
        String id = dashboardContext[selectItem];
        return items.get(id)
    }

    public boolean isNotValue(Object value) {
        if (value == null) {
            return true
        } else if (value instanceof HashMap) {
            return value.isEmpty()
        } else if (value instanceof String) {
            return value.isEmpty()
        }
        return false
    }
}

// Команда ввода данных
abstract class InputDataCommand implements Command<String> {
    final String item;
    HashMap dashboardContext;

    InputDataCommand(String item, HashMap dashboardContext) {
        this.item = item
        this.dashboardContext = dashboardContext
    }

    void execute(DataProvider dataProvider) {
        // нет реализации
    }

    void clear() {
        dashboardContext[item] = null
    }

    String get() {
        return dashboardContext[item];
    }
}

/////////////////////////////////////////////////////////////////////////////////////////////////

// Получить муниципальные образования
class MunicipalityCommand extends MenuDataCommand<Municipality> {


    MunicipalityCommand(String selectItem, String selectItems, HashMap dashboardContext) {
        super(selectItem, selectItems, dashboardContext)
        dashboardContext[selectItems] = new LinkedHashMap<>();
    }

    void execute(DataProvider dataProvider) {
        dashboardContext[selectItems] = getMunicipalities(dataProvider)

    }

    private LinkedHashMap<String, Municipality> getMunicipalities(DataProvider dataProvider) {
        final LinkedHashMap<String, Municipality> municipalities = new LinkedHashMap<>();
        dataProvider.getData("tehprisRSO_Municipalities", new MunicipalityDataHandler(
                { municipality -> municipalities.put(municipality.id, municipality) }));
        return municipalities;
    }

    boolean check(MessageHandler handler) {
        if (isNotValue(dashboardContext[selectItems])) {
            handler.print("Муниципальные образования не найдены");
            return false;
        }
        if (isNotValue(dashboardContext[selectItem])) {
            handler.print("Выберите муниципальное образование");
            return false;
        }
        return true;
    }
}

// Получить населенные пункты
class LocalityCommand extends MenuDataCommand<Locality> {

    Command<Municipality> municipalityCommand;

    LocalityCommand(String selectItem, String selectItems, Command<Municipality> municipalityCommand, HashMap dashboardContext) {
        super(selectItem, selectItems, dashboardContext)
        this.municipalityCommand = municipalityCommand;
        dashboardContext[selectItems] = new LinkedHashMap<>();
    }

    void execute(DataProvider dataProvider) {
        if (municipalityCommand.get() != null) {
            dashboardContext[selectItems] = getLocality(dataProvider, municipalityCommand.get())
        } else {
            dashboardContext[selectItems] = null
        }

    }

    LinkedHashMap<String, Locality> getLocality(DataProvider dataProvider, Municipality municipality) {
        final LinkedHashMap<String, Locality> localities = new LinkedHashMap<>();

        // Создаем описание для фильтра по муниципалитету
        LinkedHashMap<String, Object> entityLocality = [
                link: 'tehprisRSO_Locality',
                data: [
                        Municipality: [link: 'tehprisRSO_Municipalities',
                                       data: [Id: municipality.id]]
                ]]

        // Получаем обьекты по фильтру с помощью сторонего скрипта
        List<DataObject> dataLocalityObjectList = dataProvider.integrationsDataProvider
                .runSavedScript('tehprisRSO_getObjectEntity', [entity: entityLocality])

        // Заполняем список населенных пунктов
        dataProvider.getData(dataLocalityObjectList,
                new LocalityDataHandler(
                        { locality -> localities.put(locality.id, locality) }, municipality))
        return localities;
    }


    boolean check(MessageHandler handler) {
        if (isNotValue(dashboardContext[selectItems])) {
            handler.print("Населенные пункты не найдены");
            return false;
        }
        if (isNotValue(dashboardContext[selectItem])) {
            handler.print("Выберите населенный пункт");
            return false;
        } else {

            def localities = dashboardContext[selectItems]
            def selectLocality = dashboardContext[selectItem]
            if (!localities.containsKey(selectLocality)) {
                return false;
            }
        }
        return true;
    }
}

// Получить организации по теплоснабжению
class OrganizationCommand extends MenuDataCommand<Organization> {

    Command<Locality> localityCommand;

    OrganizationCommand(String selectItem, String selectItems, Command<Locality> localityCommand, HashMap dashboardContext) {
        super(selectItem, selectItems, dashboardContext)
        this.localityCommand = localityCommand;
        dashboardContext[selectItems] = new LinkedHashMap<>();
    }

    void execute(DataProvider dataProvider) {
        if (localityCommand.get() != null) {
            dashboardContext[selectItems] = getOrganization(dataProvider, localityCommand.get())
        } else {
            dashboardContext[selectItems] = null
        }

    }

    LinkedHashMap<String, Organization> getOrganization(DataProvider dataProvider, Locality locality) {
        final LinkedHashMap<String, Organization> organizations = new LinkedHashMap<>();

        // Создаем описание для фильтра по муниципалитету
        LinkedHashMap<String, Object> activityOfOrganizationEntity = [
                link: 'tehprisRSO_ActivityOfOrganization',
                data: [
                        TypeOfActivity:
                                [link: 'tehprisRSO_TypeOfActivity',
                                 data: [Code: 'TS']]
                ]]

        // Получаем обьекты по фильтру с помощью сторонего скрипта
        List<DataObject> dataOrganizationObjectList = dataProvider.integrationsDataProvider
                .runSavedScript('tehprisRSO_getObjectEntity', [entity: activityOfOrganizationEntity])

        // Заполняем список населенных пунктов
        dataProvider.getData(dataOrganizationObjectList,
                new OrganizationDataHandler(
                        { organization -> organizations.put(organization.tenantId, organization) }, locality))
        return organizations;
    }


    boolean check(MessageHandler handler) {
        if (isNotValue(dashboardContext[selectItems])) {
            handler.print("Организации не найдены");
            return false;
        }
        if (isNotValue(dashboardContext[selectItem])) {
            handler.print("Выберите организацию");
            return false;
        }
        return true;
    }
}

// Получить тепловую нагрузку
class CapacityCommand extends InputDataCommand {

    CapacityCommand(String item, HashMap dashboardContext) {
        super(item, dashboardContext)
    }

    boolean check(MessageHandler handler) {
        if (!dashboardContext[this.item]) {
            handler.print("Введите тепловую нагрузку");
            return false;
        }

        if (convertToDouble(dashboardContext[this.item]) < 0) {
            handler.print("Тепловая нагрузка не может быть отрицательным");
            return false;
        }

        return true
    }

    Double convertToDouble(Object value) {
        return Double.parseDouble(value)
    }
}
// Получить Способ прокладки
class WayOfLayingCommand extends MenuDataCommand<WayOfLaying> {

    WayOfLayingCommand(String selectItem, String selectItems, HashMap dashboardContext) {
        super(selectItem, selectItems, dashboardContext)
        dashboardContext[selectItems] = new LinkedHashMap<>();
    }

    void execute(DataProvider dataProvider) {
        dashboardContext[selectItems] = getWaysOfLaying(dataProvider)
    }

    private LinkedHashMap<String, WayOfLaying> getWaysOfLaying(DataProvider dataProvider) {
        final LinkedHashMap<String, WayOfLaying> waysOfLaying = new LinkedHashMap<>();
        dataProvider.getData("tehprisRSO_WayOfLayingTSScientificCalculator", new WayOfLayingDataHandler(
                { wayOfLaying -> waysOfLaying.put(wayOfLaying.id, wayOfLaying) }));
        return waysOfLaying;
    }


    boolean check(MessageHandler handler) {
        if (isNotValue(dashboardContext[selectItems])) {
            handler.print("Способы прокладки не найдены");
            return false;
        }
        if (isNotValue(dashboardContext[selectItem])) {
            handler.print("Выберите cпособ прокладки");
            return false;
        }
        return true;
    }
}

// Получить диаметр
class DiameterCommand extends MenuDataCommand<Diameter> {

    DiameterCommand(String selectItem, String selectItems, HashMap dashboardContext) {
        super(selectItem, selectItems, dashboardContext)
        dashboardContext[selectItems] = new LinkedHashMap<>();
    }

    void execute(DataProvider dataProvider) {
        dashboardContext[selectItems] = getDiameter(dataProvider)
    }

    boolean check(MessageHandler handler) {
        if (isNotValue(dashboardContext[selectItems])) {
            handler.print("Список диаметров не найдены");
            return false;
        }
        if (isNotValue(dashboardContext[selectItem])) {
            handler.print("Выберите диаметр");
            return false;
        }
        return true
    }

    LinkedHashMap<String, Diameter> getDiameter(DataProvider dataProvider) {
        final LinkedHashMap<String, Diameter> diameters = new LinkedHashMap<>();
        dataProvider.getData("tehprisRSO_DiameterTSScientificCalculator", new DiameterDataHandler(
                { diameter -> diameters.put(diameter.id, diameter) }));
        return diameters;
    }

}

// Рассчитать
class CalculateCommand implements Command<List<String>> {
    HashMap dashboardContext
    OrganizationCommand organizationCommand;
    WayOfLayingCommand wayOfLayingCommand;
    CapacityCommand capacityCommand
    DiameterCommand diameterCommand;

    final String summary;
    final String summaryVAT;
    final String summaryWithVAT;

    CalculateCommand(String summary, String summaryVAT, String summaryWithVAT, HashMap dashboardContext,
                     OrganizationCommand organizationCommand, WayOfLayingCommand wayOfLayingCommand,
                     CapacityCommand capacityCommand, DiameterCommand diameterCommand) {
        this.dashboardContext = dashboardContext
        this.organizationCommand = organizationCommand
        this.wayOfLayingCommand = wayOfLayingCommand
        this.diameterCommand = diameterCommand
        this.capacityCommand = capacityCommand

        this.summary = summary
        this.summaryVAT = summaryVAT
        this.summaryWithVAT = summaryWithVAT
    }

    void execute(DataProvider dataProvider) {

       boolean ndsOrganization = false


        if (organizationCommand.get()) {
            def uSubjectName = FilterGroup.single('Tenant', FilterElement.FilterOperator.equals,
                    [organizationCommand.get().tenantId])
            def org = dataProvider.externalDataSource.getObjectList("tehprisRSO_Organization", null,
                    uSubjectName, null, null, null, null).getAt(0)
            if (org) {
                ndsOrganization = org.value.fields.NDS ? org.value.fields.NDS.value : false
            }

        }

        List<Rate> rates = new ArrayList<>();

        // Получаем ставки платы
        dataProvider.getData([
                link: 'tehprisRSO_CalcRateTSScientificCalculator',
                data: [
                        Tenant:
                                [link: 'Tenant',
                                 data: [Id: organizationCommand.get().tenantId]]
                ]],
                new RateDataHandler(
                        { rate -> rates.add(rate) }, wayOfLayingCommand.get(),
                        diameterCommand.get(), capacityCommand.get()))


        def price = 0
        def nds = 0
        def result = 0

        if (rates.size() > 0 && rates.getAt(0) != null) {

            if (ndsOrganization) {
                price = rates.getAt(0).powerCurrent
                nds = price - price / 1.18
                result = price / 1.18

                dashboardContext[summary] = round(result, 2)
                dashboardContext[summaryVAT] = round(nds, 2)
                dashboardContext[summaryWithVAT] = round(price, 2)

            } else {
                price = rates.getAt(0).powerCurrent
                nds = price * 0.18
                result = price + nds

                dashboardContext[summary] = round(price, 2)
                dashboardContext[summaryVAT] = round(nds, 2)
                dashboardContext[summaryWithVAT] = round(result, 2)
            }
        } else {
            clear();
        }
    }

    boolean check(MessageHandler handler) {
        if (isNotValue(dashboardContext[summary])) {
            handler.print("Ставки платы для данной организации не найдены");
            return false;
        }
        return true;
    }


    void clear() {
        dashboardContext[summary] = null
        dashboardContext[summaryVAT] = null
        dashboardContext[summaryWithVAT] = null
    }

    List<String> get() {
        return null
    }

    double round(double value, int places) {
        BigDecimal bd = new BigDecimal(value);
        bd = bd.setScale(places, RoundingMode.HALF_UP);
        return bd.doubleValue();
    }

    boolean isNotValue(Double value) {
        if (value == null) {
            return true
        }
        return false
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////

// Обработчик вывода сообщений в dashboard
class MessageDashboardHandler implements MessageHandler {

    final String field;
    HashMap dashboardContext

    MessageDashboardHandler(HashMap dashboardContext, String field) {
        this.field = field
        this.dashboardContext = dashboardContext
    }

    void print(String status) {
        dashboardContext[field] = status
    }

    void clear() {
        dashboardContext[field] = null
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////

MunicipalityCommand municipalityCommand = new MunicipalityCommand(
        "tehprisRSO_HeatSupplyCalc_selectMunicipality",
        "tehprisRSO_HeatSupplyCalc_municipalities", dashboardContext.context)

LocalityCommand localityCommand = new LocalityCommand(
        "tehprisRSO_HeatSupplyCalc_selectLocality",
        "tehprisRSO_HeatSupplyCalc_localities",
        municipalityCommand, dashboardContext.context)

OrganizationCommand organizationCommand = new OrganizationCommand(
        "tehprisRSO_HeatSupplyCalc_selectOrganization",
        "tehprisRSO_HeatSupplyCalc_organizations",
        localityCommand, dashboardContext.context)

CapacityCommand capacityCommand = new CapacityCommand(
        "tehprisRSO_HeatSupplyCalc_selectCapacity", dashboardContext.context)

WayOfLayingCommand wayOfLayingCommand = new WayOfLayingCommand(
        "tehprisRSO_HeatSupplyCalc_selectWayOfLaying",
        "tehprisRSO_HeatSupplyCalc_waysOfLaying", dashboardContext.context)

DiameterCommand diameterCommand = new DiameterCommand(
        "tehprisRSO_HeatSupplyCalc_selectDiameter",
        "tehprisRSO_HeatSupplyCalc_diameters",
        dashboardContext.context)

CalculateCommand calculateCommand = new CalculateCommand(
        "tehprisRSO_HeatSupplyCalc_summary",
        "tehprisRSO_HeatSupplyCalc_summaryVAT",
        "tehprisRSO_HeatSupplyCalc_summaryWithVAT", dashboardContext.context,
        organizationCommand, wayOfLayingCommand, capacityCommand, diameterCommand
)
/////////////////////////////////////////////////////////////////////////////////////////////////

DataProvider dataProvider = new DataProvider()
dataProvider.setExternalDataSource(externalDataSource)
dataProvider.setIntegrationsDataProvider(integrationsDataProvider)

List<Command> commands = new ArrayList<Command>();
commands.add(municipalityCommand)
commands.add(localityCommand)
commands.add(organizationCommand)
commands.add(capacityCommand)
commands.add(wayOfLayingCommand)
commands.add(diameterCommand)
commands.add(calculateCommand)

CommandInvoker invoker = new CommandInvoker(commands, dataProvider, dashboardContext.context,
        new MessageDashboardHandler(dashboardContext.context, "tehprisRSO_HeatSupplyCalc_alert"))

invoker.execute(true);


