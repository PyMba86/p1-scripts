package ru.osslabs.portaltpext.controller.portlet.calculator;


import ru.osslabs.model.datasource.DataObject;

import javax.annotation.PostConstruct;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.http.HttpServletRequest;
import java.io.Serializable;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.*;

import static ru.osslabs.portaltpext.controller.portlet.calculator.CalculatorsConstants.*;

@Named
@ViewScoped
public class PowerCalculatorBean implements Serializable{

    @Inject
    PowerCalculatorDataProvider dataProvider;

    private String premisesTypeId;
    private String consumerTypeId;
    private String consumerSubtypeId;

    private String premisesType;
    private String consumerType;
    private String consumerSubtype;

    private Map<String, String> premisesTypesMap;
    private Map<String, String> consumerTypesMap;
    private Map<String, String> consumerSubtypesMap;

    private boolean related;
    private String unitLoadMaxID;
    private String unitMeasure;
    private String advancedOption;
    private String additionalUnitMeasure;
    private String additionalAdvancedOption;
    private double minValue;
    private double maxValue;
    private int decimalPlace;

    private double m;
    private double resultP;
    private double P04;
    private double P10;

    private double Pz;

    private PowerCalculatorDataProvider.MultiTreeMap resultsMultiMap;
    private List<PowerCalculatorBean> dynList;

    private DecimalFormat df;

    @PostConstruct
    public void init() {
        premisesTypesMap = new HashMap<>();
        premisesTypesMap = dataProvider.getPremisesTypes();

        resultsMultiMap = dataProvider.new MultiTreeMap();
        dynList = new ArrayList<>();

        df = new DecimalFormat("#.###");
        df.setRoundingMode(RoundingMode.HALF_UP);
    }

    public void action() {
        clear();
        if (premisesTypeId != null && !premisesTypeId.equals("")) {
            consumerTypesMap = dataProvider.getConsumerTypes(premisesTypeId);

            premisesType = premisesTypesMap.get(premisesTypeId);
        } else {
            consumerTypesMap = new HashMap<>();
        }
        consumerTypeId = null;
        consumerSubtypeId = null;
    }

    public void action2() {
        clear();
        if (consumerTypeId != null && !consumerTypeId.equals("")) {
            consumerSubtypesMap = dataProvider.getConsumerSubtypes(consumerTypeId);

            consumerType = consumerTypesMap.get(consumerTypeId);
        } else {
            consumerSubtypesMap = new HashMap<>();
        }
        consumerSubtypeId = null;
    }

    public void action3() {
        clear();

        consumerSubtype = consumerSubtypesMap.get(consumerSubtypeId);

        dataProvider.initOptions(consumerSubtypeId);

        unitLoadMaxID = dataProvider.getUnitLoadMax().getId();
        unitMeasure = dataProvider.getUnitMeasure();
        advancedOption = dataProvider.getAdvancedOption();
        minValue = dataProvider.getRangeMin();
        maxValue = dataProvider.getRangeMax();
        related = dataProvider.isRelated();

        //fix for multiple values
        if (!related) {
            String[] parts = unitMeasure.split("\\|");
            additionalUnitMeasure = parts[0];
            unitMeasure = parts[1];
            parts = advancedOption.split("\\|");
            additionalAdvancedOption = parts[0];
            advancedOption = parts[1];
        }

        //fix for decimal places
        decimalPlace = (minValue - (int) minValue) == 0 ? 0 : 1;
    }

    public void calc() {
        if (related) {
            resultP = firstFormula();
        } else {
            resultP = secondFormula();
        }

        if (resultsMultiMap.size() == 0) {
            P04 = resultP;
            P10 = P04 * 0.9;
        } else {
            calcTotalLoad();
        }
    }

    public double firstFormula() {
        TreeMap<Integer, Double> map = dataProvider.getUnitLoad(consumerSubtypeId);
        double p = 0;

        if (map.size() == 1) {
            p = map.get(map.firstKey());
        } else {
            double P1 = 0;
            double P2 = 0;

            double m0 = m < 3 ? 3 : m;
            int m1 = 0;
            int m2 = 0;
            for (Map.Entry<Integer, Double> entry : map.entrySet()) {
                m2 = entry.getKey();

                if (m0 == m2) {
                    p = entry.getValue();
                    break;
                } else if (m0 > m2) {
                    m1 = entry.getKey();
                    P1 = entry.getValue();
                } else {
                    m2 = entry.getKey();
                    P2 = entry.getValue();
                    p = P1 + ((P2 - P1) * (m0 - m1)) / (m2 - m1);
                    break;
                }
            }
        }

        return p * m;
    }

    public double secondFormula() {
        TreeMap<Double, Double> map = dataProvider.getDemandFactors();

        double p1 = 0;
        double k1 = 0;
        for (Map.Entry<Double, Double> entry : map.entrySet()) {
            p1 = entry.getKey();

            if (Pz == p1) {
                k1 = entry.getValue();
                break;
            }else if (Pz < p1) {
                k1 = entry.getValue();
                break;
            }
        }

        map = dataProvider.getDiversionFactors();

        double p2 = 0;
        double k2 = 0;
        for (Map.Entry<Double, Double> entry : map.entrySet()) {
            p2 = entry.getKey();

            if (m == p2) {
                k2 = entry.getValue();
                break;
            }else if (m < p2) {
                k2 = entry.getValue();
                break;
            }
        }

        return Pz * m * k1 * k2;
    }

    public void calcTotalLoad() {
        double Pmax = resultsMultiMap.lastKey();
        double p;
        double k;
        double var1 = 0;
        double var2 = 0;
        DataObject baseObject = dataProvider.getUnitLoadMax();
        if (resultP >= Pmax) {
            Pmax = resultP;
        } else {
            List<String> idsList = resultsMultiMap.get(Pmax);
            String id = idsList.get(0);

            DataObject dataObject = baseObject;
            baseObject = dataProvider.getDataSource().getObject(FIELD_UNIT_LOAD_MAX, id);

            String code = dataObject.getFields().get(FIELD_CODE).getValue().toString();
            k = Double.parseDouble(baseObject.getFields().get(code).getValue().toString());
            var1 = resultP * k;

            code = baseObject.getFields().get(FIELD_CODE).getValue().toString();
            k = Double.parseDouble(baseObject.getFields().get(code).getValue().toString());
            var2 = Pmax * k;
        }

        P04 = 0;
        for (Map.Entry<Double, List<String>> e : resultsMultiMap.entrySet()) {
            p = e.getKey();
            List<String> list = resultsMultiMap.get(p);

            for (String aList : list) {
                DataObject dataObject = dataProvider.getDataSource().getObject(FIELD_UNIT_LOAD_MAX, aList);
                String code = dataObject.getFields().get(FIELD_CODE).getValue().toString();
                k = Double.parseDouble(baseObject.getFields().get(code).getValue().toString());

                P04 += p * k;
            }
        }

        P04 = Pmax + var1 + P04 - var2;
        P10 = P04 * 0.9;
    }

    public void addObject() {
        calc();
        resultsMultiMap.put(resultP, unitLoadMaxID);
        dynList.add(calculatedObject(this));

        clear();

        premisesTypeId = null;
        consumerTypeId = null;
        consumerSubtypeId = null;
    }

    public void removeObject(int index) {
        double calculatedResultP = dynList.get(index).getResultP();
        if (resultsMultiMap.get(calculatedResultP).size() == 1) {
            resultsMultiMap.remove(calculatedResultP);
        } else {
            String id = dynList.get(index).getUnitLoadMaxID();
            resultsMultiMap.get(calculatedResultP).remove(id);
        }
        dynList.remove(index);
        if (resultsMultiMap.size() != 0) {
            clear();
            calcTotalLoad();
        }
    }

    public PowerCalculatorBean calculatedObject(PowerCalculatorBean pc) {
        PowerCalculatorBean calculations = new PowerCalculatorBean();

        calculations.premisesType = pc.getPremisesType();
        calculations.consumerType = pc.getConsumerType();
        calculations.consumerSubtype = pc.getConsumerSubtype();
        calculations.unitMeasure = pc.getUnitMeasure();
        calculations.advancedOption = pc.getAdvancedOption();
        calculations.m = pc.getM();
        calculations.resultP = pc.getResultP();
        calculations.unitLoadMaxID = pc.getUnitLoadMaxID();

        return calculations;
    }

    public void clear() {
        m = 0;
        Pz = 0;
        resultP = 0;
    }

    public String formatter(double value) {
        return df.format(value);
    }

    public int getDecimalPlace() {
        return decimalPlace;
    }

    public String getAdditionalUnitMeasure() {
        return additionalUnitMeasure;
    }

    public String getAdditionalAdvancedOption() {
        return additionalAdvancedOption;
    }

    public boolean isRelated() {
        return related;
    }

    public void setPz(double pz) {
        Pz = pz;
    }

    public double getPz() {
        return Pz;
    }

    public String getUnitLoadMaxID() {
        return unitLoadMaxID;
    }

    public List<PowerCalculatorBean> getDynList() {
        return dynList;
    }

    public double getP04() {
        return P04;
    }

    public double getP10() {
        return P10;
    }

    public double getResultP() {
        return resultP;
    }

    public double getM() {
        return m;
    }

    public void setM(double m) {
        this.m = m;
    }

    public String getUnitMeasure() {
        return unitMeasure;
    }

    public String getAdvancedOption() {
        return advancedOption;
    }

    public double getMinValue() {
        return minValue;
    }

    public double getMaxValue() {
        return maxValue;
    }

    public String getPremisesType() {
        return premisesType;
    }

    public String getConsumerType() {
        return consumerType;
    }

    public String getConsumerSubtype() {
        return consumerSubtype;
    }

    public String getConsumerSubtypeId() {
        return consumerSubtypeId;
    }

    public void setConsumerSubtypeId(String consumerSubtypeId) {
        this.consumerSubtypeId = consumerSubtypeId;
    }

    public String getPremisesTypeId() {
        return premisesTypeId;
    }

    public void setPremisesTypeId(String premisesTypeId) {
        this.premisesTypeId = premisesTypeId;
    }

    public String getConsumerTypeId() {
        return consumerTypeId;
    }

    public void setConsumerTypeId(String consumerTypeId) {
        this.consumerTypeId = consumerTypeId;
    }

    public Map<String, String> getPremisesTypesMap() {
        return premisesTypesMap;
    }

    public Map<String, String> getConsumerTypesMap() {
        return consumerTypesMap;
    }

    public Map<String, String> getConsumerSubtypesMap() {
        return consumerSubtypesMap;
    }
}
