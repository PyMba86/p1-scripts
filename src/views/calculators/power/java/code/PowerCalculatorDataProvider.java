package ru.osslabs.portaltpext.controller.portlet.calculator;

import ru.osslabs.model.common.filter.Filter;
import ru.osslabs.model.common.filter.FilterElement;
import ru.osslabs.model.common.filter.FilterGroup;
import ru.osslabs.model.datasource.DataObject;
import ru.osslabs.model.datasource.ExternalDataSource;
import ru.osslabs.portaltpext.utils.LocalObjectUtils;

import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.Serializable;
import java.util.*;

import static ru.osslabs.portaltpext.controller.portlet.calculator.CalculatorsConstants.*;

@Named
@ViewScoped
public class PowerCalculatorDataProvider implements Serializable {

    @Inject
    private ExternalDataSource dataSource;

    private List<DataObject> premisesTypes;
    private List<DataObject> consumerTypes;
    private List<DataObject> consumerSubtypes;
    private List<DataObject> unitLoad;
    private List<DataObject> demandFactors;
    private List<DataObject> diversionFactors;


    private DataObject unitLoadMax;
    private String unitMeasure;
    private String advancedOption;
    private double rangeMin;
    private double rangeMax;
    private boolean related;

    @PostConstruct
    void init() {
        premisesTypes = getDataSource().getObjectList(CLASS_TYPE_PREMISES_TYPES, null);
        consumerTypes = getDataSource().getObjectList(CLASS_TYPE_CONSUMER_TYPES, null);
        consumerSubtypes = getDataSource().getObjectList(CLASS_TYPE_CONSUMER_SUBTYPES, null);
        unitLoad = getDataSource().getObjectList(CLASS_TYPE_UNIT_LOAD, null);
        demandFactors = getDataSource().getObjectList(CLASS_TYPE_DEMAND_FACTORS, null);
        diversionFactors = getDataSource().getObjectList(CLASS_TYPE_DIVERSION_FACTORS, null);
    }

    public Map<String, String> getPremisesTypes() {
        Map<String, String> map = new LinkedHashMap<>();

        for (DataObject dataObject : premisesTypes) {
            String value = dataObject.getFields().get(FIELD_DESCRIPTION).getValue().toString();
            map.put(dataObject.getId(), value);
        }

        return map;
    }

    public Map<String, String> getConsumerTypes(String id) {
        Map<String, String> map = new LinkedHashMap<>();

        FilterGroup filterGroup = FilterGroup.single(FIELD_PREMISES_TYPE, FilterElement.FilterOperator.equals, Collections.singletonList((Object) id));

        List<DataObject> dataObjects = getDataSource().getObjectList(CLASS_TYPE_CONSUMER_TYPES, null, filterGroup, null, null, null, null);
//        List<DataObject> dataObjects = LocalObjectUtils.getObjectRelationsList(consumerTypes, id, FIELD_PREMISES_TYPE);
        for (DataObject dataObject : dataObjects) {
            String value = dataObject.getFields().get(FIELD_DESCRIPTION).getValue().toString();
            map.put(dataObject.getId(), value);
        }

        return map;
    }

    public Map<String, String> getConsumerSubtypes(String id) {
        Map<String, String> map = new LinkedHashMap<>();

        FilterGroup filterGroup = FilterGroup.single(FIELD_CONSUMER_TYPE, FilterElement.FilterOperator.equals, Collections.singletonList((Object) id));

        List<DataObject> dataObjects = getDataSource().getObjectList(CLASS_TYPE_CONSUMER_SUBTYPES, null, filterGroup, null, null, null, null);
//        List<DataObject> dataObjects = LocalObjectUtils.getObjectRelationsList(consumerSubtypes, id, FIELD_CONSUMER_TYPE);
        for (DataObject dataObject : dataObjects) {
            String value = dataObject.getFields().get(FIELD_DESCRIPTION).getValue().toString();
            map.put(dataObject.getId(), value);
        }

        return map;
    }

    public TreeMap<Integer, Double> getUnitLoad(String id) {
        TreeMap<Integer, Double> map = new TreeMap<>();

        FilterGroup filterGroup = FilterGroup.single(FIELD_CONSUMER_SUBTYPE, FilterElement.FilterOperator.equals, Collections.singletonList((Object) id));

        List<DataObject> dataObjects = getDataSource().getObjectList(CLASS_TYPE_UNIT_LOAD, null, filterGroup, null, null, null, null);
//        List<DataObject> dataObjects = LocalObjectUtils.getObjectRelationsList(unitLoad, id, FIELD_CONSUMER_SUBTYPE);
        for (DataObject dataObject : dataObjects) {
            Object valueObj = dataObject.getFields().get(FIELD_AMOUNT).getValue();
            String amount = valueObj != null && !valueObj.toString().isEmpty() ? valueObj.toString() : "0";
            String value = dataObject.getFields().get(FIELD_VALUE).getValue().toString();
            map.put(Integer.parseInt(amount), Double.parseDouble(value));
        }

        return map;
    }

    public TreeMap<Double, Double> getDemandFactors() {
        TreeMap<Double, Double> map = new TreeMap<>();

        for (DataObject dataObject : demandFactors) {
            String code = dataObject.getFields().get(FIELD_CODE).getValue().toString();
            String value = dataObject.getFields().get(FIELD_DESCRIPTION).getValue().toString();
            map.put(Double.parseDouble(code), Double.parseDouble(value));
        }

        return map;
    }

    public TreeMap<Double, Double> getDiversionFactors() {
        TreeMap<Double, Double> map = new TreeMap<>();

        for (DataObject dataObject : diversionFactors) {
            String code = dataObject.getFields().get(FIELD_CODE).getValue().toString();
            String value = dataObject.getFields().get(FIELD_DESCRIPTION).getValue().toString();
            map.put(Double.parseDouble(code), Double.parseDouble(value));
        }

        return map;
    }

    public void initOptions(String id) {
        DataObject dataObject = LocalObjectUtils.getObject(consumerSubtypes, id);

        FilterGroup filterGroup = FilterGroup.single(FIELD_CONSUMER_SUBTYPE, FilterElement.FilterOperator.equals, Collections.singletonList((Object) id));

        related = !getDataSource().getObjectList(CLASS_TYPE_UNIT_LOAD, null, filterGroup, null, null, null, null).isEmpty();

        unitLoadMax = (DataObject) dataObject.getFields().get(FIELD_UNIT_LOAD_MAX).getValue();
        unitMeasure = dataObject.getFields().get(FIELD_UNIT_MEASURE).getValue().toString();
        advancedOption = dataObject.getFields().get(FIELD_ADVANCED_OPTION).getValue().toString();
        rangeMin = Double.parseDouble(dataObject.getFields().get(FIELD_MIN_VALUE).getValue().toString());
        rangeMax = Double.parseDouble(dataObject.getFields().get(FIELD_MAX_VALUE).getValue().toString());
    }

    class MultiTreeMap extends TreeMap<Double, List<String>> {
        public void put(Double key, String value) {
            List<String> current = get(key);
            if (current == null) {
                current = new ArrayList<>();
                super.put(key, current);
            }
            current.add(value);
        }
    }

    public boolean isRelated() {
        return related;
    }

    public DataObject getUnitLoadMax() {
        return unitLoadMax;
    }

    public String getUnitMeasure() {
        return unitMeasure;
    }

    public String getAdvancedOption() {
        return advancedOption;
    }

    public double getRangeMin() {
        return rangeMin;
    }

    public double getRangeMax() {
        return rangeMax;
    }

    public ExternalDataSource getDataSource() {
        return dataSource;
    }
}
