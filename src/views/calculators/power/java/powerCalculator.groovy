import ru.osslabs.model.datasource.DataObject;
import ru.osslabs.model.datasource.DataObjectField;
import java.util.LinkedHashMap;
import ru.osslabs.model.common.filter.FilterElement;
import ru.osslabs.model.common.filter.FilterGroup;

// Обработчик по получению данных из DataObject
// Получает обьект, Заполняет новый обьект по определенным полям
abstract class DataHandler<T> {

    private final Callback<T> callback;
    private DataProvider dataProvider;
    private int total;
    private int filtered;
    protected LinkedHashMap<String, DataObjectField> fields;
    private String id;

    DataHandler(Callback<T> callback) {
        this.callback = callback;
    }

    void fill(LinkedHashMap<String, DataObjectField> fields) {
        this.fields = fields;

        T object = data();

        if (object != null) {
            callback.data(object)
        } else {
            filtered++
        }
        total++;
    }

    protected abstract T data();

    protected DataObjectField get(String name) {
        return fields.get(name)
    }

    String getId() {
        return id
    }

    void setId(String id) {
        this.id = id
    }

    void setDataProvider(DataProvider dataProvider) {
        this.dataProvider = dataProvider
    }

    DataProvider getDataProvider() {
        return dataProvider
    }

    static interface Callback<T> {
        void data(T object)
    }
}

// Поставшик данных
class DataProvider {

    def externalDatasource;
    def integrationsDataProvider;

    void getData(DataObject dataObject, DataHandler handler) {
        // Передаем поставшика для того чтобы можно было получать данные из других сущностей
        handler.setDataProvider(this)
        // Добоавляем id записи
        handler.setId(dataObject.getId())
        // Вызывает кэлбэк после заполнения обьекта
        handler.fill(dataObject.getFields())
    }

// Получаем данные из списка обьектов с обработкой их в хэндлере
    void getData(List<DataObject> dataObjectList, DataHandler handler) {
        for (DataObject dataObject : dataObjectList) {
            getData(dataObject, handler)
        }
    }

// Получаем данные из сущности с обработкой их в хэнделере
    void getData(String entityName, DataHandler handler) {
        this.getData(this.externalDataSource.getObjectList(entityName, null)
                as List<DataObject>, handler)
    }


    void getData(String entityName, FilterGroup filter, DataHandler handler) {
        // Получаем список обьектов по фильтру
        List<DataObject> list =
                this.externalDataSource.getObjectList(entityName, null, filter, null, null, null, null)

        this.getData(list, handler)
    }

    void getData(LinkedHashMap<String, Object> entity, DataHandler handler) {
        // Получаем обьекты по фильтру
        List<DataObject> objectList = integrationsDataProvider
                .runSavedScript('tehprisRSO_getObjectEntity', [entity: entity])
        // Заполняем список
        this.getData(objectList, handler)
    }

    void setExternalDataSource(externalDatasource) {
        this.externalDatasource = externalDatasource
    }

    def getExternalDataSource() {
        return this.externalDatasource;
    }

    void setIntegrationsDataProvider(integrationsDataProvider) {
        this.integrationsDataProvider = integrationsDataProvider
    }

}

////////////////////////////////////////////////////////////////////////////////////////////////////////////

// Тип помешения
class PremiseType {
    public String id;
    public String description;

    public String getDescription() {
        return description
    }

    public void setDescription(String description) {
        this.description = description
    }
}

// Тип потребителя
class ConsumerType {
    public String id;
    public String description;

    public String getDescription() {
        return description
    }

    public void setDescription(String description) {
        this.description = description
    }
}

// Подтип потребителя
class ConsumerSubType {
    public String id;
    public String description;
    public ConsumerType consumerType;
    public UnitLoadMax unitLoadMax;
    public String unitMeasure;
    public String advancedOption;
    public Double minValue;
    public Double maxValue;
    public String additionalAdvancedOption;


    String getAdditionalAdvancedOption() {
        return additionalAdvancedOption
    }

    void setAdditionalAdvancedOption(String additionalAdvancedOption) {
        this.additionalAdvancedOption = additionalAdvancedOption
    }

    public String getDescription() {
        return description
    }

    public void setDescription(String description) {
        this.description = description
    }

    String getUnitMeasure() {
        return unitMeasure
    }

    void setUnitMeasure(String unitMeasure) {
        this.unitMeasure = unitMeasure
    }

    String getAdvancedOption() {
        return advancedOption
    }

    void setAdvancedOption(String advancedOption) {
        this.advancedOption = advancedOption
    }

    Double getMinValue() {
        return minValue
    }

    void setMinValue(Double minValue) {
        this.minValue = minValue
    }

    Double getMaxValue() {
        return maxValue
    }

    void setMaxValue(Double maxValue) {
        this.maxValue = maxValue
    }
}

// Максимум нагрузки
class UnitLoadMax {
    public String id;
    public String code;
    public String description;
    public ArrayList<Double> units = new ArrayList<Double>();

    public String getDescription() {
        return description
    }

    public void setDescription(String description) {
        this.description = description
    }
}

// Коэффиценты спроса
class DemandFactor {
    public String id;
    public String code;
    public String description;

    public String getDescription() {
        return description
    }

    public void setDescription(String description) {
        this.description = description
    }
}

// Коэффиценты одновремености
class DiversionFactor {
    public String id;
    public String code;
    public String description;

    public String getDescription() {
        return description
    }

    public void setDescription(String description) {
        this.description = description
    }
}

// Удельная нагрузка
class UnitLoad {
    public String id;
    public String code;
    public ConsumerSubType consumerSubType;
    public Integer amount;
    public Double value;

    public String getDescription() {
        return description
    }

    public void setDescription(String description) {
        this.description = description
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////

// Обработчик по типам помешения
class PremiseTypeDataHandler extends DataHandler<PremiseType> {

    PremiseTypeDataHandler(Callback<PremiseType> callback) {
        super(callback)
    }

    PremiseType data() {
        PremiseType type = new PremiseType()
        type.id = id;
        type.description = get('Description').getValue()
        return type
    }
}

// Обработчик по типам потребителя
class ConsumerTypeDataHandler extends DataHandler<ConsumerType> {

    ConsumerTypeDataHandler(Callback<ConsumerType> callback) {
        super(callback)
    }

    ConsumerType data() {
        ConsumerType type = new ConsumerType()
        type.id = id;
        type.description = get('Description').getValue()
        return type
    }
}

// Обработчик по подтипам потребителя
class ConsumerSubTypeDataHandler extends DataHandler<ConsumerSubType> {

    ConsumerType consumerType;
    boolean related;

    ConsumerSubTypeDataHandler(Callback<ConsumerSubType> callback, ConsumerType consumerType, boolean related) {
        super(callback)
        this.consumerType = consumerType;
        this.related = related;
    }

    ConsumerSubType data() {
        ConsumerSubType type = new ConsumerSubType()
        type.id = id;
        type.consumerType = consumerType;
        type.description = get('Description').getValue()

        DataObject unitLoadMaxObject = get('UnitLoadMax').getValue()
        if (unitLoadMaxObject) {
            UnitLoadMax unitLoadMax = new UnitLoadMax()
            dataProvider.getData(unitLoadMaxObject,
                    new UnitLoadMaxDataHandler({ unit -> unitLoadMax = unit }))
            type.unitLoadMax = unitLoadMax
        } else {
            return null
        }

        if (!related) {
            String[] parts = get('UnitMeasure').getValue().split("\\|");
            type.additionalUnitMeasure = parts[0];
            type.unitMeasure = parts[1];
            parts = advancedOption.split("\\|");
            type.additionalAdvancedOption = parts[0];
            type.advancedOption = parts[1];
        } else {
            type.advancedOption = get('AdvancedOption').getValue()
            type.unitMeasure = get('UnitMeasure').getValue()
        }

        type.minValue = get('MinValue').getValue()
        type.maxValue = get('MaxValue').getValue()

        return type
    }
}

// Обработчик по максимум нагрузки
class UnitLoadMaxDataHandler extends DataHandler<UnitLoadMax> {


    UnitLoadMaxDataHandler(Callback<UnitLoadMax> callback) {
        super(callback)
    }

    UnitLoadMax data() {
        UnitLoadMax unitLoadMax = new UnitLoadMax()
        unitLoadMax.code = get('Code').getValue()
        unitLoadMax.id = id;
        unitLoadMax.description = get('Description').getValue()

        for (def i = 1; i < 12; i++) {
            unitLoadMax.units.add(get('Unit' + i.toString()).getValue())
        }

        return unitLoadMax
    }
}

// Удельная нагрузка
class UnitLoadDataHandler extends DataHandler<UnitLoad> {

    ConsumerSubType consumerSubType;

    UnitLoadDataHandler(Callback<UnitLoad> callback, ConsumerSubType consumerSubType) {
        super(callback)
        this.consumerSubType = consumerSubType
    }

    UnitLoad data() {
        UnitLoad unitLoad = new UnitLoad()
        unitLoad.code = get('Code').getValue()
        unitLoad.id = id;
        unitLoad.consumerSubType = consumerSubType;
        unitLoad.amount = get('Amount').getValue() != null ? get('Amount').getValue() : 0
        unitLoad.value = get('Value').getValue()

        return unitLoad
    }
}

// Удельная нагрузка
class DemandFactorsDataHandler extends DataHandler<DemandFactor> {


    DemandFactorsDataHandler(Callback<DemandFactor> callback) {
        super(callback)
    }

    DemandFactor data() {
        DemandFactor factor = new DemandFactor()
        factor.code = get('Code').getValue()
        factor.id = id;
        factor.description = get('Description').getValue()
        return factor
    }
}

// Коэффициенты одновременности
class DiversionFactorsDataHandler extends DataHandler<DiversionFactor> {


    DiversionFactorsDataHandler(Callback<DiversionFactor> callback) {
        super(callback)
    }

    DiversionFactor data() {
        DiversionFactor factor = new DiversionFactor()
        factor.code = get('Code').getValue()
        factor.id = id;
        factor.description = get('Description').getValue()
        return factor
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////

premisesType = null;
consumerType = null;
consumerSubtype = null;

premisesTypes = null;
consumerTypes = null;
consumerSubtypes = null;

unitLoads = null;

related = false;

int decimalPlace;

m = '';
resultP = 0;
P04 = 0;
P10 = 0;

Pz = '';

////////////////////////////////////////////////////////////////////////////////////////////////////////////

// Получить Тип помешений
static LinkedHashMap<String, PremiseType> getPremisesTypes(DataProvider dataProvider) {
    final LinkedHashMap<String, PremiseType> premisesTypes = new LinkedHashMap<>();
    dataProvider.getData("tehprisRSO_PremisesTypesPowerCalculator", new PremiseTypeDataHandler(
            { type -> premisesTypes.put(type.id, type) }));
    return premisesTypes;
}

// Получить Тип потребителя
static LinkedHashMap<String, ConsumerType> getConsumersTypes(DataProvider dataProvider, premiseType) {
    final LinkedHashMap<String, ConsumerType> сonsumersTypes = new LinkedHashMap<>();

    // Фильтруем по типу помешения
    FilterGroup filter = FilterGroup.single('PremisesType',
            FilterElement.FilterOperator.equals, Collections.singletonList(premiseType.id));

    dataProvider.getData("tehprisRSO_ConsumerTypesPowerCalculator", filter, new ConsumerTypeDataHandler(
            { type -> сonsumersTypes.put(type.id, type) }));
    return сonsumersTypes;
}

// Получить Тип потребителя
LinkedHashMap<String, ConsumerSubType> getConsumersSubTypes(DataProvider dataProvider, consumerType, related) {

    final LinkedHashMap<String, ConsumerSubType> сonsumersSubTypes = new LinkedHashMap<>();

    // Фильтруем по типу помешения
    FilterGroup filter = FilterGroup.single('ConsumerType',
            FilterElement.FilterOperator.equals, Collections.singletonList(consumerType.id));

    dataProvider.getData("tehprisRSO_ConsumerSubtypesPowerCalculator", filter, new ConsumerSubTypeDataHandler(
            { type -> сonsumersSubTypes.put(type.id, type) }, consumerType, related));
    return сonsumersSubTypes;
}

// Получить Тип потребителя
TreeMap<Integer, Double> getUnitLoad(DataProvider dataProvider, consumerSubType) {

    final TreeMap<Integer, Double> unitLoads = new TreeMap<>();

    // Фильтруем по типу помешения
    FilterGroup filter = FilterGroup.single('ConsumerSubtype',
            FilterElement.FilterOperator.equals, Collections.singletonList(consumerSubType.id));



    dataProvider.getData("tehprisRSO_UnitLoadPowerCalculator", filter, new UnitLoadDataHandler(
            { unit -> unitLoads.put(unit.amount, unit.value) }, consumerSubType));

    return unitLoads;


}

// Получить Тип потребителя
TreeMap<Double, Double> getDemandFactors(DataProvider dataProvider) {

    final TreeMap<Double, Double> demandFactors = new TreeMap<>();

    dataProvider.getData("tehprisRSO_DemandFactorsPowerCalculator", new DemandFactorsDataHandler(
            { factor -> demandFactors.put(Double.parseDouble(factor.code), Double.parseDouble(factor.description)) }));

    return demandFactors;
}

// Получить Тип потребителя
TreeMap<Double, Double> getDiversionFactors(DataProvider dataProvider) {

    final TreeMap<Double, Double> diversionFactors = new TreeMap<>();

    dataProvider.getData("tehprisRSO_DiversionFactorsPowerCalculator", new DiversionFactorsDataHandler(
            { factor -> diversionFactors.put(Double.parseDouble(factor.code), Double.parseDouble(factor.description)) }));

    return diversionFactors;

}

////////////////////////////////////////////////////////////////////////////////////////////////////////////

def firstFormula(DataProvider dataProvider, consumerSubType, m, Pz) {
    TreeMap<Integer, Double> map = getUnitLoad(dataProvider, consumerSubType)

    double p = 0;

    if (map.size() == 1) {
        p = map.get(map.firstKey());
    } else {
        double P1 = 0;
        double P2 = 0;

        double m0 = m < 3 ? 3 : m;
        int m1 = 0;
        int m2 = 0;
        for (Map.Entry<Integer, Double> entry : map.entrySet()) {
            m2 = entry.getKey();

            if (m0 == m2) {
                p = entry.getValue();
                break;
            } else if (m0 > m2) {
                m1 = entry.getKey();
                P1 = entry.getValue();
            } else {
                m2 = entry.getKey();
                P2 = entry.getValue();
                p = P1 + ((P2 - P1) * (m0 - m1)) / (m2 - m1);
                break;
            }
        }
    }

    return p * Double.parseDouble(m);
}

def secondFormula(DataProvider dataProvider, m, Pz) {
    TreeMap<Double, Double> map = getDemandFactors(dataProvider)

    double p1 = 0;
    double k1 = 0;
    for (Map.Entry<Double, Double> entry : map.entrySet()) {
        p1 = entry.getKey();

        if (Pz == p1) {
            k1 = entry.getValue();
            break;
        } else if (Pz < p1) {
            k1 = entry.getValue();
            break;
        }
    }

    map = getDiversionFactors(dataProvider)

    double p2 = 0;
    double k2 = 0;
    for (Map.Entry<Double, Double> entry : map.entrySet()) {
        p2 = entry.getKey();

        if (m == p2) {
            k2 = entry.getValue();
            break;
        } else if (m < p2) {
            k2 = entry.getValue();
            break;
        }
    }

    return Double.parseDouble(Pz) * m * k1 * k2;
}

def calc(dataProvider, consumerSubType, related, m, Pz) {

    if (related) {
        resultP = firstFormula(dataProvider, consumerSubType, m, Pz);
    } else {
        resultP = secondFormula(dataProvider, m, Pz);
    }

    dashboardContext.context['tehprisRSO_ELPowerCalc_resultP'] = resultP

    def P04 = resultP;
    dashboardContext.context['tehprisRSO_ELPowerCalc_p04'] = P04

    def P10 = P04 * 0.9;
    dashboardContext.context['tehprisRSO_ELPowerCalc_p10'] = P10

}

////////////////////////////////////////////////////////////////////////////////////////////////////////////

// Создаем поставшика и добавляем источник данных
DataProvider dataProvider = new DataProvider()
dataProvider.setExternalDataSource(externalDataSource)
dataProvider.setIntegrationsDataProvider(integrationsDataProvider)



if (dashboardContext.context['tehprisRSO_ELPowerCalc_m'] != null) {
    m = dashboardContext.context['tehprisRSO_ELPowerCalc_m']
}

if (dashboardContext.context['tehprisRSO_ELPowerCalc_pz'] != null) {
    Pz = dashboardContext.context['tehprisRSO_ELPowerCalc_pz']
}

// Заполняем список типов помешений
if (dashboardContext.context['tehprisRSO_ELPowerCalc_premisesTypes'] == null) {
    dashboardContext.context['tehprisRSO_ELPowerCalc_premisesTypes'] = getPremisesTypes(dataProvider)
}

// Заполняем список населенных пунктов мун.образования
if (dashboardContext.context['tehprisRSO_ELPowerCalc_selectPremiseType'] != null) {

    // Получаем выбранный тип помешения
    String selectPremiseType = dashboardContext.context['tehprisRSO_ELPowerCalc_selectPremiseType'];
    premisesTypes = dashboardContext.context['tehprisRSO_ELPowerCalc_premisesTypes']
    premisesType = premisesTypes.get(selectPremiseType)

    dashboardContext.context['tehprisRSO_ELPowerCalc_consumersTypes'] = getConsumersTypes(dataProvider, premisesType)
}

if (dashboardContext.context['tehprisRSO_ELPowerCalc_selectConsumerType'] != null) {

    // Получаем выбранный тип помешения
    String selectConsumerType = dashboardContext.context['tehprisRSO_ELPowerCalc_selectConsumerType'];
    consumersTypes = dashboardContext.context['tehprisRSO_ELPowerCalc_consumersTypes']
    consumerType = consumersTypes.get(selectConsumerType)

    dashboardContext.context['tehprisRSO_ELPowerCalc_related'] = related =
            !externalDataSource.getObjectList('tehprisRSO_ConsumerSubtypesPowerCalculator', null, null, null, null, null, null).isEmpty();


    dashboardContext.context['tehprisRSO_ELPowerCalc_consumersSubTypes'] = getConsumersSubTypes(dataProvider, consumerType, related)
}


if (dashboardContext.context['tehprisRSO_ELPowerCalc_selectConsumerSubType'] != null) {

    String selectConsumerSubType = dashboardContext.context['tehprisRSO_ELPowerCalc_selectConsumerSubType'];
    consumersSubTypes = dashboardContext.context['tehprisRSO_ELPowerCalc_consumersSubTypes']
    consumerSubType = consumersSubTypes.get(selectConsumerSubType)
    dashboardContext.context['tehprisRSO_ELPowerCalc_consumerSubType'] = consumerSubType
    decimalPlace = (consumerSubType.minValue -  consumerSubType.minValue) == 0 ? 0 : 1;
    dashboardContext.context['tehprisRSO_ELPowerCalc_decimalPlace'] = decimalPlace

    if (dashboardContext.context['tehprisRSO_ELPowerCalc_m']!= null
            ||  dashboardContext.context['tehprisRSO_ELPowerCalc_pz'] !=null) {


        def related = dashboardContext.context['tehprisRSO_ELPowerCalc_related']
        def m = dashboardContext.context['tehprisRSO_ELPowerCalc_m']
        def Pz = dashboardContext.context['tehprisRSO_ELPowerCalc_pz']

        calc(dataProvider, consumerSubType, related, m, Pz);

    }

}