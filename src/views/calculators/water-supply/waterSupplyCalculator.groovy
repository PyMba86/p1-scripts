import ru.osslabs.model.datasource.DataObject;
import ru.osslabs.model.datasource.DataObjectField;
import ru.osslabs.model.datasource.DataObjectList;
import java.util.LinkedHashMap;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import java.math.BigDecimal;
import java.math.RoundingMode;

/////////////////////////////////////////////////////////////////////////////////////////////////
// Калькулятор стоимости подключения водоснабжения
/////////////////////////////////////////////////////////////////////////////////////////////////

//Муниципальные образования
public class Municipality {
    private String id;
    private String code;
    private String description;

    public String getId() {
        return id
    }

    public void setId(String id) {
        this.id = id
    }

    public String getCode() {
        return code
    }

    public void setCode(String code) {
        this.code = code
    }

    public String getDescription() {
        return description
    }

    public void setDescription(String description) {
        this.description = description
    }
}

// Способ прокладки
public class WayOfLaying {
    private String id;
    private String code;
    private String description;

    public String getId() {
        return id
    }

    public void setId(String id) {
        this.id = id
    }

    public String getCode() {
        return code
    }

    public void setCode(String code) {
        this.code = code
    }

    public String getDescription() {
        return description
    }

    public void setDescription(String description) {
        this.description = description
    }
}

// Материал труб
public class MaterialOfPipe {
    private String id;
    private String code;
    private String description;

    public String getId() {
        return id
    }

    public void setId(String id) {
        this.id = id
    }

    public String getCode() {
        return code
    }

    public void setCode(String code) {
        this.code = code
    }

    public String getDescription() {
        return description
    }

    public void setDescription(String description) {
        this.description = description
    }
}

// Тип грунта
public class TypeOfSoil {
    private String id;
    private String code;
    private String description;

    public String getId() {
        return id
    }

    public void setId(String id) {
        this.id = id
    }

    public String getCode() {
        return code
    }

    public void setCode(String code) {
        this.code = code
    }

    public String getDescription() {
        return description
    }

    public void setDescription(String description) {
        this.description = description
    }
}

// Насселенные пункты
public class Locality {
    private String id;
    private String code;
    private String description;
    public Municipality municipality;
    public type;


    public String getId() {
        return id
    }

    public void setId(String id) {
        this.id = id
    }

    public String getCode() {
        return code
    }

    public void setCode(String code) {
        this.code = code
    }

    public String getDescription() {
        return description
    }

    public void setDescription(String description) {
        this.description = description
    }
}

// Ставка платы
class Rate {
    public String id;
    public String code;
    public String description;
    public Double min;
    public Double max;
    public String minSign;
    public String maxSign;
    public Double powerCurrent;
    public Double standCurrent;
}

// Тип деятельности
class TypeOfActivity {
    public String id;
    public String description;
}

// Организация
class Organization {
    public String id;
    public String description;
    public Locality locality;
    public String tenantId;


    public String getDescription() {
        return description
    }

    public void setDescription(String description) {
        this.description = description
    }
}

/////////////////////////////////////////////////////////////////////////////////////////////////

// Обработчик по получению данных из DataObject
// Получает обьект, Заполняет новый обьект по определенным полям
abstract class DataHandler<T> {

    private final Callback<T> callback;
    private DataProvider dataProvider;
    private int total;
    private int filtered;
    private LinkedHashMap<String, DataObjectField> fields;
    private String id;

    DataHandler(Callback<T> callback) {
        this.callback = callback;
    }

    void fill(LinkedHashMap<String, DataObjectField> fields) {
        this.fields = fields;

        T object = data();

        if (object != null) {
            callback.data(object)
        } else {
            filtered++
        }
        total++;
    }

    protected abstract T data();

    protected DataObjectField get(String name) {
        return fields.get(name)
    }

    String getId() {
        return id
    }

    void setId(String id) {
        this.id = id
    }

    void setDataProvider(DataProvider dataProvider) {
        this.dataProvider = dataProvider
    }

    DataProvider getDataProvider() {
        return dataProvider
    }

    static interface Callback<T> {
        void data(T object)
    }
}

// Обработчик по организациям
class OrganizationDataHandler extends DataHandler<Organization> {

    private Locality locality

    OrganizationDataHandler(Callback<Organization> callback, Locality locality) {
        super(callback)
        this.locality = locality
    }

    boolean containsLocality(DataObjectList localityDataObjectList) {
        for (localityDataObject in localityDataObjectList) {
            if (locality.id.equals(localityDataObject.id)) return true;
        }
        return false;
    }

    Organization data() {
        // Принимает ActivityOfOrganization
        DataObject orgDataObject = get('Tenant').getValue()
        DataObjectList localityDataObject = get('Locality').getValue()
        if (orgDataObject && containsLocality(localityDataObject)) {
            Organization organization = new Organization()
            organization.id = id;
            organization.tenantId = orgDataObject.id;
            organization.description = orgDataObject.getFields()
                    .get('Description').getValue()
            organization.locality = locality
            return organization

        } else {
            return null
        }

    }
}

// Обработчик по населенным пунктам
class LocalityDataHandler extends DataHandler<Locality> {

    private Municipality municipality

    LocalityDataHandler(Callback<Locality> callback, Municipality municipality) {
        super(callback)
        this.municipality = municipality
    }

    Locality data() {
        Locality locality = new Locality()
        locality.id = id;
        locality.code = get('Code').getValue() != null ? get('Code').getValue() : ''
        locality.description = get('Description').getValue()
        DataObject typeDataObject = get('TypeOfMunicipalFormation').getValue()
        locality.type = typeDataObject != null ? typeDataObject.getFields().get('Code').getValue() : ''
        return locality
    }
}

// Обработчик по муниципальным организациям
class MunicipalityDataHandler extends DataHandler<Municipality> {

    MunicipalityDataHandler(Callback<Municipality> callback) {
        super(callback)
    }

    Municipality data() {
        Municipality municipality = new Municipality()
        municipality.id = id;
        municipality.code = get('Code').getValue() != null ? get('Code').getValue() : ''
        municipality.description = get('Description').getValue()
        return municipality
    }
}

// Обработчик по способу прокладки
class WayOfLayingDataHandler extends DataHandler<WayOfLaying> {

    WayOfLayingDataHandler(Callback<WayOfLaying> callback) {
        super(callback)
    }

    WayOfLaying data() {
        WayOfLaying wayOfLaying = new WayOfLaying()
        wayOfLaying.id = id;
        wayOfLaying.code = get('Code').getValue() != null ? get('Code').getValue() : ''
        wayOfLaying.description = get('Description').getValue()
        return wayOfLaying
    }
}

// Обработчик материал труб
class MaterialOfPipeDataHandler extends DataHandler<MaterialOfPipe> {

    MaterialOfPipeDataHandler(Callback<MaterialOfPipe> callback) {
        super(callback)
    }

    MaterialOfPipe data() {
        MaterialOfPipe materialOfPipe = new MaterialOfPipe()
        materialOfPipe.id = id;
        materialOfPipe.code = get('Code').getValue() != null ? get('Code').getValue() : ''
        materialOfPipe.description = get('Description').getValue()
        return materialOfPipe
    }
}

// Обработчик материал труб
class TypeOfSoilDataHandler extends DataHandler<TypeOfSoil> {

    TypeOfSoilDataHandler(Callback<TypeOfSoil> callback) {
        super(callback)
    }

    TypeOfSoil data() {
        TypeOfSoil typeOfSoil = new TypeOfSoil()
        typeOfSoil.id = id;
        typeOfSoil.code = get('Code').getValue() != null ? get('Code').getValue() : ''
        typeOfSoil.description = get('Description').getValue()
        return typeOfSoil
    }
}

// Обработчик по ставкам платы
class RateDataHandler extends DataHandler<Rate> {

    WayOfLaying wayOfLaying;
    Double value;
    MaterialOfPipe materialOfPipe;
    TypeOfSoil typeOfSoil;


    RateDataHandler(Callback<Rate> callback, WayOfLaying wayOfLaying, MaterialOfPipe materialOfPipe,
                    TypeOfSoil typeOfSoil, String value) {
        super(callback)
        this.wayOfLaying = wayOfLaying;
        this.value = convertStringToDouble(value);
        this.materialOfPipe = materialOfPipe;
        this.typeOfSoil = typeOfSoil;
    }

    // Проверяем на принадлежность диапазону
    boolean inRange(Double firstValue, String sign, Double secondValue) {
        if (firstValue == null || secondValue == null) return true;
        switch (sign) {
            case "moreSrictly":
                return (firstValue > secondValue);
            case "moreEqual":
                return (firstValue >= secondValue);
            case "equalSrictly":
                final double epsilon = 0.001;
                return (Math.abs(firstValue - secondValue) < epsilon);
            case "lessSrictly":
                return (firstValue < secondValue);
            case "lessEqual":
                return (firstValue <= secondValue);
            default:
                return false;
        }
    }

    // id полей должны совпадать
    boolean check(String id, String nameField) {
        return id.equals(get(nameField).getValue().getId())
    }

    Rate data() {

        if (check(wayOfLaying.id, 'WayOfLaying')
                || check(typeOfSoil.id, 'TypeOfSoil')
                || check(materialOfPipe.id, 'MaterialOfPipe')) {

            // Ставка платы
            Rate rate = new Rate()
            rate.id = id;
            rate.code = get('Code').getValue() != null ? get('Code').getValue() : ''
            rate.description = get('Description').getValue()

            rate.min = convertStringToDouble(get('RateRangeMin').getValue())
            rate.max = convertStringToDouble(get('RateRangeMax').getValue())

            DataObject minSign = get('RateRangeMinSign').getValue()
            rate.minSign = minSign != null ? minSign.getFields().get('Code').getValue() : ''

            DataObject maxSign = get('RateRangeMaxSign').getValue()
            rate.maxSign = maxSign != null ? maxSign.getFields().get('Code').getValue() : ''

            rate.powerCurrent = convertStringToDouble(get('RatePowerCurrent').getValue())
            rate.standCurrent = convertStringToDouble(get('RateStandCurrent').getValue())

            return inRange(value, rate.minSign, rate.min) && inRange(value, rate.maxSign, rate.max) ? rate : null
        } else
            return null
    }

    Double convertStringToDouble(String value) {
        return (StringUtils.isNotBlank(value)
                && NumberUtils.isNumber(value)) ? Double.parseDouble(value) : null
    }
}

/////////////////////////////////////////////////////////////////////////////////////////////////

// Поставшик данных
class DataProvider {

    def externalDatasource;
    def integrationsDataProvider;

    void getData(DataObject dataObject, DataHandler handler) {
        // Передаем поставшика для того чтобы можно было получать данные из других сущностей
        handler.setDataProvider(this)
        // Добоавляем id записи
        handler.setId(dataObject.getId())
        // Вызывает кэлбэк после заполнения обьекта
        handler.fill(dataObject.getFields())
    }

    // Получаем данные из списка обьектов с обработкой их в хэндлере
    void getData(List<DataObject> dataObjectList, DataHandler handler) {
        for (DataObject dataObject : dataObjectList) {
            getData(dataObject, handler)
        }
    }

    // Получаем данные из сущности с обработкой их в хэнделере
    void getData(String entityName, DataHandler handler) {
        this.getData(this.externalDataSource.getObjectList(entityName, null)
                as List<DataObject>, handler)
    }

    void getData(LinkedHashMap<String, Object> entity, DataHandler handler) {
        // Получаем обьекты по фильтру
        List<DataObject> objectList = integrationsDataProvider
                .runSavedScript('tehprisRSO_getObjectEntity', [entity: entity])
        // Заполняем список
        this.getData(objectList, handler)
    }

    void setExternalDataSource(externalDatasource) {
        this.externalDatasource = externalDatasource
    }

    def getExternalDataSource() {
        return this.externalDatasource;
    }

    void setIntegrationsDataProvider(integrationsDataProvider) {
        this.integrationsDataProvider = integrationsDataProvider
    }

}

////////////////////////////////////////////////////////////////////////////////////////////////


interface Command<T> {
    // Выполнить команду
    void execute(DataProvider dataProvider);

    // Проверить выполение команды
    boolean check(MessageHandler handler);

    // Очистить выполнение команды
    void clear();

    T get();
}

interface MessageHandler {
    // Вывести сообщение
    void print(String status)

    // Очистить сообщение
    void clear();
}

class CommandInvoker {
    private List<Command> commands;
    DataProvider dataProvider;
    HashMap dashboardContext;
    boolean valid = true;
    private final MessageHandler handler;

    CommandInvoker(List<Command> commands, DataProvider dataProvider, HashMap dashboardContext,
                   MessageHandler messageHandler) {
        this.commands = commands
        this.dataProvider = dataProvider
        this.dashboardContext = dashboardContext
        this.handler = messageHandler;

    }

    void execute(boolean checked) {
        for (Command command : commands) {

            if (valid) {
                handler.clear();
                command.execute(dataProvider);
                if (checked && !command.check(handler)) {
                    valid = false;
                }
            } else {
                command.clear();
            }
        }
    }
}

abstract class MenuDataCommand<T> implements Command<T> {

    final String selectItem;
    final String selectItems;
    HashMap dashboardContext;


    MenuDataCommand(String selectItem, String selectItems, HashMap dashboardContext) {
        this.selectItem = selectItem
        this.selectItems = selectItems
        this.dashboardContext = dashboardContext
    }

    void clear() {
        dashboardContext[selectItems] = null
        dashboardContext[selectItem] = null
    }

    T get() {
        def items = dashboardContext[selectItems]
        String id = dashboardContext[selectItem];
        return items.get(id)
    }

    public boolean isNotValue(Object value) {
        if (value == null) {
            return true
        } else if (value instanceof HashMap) {
            return value.isEmpty()
        } else if (value instanceof String) {
            return value.isEmpty()
        }
        return false
    }
}

abstract class InputDataCommand implements Command<String> {
    final String item;
    HashMap dashboardContext;

    InputDataCommand(String item, HashMap dashboardContext) {
        this.item = item
        this.dashboardContext = dashboardContext
    }

    void execute(DataProvider dataProvider) {
        // нет реализации
    }

    void clear() {
        dashboardContext[item] = null
    }

    String get() {
        return dashboardContext[item];
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////

class MunicipalityCommand extends MenuDataCommand<Municipality> {


    MunicipalityCommand(String selectItem, String selectItems, HashMap dashboardContext) {
        super(selectItem, selectItems, dashboardContext)
        dashboardContext[selectItems] = new LinkedHashMap<>();
    }

    void execute(DataProvider dataProvider) {
        dashboardContext[selectItems] = getMunicipalities(dataProvider)

    }

    private LinkedHashMap<String, Municipality> getMunicipalities(DataProvider dataProvider) {
        final LinkedHashMap<String, Municipality> municipalities = new LinkedHashMap<>();
        dataProvider.getData("tehprisRSO_Municipalities", new MunicipalityDataHandler(
                { municipality -> municipalities.put(municipality.id, municipality) }));
        return municipalities;
    }

    boolean check(MessageHandler handler) {
        if (isNotValue(dashboardContext[selectItems])) {
            handler.print("Муниципальные образования не найдены");
            return false;
        }
        if (isNotValue(dashboardContext[selectItem])) {
            handler.print("Выберите муниципальное образование");
            return false;
        }
        return true;
    }
}


class LocalityCommand extends MenuDataCommand<Locality> {

    Command<Municipality> municipalityCommand;

    LocalityCommand(String selectItem, String selectItems, Command<Municipality> municipalityCommand, HashMap dashboardContext) {
        super(selectItem, selectItems, dashboardContext)
        this.municipalityCommand = municipalityCommand;
        dashboardContext[selectItems] = new LinkedHashMap<>();
    }

    void execute(DataProvider dataProvider) {
        if (municipalityCommand.get() != null) {
            dashboardContext[selectItems] = getLocality(dataProvider, municipalityCommand.get())
        } else {
            dashboardContext[selectItems] = null
        }

    }

    LinkedHashMap<String, Locality> getLocality(DataProvider dataProvider, Municipality municipality) {
        final LinkedHashMap<String, Locality> localities = new LinkedHashMap<>();

        // Создаем описание для фильтра по муниципалитету
        LinkedHashMap<String, Object> entityLocality = [
                link: 'tehprisRSO_Locality',
                data: [
                        Municipality: [link: 'tehprisRSO_Municipalities',
                                       data: [Id: municipality.id]]
                ]]

        // Получаем обьекты по фильтру с помощью сторонего скрипта
        List<DataObject> dataLocalityObjectList = dataProvider.integrationsDataProvider
                .runSavedScript('tehprisRSO_getObjectEntity', [entity: entityLocality])

        // Заполняем список населенных пунктов
        dataProvider.getData(dataLocalityObjectList,
                new LocalityDataHandler(
                        { locality -> localities.put(locality.id, locality) }, municipality))
        return localities;
    }


    boolean check(MessageHandler handler) {
        if (isNotValue(dashboardContext[selectItems])) {
            handler.print("Населенные пункты не найдены");
            return false;
        }
        if (isNotValue(dashboardContext[selectItem])) {
            handler.print("Выберите населенный пункт");
            return false;
        } else {

            def localities = dashboardContext[selectItems]
            def selectLocality = dashboardContext[selectItem]
            if (!localities.containsKey(selectLocality)) {
                return false;
            }
        }
        return true;
    }
}

class OrganizationCommand extends MenuDataCommand<Organization> {

    Command<Locality> localityCommand;

    OrganizationCommand(String selectItem, String selectItems, Command<Locality> localityCommand, HashMap dashboardContext) {
        super(selectItem, selectItems, dashboardContext)
        this.localityCommand = localityCommand;
        dashboardContext[selectItems] = new LinkedHashMap<>();
    }

    void execute(DataProvider dataProvider) {
        if (localityCommand.get() != null) {
            dashboardContext[selectItems] = getOrganization(dataProvider, localityCommand.get())
        } else {
            dashboardContext[selectItems] = null
        }

    }

    LinkedHashMap<String, Organization> getOrganization(DataProvider dataProvider, Locality locality) {
        final LinkedHashMap<String, Organization> organizations = new LinkedHashMap<>();

        // Создаем описание для фильтра по муниципалитету
        LinkedHashMap<String, Object> activityOfOrganizationEntity = [
                link: 'tehprisRSO_ActivityOfOrganization',
                data: [
                        TypeOfActivity:
                                [link: 'tehprisRSO_TypeOfActivity',
                                 data: [Code: 'VS']]
                ]]

        // Получаем обьекты по фильтру с помощью сторонего скрипта
        List<DataObject> dataOrganizationObjectList = dataProvider.integrationsDataProvider
                .runSavedScript('tehprisRSO_getObjectEntity', [entity: activityOfOrganizationEntity])

        // Заполняем список населенных пунктов
        dataProvider.getData(dataOrganizationObjectList,
                new OrganizationDataHandler(
                        { organization -> organizations.put(organization.tenantId, organization) }, locality))
        return organizations;
    }


    boolean check(MessageHandler handler) {
        if (isNotValue(dashboardContext[selectItems])) {
            handler.print("Организации не найдены");
            return false;
        }
        if (isNotValue(dashboardContext[selectItem])) {
            handler.print("Выберите организацию");
            return false;
        }
        return true;
    }
}


class WayOfLayingCommand extends MenuDataCommand<WayOfLaying> {


    WayOfLayingCommand(String selectItem, String selectItems, HashMap dashboardContext) {
        super(selectItem, selectItems, dashboardContext)
        dashboardContext[selectItems] = new LinkedHashMap<>();
    }

    void execute(DataProvider dataProvider) {

        dashboardContext[selectItems] = getWaysOfLaying(dataProvider)

    }

    private LinkedHashMap<String, WayOfLaying> getWaysOfLaying(DataProvider dataProvider) {
        final LinkedHashMap<String, WayOfLaying> waysOfLaying = new LinkedHashMap<>();
        dataProvider.getData("tehprisRSO_WayOfLayingVSScientificCalculator", new WayOfLayingDataHandler(
                { wayOfLaying -> waysOfLaying.put(wayOfLaying.id, wayOfLaying) }));
        return waysOfLaying;
    }


    boolean check(MessageHandler handler) {
        if (isNotValue(dashboardContext[selectItems])) {
            handler.print("Способы прокладки не найдены");
            return false;
        }
        if (isNotValue(dashboardContext[selectItem])) {
            handler.print("Выберите cпособ прокладки");
            return false;
        }
        return true;
    }
}


class MaterialOfPipeCommand extends MenuDataCommand<MaterialOfPipe> {


    MaterialOfPipeCommand(String selectItem, String selectItems, HashMap dashboardContext) {
        super(selectItem, selectItems, dashboardContext)
        dashboardContext[selectItems] = new LinkedHashMap<>();
    }

    void execute(DataProvider dataProvider) {
        dashboardContext[selectItems] = getMaterialsOfPipe(dataProvider)
    }

    private LinkedHashMap<String, MaterialOfPipe> getMaterialsOfPipe(DataProvider dataProvider) {
        final LinkedHashMap<String, MaterialOfPipe> materialsOfPipe = new LinkedHashMap<>();
        dataProvider.getData("tehprisRSO_MaterialOfPipeVSScientificCalculator", new MaterialOfPipeDataHandler(
                { materialOfPipe -> materialsOfPipe.put(materialOfPipe.id, materialOfPipe) }));
        return materialsOfPipe;
    }


    boolean check(MessageHandler handler) {
        if (isNotValue(dashboardContext[selectItems])) {
            handler.print("Материал труб не найдены");
            return false;
        }
        if (isNotValue(dashboardContext[selectItem])) {
            handler.print("Выберите материал труб");
            return false;
        }
        return true;
    }
}

class TypeOfSoilCommand extends MenuDataCommand<TypeOfSoil> {


    TypeOfSoilCommand(String selectItem, String selectItems, HashMap dashboardContext) {
        super(selectItem, selectItems, dashboardContext)
        dashboardContext[selectItems] = new LinkedHashMap<>();
    }

    void execute(DataProvider dataProvider) {
        dashboardContext[selectItems] = getTypesOfSoil(dataProvider)
    }

    private LinkedHashMap<String, TypeOfSoil> getTypesOfSoil(DataProvider dataProvider) {
        final LinkedHashMap<String, TypeOfSoil> typesOfSoil = new LinkedHashMap<>();
        dataProvider.getData("tehprisRSO_TypeOfSoilVSScientificCalculator", new TypeOfSoilDataHandler(
                { typeOfSoil -> typesOfSoil.put(typeOfSoil.id, typeOfSoil) }));
        return typesOfSoil;
    }


    boolean check(MessageHandler handler) {
        if (isNotValue(dashboardContext[selectItems])) {
            handler.print("Тип грунта не найдены");
            return false;
        }
        if (isNotValue(dashboardContext[selectItem])) {
            handler.print("Выберите тип грунта");
            return false;
        }
        return true;
    }
}

class DiameterCommand extends InputDataCommand {

    DiameterCommand(String item, HashMap dashboardContext) {
        super(item, dashboardContext)
    }

    boolean check(MessageHandler handler) {
        if (!dashboardContext[this.item]) {
            handler.print("Введите диаметр");
            return false;
        }

        if (convertToDouble(dashboardContext[this.item]) < 0) {
            handler.print("Диаметр не может быть отрицательным");
            return false;
        }

        return true
    }

    Double convertToDouble(Object value) {
        return Double.parseDouble(value)
    }
}

class DistanceCommand extends InputDataCommand {

    DistanceCommand(String item, HashMap dashboardContext) {
        super(item, dashboardContext)
    }

    boolean check(MessageHandler handler) {
        if (!dashboardContext[this.item]) {
            handler.print("Введите расстояние");
            return false;
        }

        if (convertToDouble(dashboardContext[this.item]) < 0) {
            handler.print("Расстояние не может быть отрицательным");
            return false;
        }

        return true
    }

    Double convertToDouble(Object value) {
        return Double.parseDouble(value)
    }
}

class CalculateCommand implements Command<List<String>> {

    HashMap dashboardContext
    OrganizationCommand organizationCommand;
    WayOfLayingCommand wayOfLayingCommand;
    MaterialOfPipeCommand materialOfPipeCommand;
    TypeOfSoilCommand typeOfSoilCommand;
    DistanceCommand distanceCommand;
    DiameterCommand diameterCommand;

    final String summary;
    final String summaryVAT;
    final String summaryWithVAT;

    CalculateCommand(String summary, String summaryVAT, String summaryWithVAT, HashMap dashboardContext,
                     OrganizationCommand organizationCommand, WayOfLayingCommand wayOfLayingCommand,
                     MaterialOfPipeCommand materialOfPipeCommand, TypeOfSoilCommand typeOfSoilCommand,
                     DistanceCommand distanceCommand, DiameterCommand diameterCommand) {
        this.dashboardContext = dashboardContext
        this.organizationCommand = organizationCommand
        this.wayOfLayingCommand = wayOfLayingCommand
        this.materialOfPipeCommand = materialOfPipeCommand
        this.typeOfSoilCommand = typeOfSoilCommand
        this.diameterCommand = diameterCommand
        this.distanceCommand = distanceCommand
        this.summary = summary
        this.summaryVAT = summaryVAT
        this.summaryWithVAT = summaryWithVAT
    }

    void execute(DataProvider dataProvider) {

        def rates = new ArrayList<>();

        // Получаем ставки платы
        dataProvider.getData([
                link: 'tehprisRSO_CalcRateVSScientificCalculator',
                data: [
                        Tenant:
                                [link: 'Tenant',
                                 data: [Id: organizationCommand.get().tenantId]]
                ]],
                new RateDataHandler(
                        { rate -> rates.add(rate) }, wayOfLayingCommand.get()
                        , materialOfPipeCommand.get(), typeOfSoilCommand.get(), diameterCommand.get()))

        if (rates.size() > 0) {
            Double price = rates.getAt(0).powerCurrent * Double.valueOf(distanceCommand.get())
            Double nds = price - price / 1.18
            dashboardContext[summary] = round(price / 1.18, 2)
            dashboardContext[summaryVAT] = round(nds, 2)
            dashboardContext[summaryWithVAT] = round(price, 2)
        } else {
            clear();
        }

    }

    boolean check(MessageHandler handler) {
        if (isNotValue(dashboardContext[summary])) {
            handler.print("Ставки платы для данной организации не найдены");
            return false;
        }
        return true;
    }


    void clear() {
        dashboardContext[summary] = null
        dashboardContext[summaryVAT] = null
        dashboardContext[summaryWithVAT] = null
    }

    List<String> get() {
        return null
    }

    double round(double value, int places) {
        BigDecimal bd = new BigDecimal(value);
        bd = bd.setScale(places, RoundingMode.HALF_UP);
        return bd.doubleValue();
    }

    boolean isNotValue(Double value) {
        if (value == null) {
            return true
        }
        return false
    }
}

// Обработчик вывода сообщений в dashboard
class MessageDashboardHandler implements MessageHandler {

    final String field;
    HashMap dashboardContext

    MessageDashboardHandler(HashMap dashboardContext, String field) {
        this.field = field
        this.dashboardContext = dashboardContext
    }

    void print(String status) {
        dashboardContext[field] = status
    }

    void clear() {
        dashboardContext[field] = null
    }
}


DataProvider dataProvider = new DataProvider()
dataProvider.setExternalDataSource(externalDataSource)
dataProvider.setIntegrationsDataProvider(integrationsDataProvider)

MunicipalityCommand municipalityCommand = new MunicipalityCommand(
        "tehprisRSO_WaterSupplyCalc_selectMunicipality",
        "tehprisRSO_WaterSupplyCalc_municipalities", dashboardContext.context)

LocalityCommand localityCommand = new LocalityCommand(
        "tehprisRSO_WaterSupplyCalc_selectLocality",
        "tehprisRSO_WaterSupplyCalc_localities",
        municipalityCommand, dashboardContext.context)

OrganizationCommand organizationCommand = new OrganizationCommand(
        "tehprisRSO_WaterSupplyCalc_selectOrganization",
        "tehprisRSO_WaterSupplyCalc_organizations",
        localityCommand, dashboardContext.context)

WayOfLayingCommand wayOfLayingCommand = new WayOfLayingCommand(
        "tehprisRSO_WaterSupplyCalc_selectWayOfLaying",
        "tehprisRSO_WaterSupplyCalc_waysOfLaying", dashboardContext.context)

MaterialOfPipeCommand materialOfPipeCommand = new MaterialOfPipeCommand(
        "tehprisRSO_WaterSupplyCalc_selectMaterialOfPipe",
        "tehprisRSO_WaterSupplyCalc_materialsOfPipe", dashboardContext.context)

TypeOfSoilCommand typeOfSoilCommand = new TypeOfSoilCommand(
        "tehprisRSO_WaterSupplyCalc_selectTypeOfSoil",
        "tehprisRSO_WaterSupplyCalc_typesOfSoil", dashboardContext.context)

DiameterCommand diameterCommand = new DiameterCommand(
        "tehprisRSO_WaterSupplyCalc_diameter",
        dashboardContext.context)

DistanceCommand distanceCommand = new DistanceCommand(
        "tehprisRSO_WaterSupplyCalc_distance",
        dashboardContext.context)

CalculateCommand calculateCommand = new CalculateCommand(
        "tehprisRSO_WaterSupplyCalc_summary",
        "tehprisRSO_WaterSupplyCalc_summaryVAT",
        "tehprisRSO_WaterSupplyCalc_summaryWithVAT",
        dashboardContext.context,
        organizationCommand, wayOfLayingCommand, materialOfPipeCommand, typeOfSoilCommand,
        distanceCommand, diameterCommand)

List<Command> commands = new ArrayList<Command>();
commands.add(municipalityCommand)
commands.add(localityCommand)
commands.add(organizationCommand)
commands.add(wayOfLayingCommand)
commands.add(materialOfPipeCommand)
commands.add(typeOfSoilCommand)
commands.add(diameterCommand)
commands.add(distanceCommand)
commands.add(calculateCommand)

CommandInvoker invoker = new CommandInvoker(commands, dataProvider, dashboardContext.context,
        new MessageDashboardHandler(dashboardContext.context, "tehprisRSO_WaterSupplyCalc_alert"))

invoker.execute(true);