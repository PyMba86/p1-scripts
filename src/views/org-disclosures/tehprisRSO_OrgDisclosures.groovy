import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import ru.osslabs.model.common.filter.FilterGroup;
import ru.osslabs.model.common.filter.FilterElement;

def paramMap = javax.faces.context.FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap()

sectionParamCode = paramMap.get('code')

class Section {
    public String name;
    public String link;
    public children;
    public String type;
    public String code;
    public boolean active;

    Section(String code, String name) {
        this.name = name
        this.code = code
        this.children = []
    }


    void setActive(boolean active) {
        this.active = active
    }

}


Section traverseSection(Object obj, boolean active = false, Section children = null) {

    def section = new Section(obj.fields.Code.value, obj.fields.Description.value)

    section.setActive(active)

    // Если у секции есть потомки
    if (children) {
        section.children.push(children)
    }
    // Если есть родитель, то добавляем данный пункт к родителю и возвращаем,
    // если нет возраващем этот
    def parent = obj.fields.ParentSection.value
    if (parent) {
        return traverseSection(parent, active, section)
    }
    return section;
}

// Соединяем с родителями, если есть такие
void createSection(Object obj, HashMap<String, Section> menu) {
    // Создаем отдельные секции
    def section = traverseSection(obj, sectionParamCode.equals(obj.fields.Code.value))
    // Склеиваем их вместе
    def sectionMenu = menu.get(section.code) as Section

    if (sectionMenu) {

        // Устанавливаем активность секции, если в ней есть активные дочерние секции
        sectionMenu.active = section.active != sectionMenu.active ? true : false

        // Добавляем дочерние секции
        for (children in section.children) {
            sectionMenu.children.add(children)
        }
    } else {
        menu.put(section.code, section)
    }
}


def TypeOfActivityParamId = null //  Если не указан то по элеткромнабжению

if (dashboardContext.context['tehprisRSO_TypeOfActivity'] == null && (paramMap.get('type') != null)) {
    TypeOfActivityParamId = paramMap.get('type')

}

def TenantParamId = null
if (dashboardContext.context['tehprisRSO_Tenant'] == null && (paramMap.get('tenant') != null)) {
    TenantParamId = paramMap.get('tenant')
}


typeOfActivity_codes = [:]
if (dashboardContext.context['typeOfActivity_list'] == null) {
    typeOfActivity_list = externalDataSource.getObjectList("tehprisRSO_TypeOfActivity", null)
    typeOfActivity_listNew = []
    typeOfActivity_ids = []
    typeOfActivity_list.each {
        typeOfActivity_listNew.add([it.id, it.fields.Description.value])
        typeOfActivity_ids.add(it.id)
        typeOfActivity_codes.put(it.id, it.fields.Code.value)
    }

    if (typeOfActivity_ids.contains(TypeOfActivityParamId)) {
        TypeOfActivityParamId = TypeOfActivityParamId
    } else {
        TypeOfActivityParamId = typeOfActivity_ids[0]
    }

    dashboardContext.context['typeOfActivity_list'] = typeOfActivity_listNew
    dashboardContext.context['typeOfActivity'] = TypeOfActivityParamId
}


tenant_codes = [:]
if (dashboardContext.context['tenant_list'] == null) {
    def rootTenantFilter = FilterGroup.single('Root', FilterElement.FilterOperator.equals, [true])
    tenant_list = externalDataSource.getObjectList("Tenant", null, rootTenantFilter, null, null, null, null)
    tenant_listNew = []
    tenant_ids = []

    tenant_list.each {
        tenant_listNew.add([it.id, it.fields.Description.value])
        tenant_ids.add(it.id)
        tenant_codes.put(it.id, it.fields.Code.value)
    }

    if (tenant_ids.contains(TenantParamId)) {
        TenantParamId = TenantParamId
    } else {
        TenantParamId = tenant_ids[0]
    }

    dashboardContext.context['tenant_list'] = tenant_listNew
    dashboardContext.context['tenant'] = TenantParamId
}


if (TypeOfActivityParamId) {

    def sectionEntity = [
            link: 'tehprisRSO_OrgSectionDisclosures',
            data: [
                    TypeOfActivity: [
                            link: 'tehprisRSO_TypeOfActivity',
                            data: [
                                    Id: TypeOfActivityParamId
                            ]
                    ],
                    Inactive      : false
            ]]

    def sectionList = integrationsDataProvider.runSavedScript('tehprisRSO_getObjectEntity', [entity: sectionEntity])

    try {

        browserConsole.info(sectionList.fields.join(','))

        // Сгруппировать секции
        def sectionGroupByList = sectionList.sort({section -> section.fields.Num.value})

        browserConsole.info(sectionGroupByList.toString())

    } catch (Exception ex) {
        browserConsole.info(ex.getMessage())
    }
    // Древовидное меню: код раздела/ раздел
    def HashMap<String, Section> menu = new HashMap<String, Section>()

    // Построение меню: Проходимся по всем разделам контена
    // Заполняем навигационное меню
    for (section in sectionList) {
        createSection(section.value, menu)
    }
    Gson gson = new GsonBuilder().disableHtmlEscaping().create();

    dashboardContext.context['menu'] = gson.toJson(menu)

    if (dashboardContext.context['RSO_Info'] == null && (sectionParamCode != null)) {

        // Формируем список разделов

        sectionParamCode = paramMap.get('code')
        def entityData = []
        if (menu.get(sectionParamCode)) {
            entityData.add(Section: [link: 'tehprisRSO_OrgSectionDisclosures',
                                     data: [Code: sectionParamCode]])
        }

        if (TypeOfActivityParamId && TenantParamId) {

            def activityOfOrganizationEntity = [
                    link: 'tehprisRSO_ActivityOfOrganization',
                    data: [
                            TypeOfActivity: [link: 'tehprisRSO_TypeOfActivity',
                                             data: [Id: TypeOfActivityParamId]],
                            Tenant        : [link: 'Tenant',
                                             data: [Id: TenantParamId]]
                    ]]

            def activityOfOrganizationObject = integrationsDataProvider.runSavedScript('tehprisRSO_getObjectEntity', [entity: activityOfOrganizationEntity]).getAt(0)

            if (activityOfOrganizationObject) {
                def contentEntity = [
                        link: 'tehprisRSO_OrgContentDisclosures',
                        data: [
                                ActivityOfOrganization: [link: 'tehprisRSO_ActivityOfOrganization', data: [Id: activityOfOrganizationObject.id],
                                ], Section            : [link: 'tehprisRSO_OrgSectionDisclosures',
                                                         data: [Code: sectionParamCode]]
                        ]]

                def contentList = integrationsDataProvider.runSavedScript('tehprisRSO_getObjectEntity', [entity: contentEntity])

                def infoList = []

                for (def info in contentList) {

                    def periodCode = info.fields.PeriodCode.value ? info.fields.PeriodCode?.value.fields.Description.value : ''

                    infoList.add([
                            info.fields.Section?.value.fields.Description,
                            info.fields.PublishDate,
                            periodCode,
                            '/platform/dmsfile/' + info.fields.File.value.get(0).uid])
                }

                dashboardContext.context['info'] = infoList
            }
        }
    }
}
