class Section {
    public String name;
    public String link;
    public children;

    Section(String code, String name) {
        this.name = name
        this.code = code
        this.children = []
    }


    void setActive(boolean active) {
        this.active = active
    }

}

Section traverseSection(Object obj, boolean active = false, Section children = null) {

    def section = new Section(obj.fields.Code.value, obj.fields.Description.value)

    section.setActive(active)

    // Если у секции есть потомки
    if (children) {
        section.children.push(children)
    }
    // Если есть родитель, то добавляем данный пункт к родителю и возвращаем,
    // если нет возраващем этот
    def parent = obj.fields.ParentSection.value
    if (parent) {
        return traverseSection(parent, active, section)
    }
    return section;
}

// Соеденяем с родителями, если есть такие
void createSection(Object obj, HashMap<String, Section> menu) {
    // Создаем отдельные секции
    def section = traverseSection(obj, sectionParamCode.equals(obj.fields.Code.value))
    // Склеиваем их вместе
    def sectionMenu = menu.get(section.code) as Section

    if (sectionMenu) {

        // Устанавливаем активность секции, если в ней есть активные дочерние секции
        sectionMenu.active = section.active != sectionMenu.active ? true : false

        // Добавляем дочерние секции
        for (children in section.children) {
            sectionMenu.children.add(children)
        }
    } else {
        menu.put(section.code, section)
    }
}

// Построение меню: Проходимся по всем разделам контена
// Заполняем навигационное меню
for (section in sectionList) {
    createSection(section.value, menu)
}

//dashboardsSearch.dashBoardsTree.getChildren()[0].isPartialSelected()
dashboardsSearch.dashBoardsTree.getChildren()[0].getData().metaClass.methods*.name.sort().unique()

// getEnabled - активный боард
// getUnsecured  - публичный
// getChildren - предки
// getCode - код
// getName - название боарда
// getParent - родитель