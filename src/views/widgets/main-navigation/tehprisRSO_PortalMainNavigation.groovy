// Получение списка типов услуг

if(dashboardContext.context['PortalMainNavigation'] == null) {
    mainNavigationObjectList = externalDataSource.getObjectList("tehprisRSO_pref_PortalMainNavigation", null,null,null,null,'Num asc',null)

    def menu = []

    mainNavigationObjectList.each{
        menu.add([it.fields.Code.value, it.fields.Description.value, '../dmsfile/' + it.fields.Icon.value.get(0).uid])
    }

    dashboardContext.context['PortalMainNavigation'] = menu
}
