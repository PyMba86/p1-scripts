breadcrumb = []

def recursive(dashboard) {

    breadcrumb.add([dashboard.getName(), dashboard.getCode(), dashboard.getCenterDashes().isEmpty()]);
    def parent = dashboard.getParent()
    if (parent) {
        recursive(parent)
    }
}

recursive(dashboardController.instance)

dashboardContext.context['breadcrumb'] =breadcrumb.reverse()