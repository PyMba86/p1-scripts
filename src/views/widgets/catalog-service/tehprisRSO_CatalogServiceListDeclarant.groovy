import javax.faces.component.html.HtmlPanelGrid
import javax.faces.component.html.HtmlOutputText
import org.primefaces.component.column.Column
import org.primefaces.component.outputlabel.OutputLabel
import org.primefaces.component.link.Link
import ru.osslabs.model.common.filter.FilterGroup
import java.sql.Date

public class Item {
    public String form;
    public String description;
    public String type;
    public String button;

    public Item(String description, String form, String type, String button) {
        this.description = description;
        this.form = form;
        this.type = type;
        this.button = button;
    }

    public String getType() {
        return this.type;
    }

    public String getForm() {
        return this.form;
    }

    public String getDescription() {
        return this.description;
    }

    public String getButton() {
        return this.button;
    }

}

def data = externalDataSource.getObjectList('tehprisRSO_CatalogueServices',null, null, 0, null, 'Num asc', null)
List<Item> items = new ArrayList<Item>();

data.each{
    items.add(new Item(it.fields.Description.valueLabel, it.fields.Form.valueLabel, it.fields.ServiceType.valueLabel,  it.fields.Button.value))
}

dashboardContext.set('RSO_CatalogueServices', items)