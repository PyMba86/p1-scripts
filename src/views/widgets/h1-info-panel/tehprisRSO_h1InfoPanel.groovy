import javax.servlet.http.Cookie
import ru.osslabs.model.datasource.DataObject;
import ru.osslabs.model.datasource.DataObjectField;
import ru.osslabs.model.datasource.DataObjectList;
import javax.faces.context.FacesContext
import javax.faces.context.ExternalContext

// Муниципальное образование
public class Municipality {
    private String code;
    private String description;

    public Municipality(String code, String description) {
        this.code = code;
        this.description = description
    }

    public String getCode() {
        return code
    }

    public void setCode(String code) {
        this.code = code
    }

    public String getDescription() {
        return description
    }

    public void setDescription(String description) {
        this.description = description
    }
}

// Получаем контекст портала
ExternalContext externalContext = javax.faces.context.FacesContext.getCurrentInstance().getExternalContext()

// Создаем пустую муниципальную обр.
Municipality municipality = new Municipality('0', 'Местоположение')
LinkedHashMap<String, Municipality>  municipalities = new LinkedHashMap<>();

// Получаем муниципальные образования
List<DataObject> municipalitiesObjectList = externalDataSource.getObjectList("tehprisRSO_Municipalities", null)

String cookieLocalityId = new String()

// Получаем значение из куки
Cookie cookie = (Cookie) externalContext.getRequestCookieMap().get('MUNICIPALITY_ID');
if (cookie != null) {
    cookieLocalityId = URLDecoder.decode(cookie.getValue(), "UTF-8");
}
municipalitiesObjectList.each {
    String code = it.fields.Code.value
    String description = it.fields.Description.value
    // В случае если code совпадает, вытаскиваем значения обьекта
    if (cookieLocalityId.equals(code)) {
        municipality.setCode(code)
        municipality.setDescription(description)
    }
    municipalities.put(code, new Municipality(code,description))
}

dashboardContext.context['selectMunicipality'] = municipality
dashboardContext.context['municipalities'] = municipalities


