def paramMap = javax.faces.context.FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap()

public class Link implements Comparable<Link> {
    public String description;
    public Integer num;
    public String url;
    public String icon;

    public Link(String description, String icon, Integer num, String url) {
        this.description = description;
        this.num = num;
        this.url = url;
        this.icon = icon
    }

    public int getNum() {
        return this.num;
    }

    public String getDescription() {
        return this.description;
    }

    public String getIcon() {
        return this.icon;
    }

    public String getUrl() {
        return this.url;
    }

    @Override
    public int compareTo(Link that) {
        int link1Num = this.getNum();
        int link2Num = that.getNum();

        if (link1Num > link2Num) {
            return 1;
        } else if (link1Num < link2Num) {
            return -1;
        } else {
            return 0;
        }
    }
}

if ( dashboardContext.context['tehprisRSO_Services'] == null) {

    String title = 'Каталог услуг'

    if((paramMap.get('code') != null)) {
        def textMap = [:]
        // Получение данных
        def CatalogServicesParamId = paramMap.get('code')

        def entityCatalogs = [
                link: 'tehprisRSO_CatalogueServices',
                data: [
                        ServiceType		          : [link: 'tehprisRSO_CatalogServiceTypes',
                                                        data: [Code:CatalogServicesParamId]]
                ]]

        def ServicesObjectList =  integrationsDataProvider.runSavedScript('tehprisRSO_getObjectEntity', [entity : entityCatalogs])
        List<Link> links = new ArrayList<Link>();
        ServicesObjectList.each{
            def url =   it.fields.Code.value;
            links.add(new Link(it.fields.Description.value, '../dmsfile/'+ it.fields.ServiceType.value.fields.Icon.value.get(0).uid, it.fields.Num.value,url))
        }

        links.sort()
        title =  ServicesObjectList[0].fields.ServiceType.value
        dashboardContext.context['tehrpisRSO_ServiceLinks'] = links
        dashboardContext.context['tehrpisRSO_ServiceType'] = ServicesObjectList.isEmpty() ? '' : ServicesObjectList[0].fields.ServiceType.value
    } else  {
        catalogServiceTypesObjectList = externalDataSource.getObjectList("tehprisRSO_CatalogServiceTypes", null,null,null,null,'Num asc',null)
        catalogServiceTypesList = []
        catalogServiceTypesObjectList.each{
            catalogServiceTypesList.add([it.fields.Code.value, '../dmsfile/' + it.fields.Icon.value.get(0).uid,it.fields.Description.value])
        }
        dashboardContext.context['tehprisRSO_ServiceTypeList'] = catalogServiceTypesList
    }

    dashboardContext.context['tehprisRSO_ServiceTypeTitle'] = title
}