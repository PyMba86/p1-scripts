// Получение параметров url
def paramMap = javax.faces.context.FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap()

public class Item implements Comparable<Item> {
    public String name;
    public String description;
    public Integer num;

    public Item(String name, String description, Integer num) {
        this.description = description;
        this.num = num;
        this.name = name;
    }

    public int getNum() {
        return this.num;
    }

    public String getDescription() {
        return this.description;
    }

    public String getName() {
        return this.name;
    }

    @Override
    public int compareTo(Item that) {
        int link1Num = this.getNum();
        int link2Num = that.getNum();

        if (link1Num > link2Num) {
            return 1;
        } else if (link1Num < link2Num) {
            return -1;
        } else {
            return 0;
        }
    }
}


if(dashboardContext.context['tehprisRSO_ServiceCardList'] == null && (paramMap.get('code') != null)) {
    def textMap = [:]
    // Получение данных
    def CatalogServicesParamId = paramMap.get('code')

    def entityCatalogs = [
            link: 'tehprisRSO_CatalogServiceParameters',
            data: [
                    CatalogService		          : [link: 'tehprisRSO_CatalogueServices',
                                                       data: [Code:CatalogServicesParamId]]
            ]]

    def ServicesObjectList =  integrationsDataProvider.runSavedScript('tehprisRSO_getObjectEntity', [entity : entityCatalogs])
    List<Item> items = new ArrayList<Item>();
    ServicesObjectList.each{
        items.add(new Item(it.fields.Description.value, it.fields.Value.value, it.fields.Num.value))
    }

    items.sort()
    dashboardContext.context['tehprisRSO_ServiceList'] = items
    def Service = ServicesObjectList[0].fields.CatalogService.value
    dashboardContext.context['tehprisRSO_Service'] = Service.fields.Description.value
    dashboardContext.context['tehprisRSO_AnonymousService'] = Service.fields.Description.value && securityDataProvider.isLoggedIn()

    dashboardContext.context['tehprisRSO_ServiceForm'] = Service.fields.Form.value
    dashboardContext.context['tehprisRSO_ServiceButton'] = Service.fields.Button.value
}
