def paramMap = javax.faces.context.FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap()

public class Link implements Comparable<Link> {
    public String description;
    public Integer num;
    public String url;

    public Link(String description, Integer num, String url) {
        this.description = description;
        this.num = num;
        this.url = url;
    }

    public int getNum() {
        return this.num;
    }

    public String getDescription() {
        return this.description;
    }

    public String getUrl() {
        return this.url;
    }

    @Override
    public int compareTo(Link that) {
        int link1Num = this.getNum();
        int link2Num = that.getNum();

        if (link1Num > link2Num) {
            return 1;
        } else if (link1Num < link2Num) {
            return -1;
        } else {
            return 0;
        }
    }
}

if ( dashboardContext.context['tehprisRSO_Documents'] == null) {

    String title = 'Законодательство и нормативные документы'

    if((paramMap.get('code') != null)) {
        def textMap = [:]
        // Получение данных
        def CatalogServicesParamId = paramMap.get('code')

        def entityCatalogs = [
                link: 'tehprisRSO_CatalogueDocuments',
                data: [
                        DocumentType		          : [link: 'tehprisRSO_CatalogDocumentTypes',
                                                         data: [Code:CatalogServicesParamId]]
                ]]

        def ServicesObjectList =  integrationsDataProvider.runSavedScript('tehprisRSO_getObjectEntity', [entity : entityCatalogs])
        List<Link> links = new ArrayList<Link>();
        ServicesObjectList.each{
            def url =  '/platform/dmsfile/'+it.fields.File.value.get(0).uid;
            links.add(new Link(it.fields.Description.value, it.fields.Num.value,url))
        }

        links.sort()
        title =  ServicesObjectList[0].fields.DocumentType.value
        dashboardContext.context['tehrpisRSO_DocumentLinks'] = links
        dashboardContext.context['tehrpisRSO_DocumentType'] = ServicesObjectList[0].fields.DocumentType.value
    } else  {
        catalogServiceTypesObjectList = externalDataSource.getObjectList("tehprisRSO_CatalogDocumentTypes", null,null,null,null,'Num asc',null)
        catalogServiceTypesList = []
        catalogServiceTypesObjectList.each{
            catalogServiceTypesList.add([it.fields.Code.value, '../dmsfile/' + it.fields.Icon.value.get(0).uid,it.fields.Description.value])
        }
        dashboardContext.context['tehprisRSO_DocumentTypeList'] = catalogServiceTypesList
    }

    dashboardContext.context['tehprisRSO_DocumentTypeTitle'] = title
}

