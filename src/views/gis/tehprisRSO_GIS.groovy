if (dashboardContext.context['rso_map_json'] == null){
    // Типы деятельности (ex. Электроснабжение, Воодотведение, etc)
    TypeOfActivity = externalDataSource.getObjectList("tehprisRSO_TypeOfActivity", null)
    // Муниципалитеты
    Municipalities = externalDataSource.getObjectList("tehprisRSO_Municipalities", null)
    // Населенные пункты
    Locality = externalDataSource.getObjectList("tehprisRSO_Locality", null)


    layerList = [
            [
                    id: {'RSO_OrganizationPolygonMain'},
                    name: {'Зоны ответственности организаций'}
            ],
            [
                    id: {'RSO_OfficeOfOrganization'},
                    name: {'Офисы организаций'}
            ],
            [
                    id: {'RSO_Points'},
                    name: {'Точки подключения'}
            ]
    ]


    def jsonBuilder = new groovy.json.JsonBuilder({
        "TypeOfActivity" TypeOfActivity.collect {[
                id: it.id,
                name: it.fields.Description.value,
                activityTable: it.fields.SupplyCenterEntityName.value,
        ]}
        "Municipalities" Municipalities.collect {[
                id: it.id,
                name: it.fields.Description.value
        ]}
        "Locality" Locality.collect  {[
                id: it.id,
                name: it.fields.Description.value,
                municipalities_id:  it.fields.Municipality.value?.id
                // municipalities_name: it.fields.Municipality.value?.Description
        ]}
        "layerList" layerList.collect {[
                id: it.id(),
                name: it.name()
        ]}
    })

    dashboardContext.context['rso_map_json'] =  jsonBuilder.toPrettyString()
}