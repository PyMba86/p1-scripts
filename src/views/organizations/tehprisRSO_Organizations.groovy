import ru.osslabs.model.common.filter.FilterGroup
import ru.osslabs.model.common.filter.FilterElement

// Получение списка типов услуг
if (dashboardContext.context['TypeOfActivityList'] == null) {
    catalogServiceTypesObjectList = externalDataSource.getObjectList("tehprisRSO_TypeOfActivity", null, null, null, null, 'Num asc', null)
    catalogServiceTypesList = []
    catalogServiceTypesObjectList.each {
        catalogServiceTypesList.add([it.fields.Code.value, '../dmsfile/' + it.fields.Icon.value.get(0).uid, it.fields.Description.value])
    }
    dashboardContext.context['TypeOfActivityList'] = catalogServiceTypesList
}


if (dashboardContext.context['tehprisRSO_Organizations_OrganizationListActive'] == null) {
    dashboardContext.context['tehprisRSO_Organizations_OrganizationListActive'] = false
}

// Насселенные пункты
public class Locality {
    private String id;
    private String code;
    private String description;
    private String type;

    public String getId() {
        return id
    }

    public void setId(String id) {
        this.id = id
    }

    public String getCode() {
        return code
    }

    public void setCode(String code) {
        this.code = code
    }

    public String getDescription() {
        return description
    }

    public void setDescription(String description) {
        this.description = description
    }
}

//Муниципальные образования
public class Municipality {
    private String id;
    private String code;
    private String description;

    public String getId() {
        return id
    }

    public void setId(String id) {
        this.id = id
    }

    public String getCode() {
        return code
    }

    public void setCode(String code) {
        this.code = code
    }

    public String getDescription() {
        return description
    }

    public void setDescription(String description) {
        this.description = description
    }
}

// Обработчик по получению данных из DataObject
// Получает обьект, Заполняет новый обьект по определенным полям
abstract class DataHandler<T> {

    private final Callback<T> callback;
    private DataProvider dataProvider;
    private int total;
    private int filtered;
    private LinkedHashMap<String, DataObjectField> fields;
    private String id;

    DataHandler(Callback<T> callback) {
        this.callback = callback;
    }

    void fill(LinkedHashMap<String, DataObjectField> fields) {
        this.fields = fields;

        T object = data();

        if (object != null) {
            callback.data(object)
        } else {
            filtered++
        }
        total++;
    }

    protected abstract T data();

    protected DataObjectField get(String name) {
        return fields.get(name)
    }

    String getId() {
        return id
    }

    void setId(String id) {
        this.id = id
    }

    void setDataProvider(DataProvider dataProvider) {
        this.dataProvider = dataProvider
    }

    DataProvider getDataProvider() {
        return dataProvider
    }

    static interface Callback<T> {
        void data(T object)
    }
}

// Обработчик по населенным пунктам
class LocalityDataHandler extends DataHandler<Locality> {

    private List<String> municipalities

    LocalityDataHandler(Callback<Locality> callback, List<String> municipalities) {
        super(callback)
        this.municipalities = municipalities
    }

    Locality data() {
        if (municipalities.contains(get('Municipality').getValue().getFields().get('Description').getValue())) {
            Locality locality = new Locality()
            locality.id = id;
            locality.code = get('Code').getValue() != null ? get('Code').getValue() : ''
            locality.description = get('Description').getValue()
            DataObject typeDataObject = get('TypeOfMunicipalFormation').getValue()
            locality.type = typeDataObject != null ? typeDataObject.getFields().get('Code').getValue() : ''
            return locality
        } else {
            return null
        }
    }
}

// Обработчик по муниципальным организациям
class MunicipalityDataHandler extends DataHandler<Municipality> {

    MunicipalityDataHandler(Callback<Municipality> callback) {
        super(callback)
    }

    Municipality data() {
        Municipality municipality = new Municipality()
        municipality.id = id;
        municipality.code = get('Code').getValue() != null ? get('Code').getValue() : ''
        municipality.description = get('Description').getValue()
        return municipality
    }
}

// Поставшик данных
class DataProvider {

    def externalDatasource;
    def integrationsDataProvider;

    void getData(DataObject dataObject, DataHandler handler) {
        // Передаем поставшика для того чтобы можно было получать данные из других сущностей
        handler.setDataProvider(this)
        // Добоавляем id записи
        handler.setId(dataObject.getId())
        // Вызывает кэлбэк после заполнения обьекта
        handler.fill(dataObject.getFields())
    }

    // Получаем данные из списка обьектов с обработкой их в хэндлере
    void getData(List<DataObject> dataObjectList, DataHandler handler) {
        for (DataObject dataObject : dataObjectList) {
            getData(dataObject, handler)
        }
    }

    // Получаем данные из сущности с обработкой их в хэнделере
    void getData(String entityName, DataHandler handler) {
        this.getData(this.externalDataSource.getObjectList(entityName, null)
                as List<DataObject>, handler)
    }

    void getData(LinkedHashMap<String, Object> entity, DataHandler handler) {
        // Получаем обьекты по фильтру
        List<DataObject> objectList = integrationsDataProvider
                .runSavedScript('RSO_getObjectEntity', [entity: entity])
        // Заполняем список
        this.getData(objectList, handler)
    }

    void setExternalDataSource(externalDatasource) {
        this.externalDatasource = externalDatasource
    }

    def getExternalDataSource() {
        return this.externalDatasource;
    }

    void setIntegrationsDataProvider(integrationsDataProvider) {
        this.integrationsDataProvider = integrationsDataProvider
    }

}

// Получить Муниципальные организации
LinkedHashMap<String, Municipality> getMunicipalities(DataProvider dataProvider) {
    final LinkedHashMap<String, Municipality> municipalities = new LinkedHashMap<>();
    dataProvider.getData("tehprisRSO_Municipalities", new MunicipalityDataHandler(
            { municipality -> municipalities.put(municipality.description, municipality) }));
    return municipalities;
}

// Получить населенные пункты, принадлежащие муниципальтету
LinkedHashMap<String, Locality> getLocality(DataProvider dataProvider, List<String> municipalities) {
    final LinkedHashMap<String, Locality> localities = new LinkedHashMap<>();

    // Заполняем список населенных пунктов
    dataProvider.getData('tehprisRSO_Locality',
            new LocalityDataHandler(
                    { locality -> localities.put(locality.description, locality) }, municipalities))
    return localities;
}

// Создает список нас. пунктов по выбранным мун. образованиям
static List<String> createLocalities(List<String> selectLocalities, LinkedHashMap<String, Locality> localities) {
    List<String> result = new ArrayList<>();
    for (String selectLocality : selectLocalities) {
        if (localities.containsKey(selectLocality)) {
            result.add(selectLocality);
        }
    }
    return result;
}

// Проверяем принадлежность организации к выбранным населенным пунткам
static boolean contains(needles, haystack) {
    for (needle in needles) {
        if (haystack.contains(needle.toString())) return true;
    }
    return false;
}

List<List<String>> filterOrganizationTenants(tenants) {

    if (tenants.size() > 0) {

        // Получаем каталог организаций
        List<List<String>> result = externalDataSource.source.query()
                .select('select "Id", "Description" from "tehprisRSO_CatalogueOrganizations" where "Status" = :Status AND   "Tenant" IN (:Id)')
                .namedParams([Status: 'A', Id: tenants])
                .listResult({ rs -> def result = Arrays.asList(rs.getObject('Id'), rs.getObject('Description')) })

        return result
    } else {
        return new ArrayList<ArrayList<String>>();
    }
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////

// Создаем поставшика и добавляем источник данных
DataProvider dataProvider = new DataProvider()
dataProvider.setExternalDataSource(externalDataSource)
dataProvider.setIntegrationsDataProvider(integrationsDataProvider)

// Формируем список населенных пунктов
LinkedHashMap<String, Municipality> municipalities =
        dashboardContext.context['municipalities'] != null ?
                dashboardContext.context['municipalities'] : getMunicipalities(dataProvider)

if (dashboardContext.context['municipalities'] == null) {
    dashboardContext.context['municipalities'] = municipalities
}

// Заполняем список населенных пунктов мун.образования
if (dashboardContext.context['selectMunicipalities'] != null) {

    List<String> selectMunicipalities = dashboardContext.context['selectMunicipalities'];
    LinkedHashMap<String, Locality> localities = getLocality(dataProvider, selectMunicipalities)
    dashboardContext.context['localities'] = localities


}

if (dashboardContext.context['selectLocalities'] != null) {
    // Проверяем принадлежность населенного пункта к выбранному мун.образованию
    List<String> selectLocalities = dashboardContext.context['selectLocalities']
    localities = dashboardContext.context['localities']

    // Фильтруем населенные пункты
    List<String> filter = createLocalities(selectLocalities, localities)
    dashboardContext.context['selectLocalitiesCount'] = dashboardContext.context['selectLocalities'].size()

    if (dashboardContext.context['selectLocalities'].size() != filter.size()) {
        dashboardContext.context['selectLocalities'] = null
        dashboardContext.context['selectLocalitiesCount'] = 0
    }


}

// Получение параметров url
def paramMap = javax.faces.context.FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap()


if (paramMap.get('code') != null) {

    // Тип деятельности id
    def typeOfActivityParamCode = paramMap.get('code')

    // Фильтруем по типу помешения

    FilterGroup filterType = FilterGroup.single('Code',
            FilterElement.FilterOperator.equals, Collections.singletonList(typeOfActivityParamCode));

    DataObject typeOfActivity =
            this.externalDataSource.getObjectList('tehprisRSO_TypeOfActivity', null, filterType, null, null, null, null).getAt(0)


    if (typeOfActivity) {

        dashboardContext.context['tehprisRSO_Organizations_typeOfActivityIcon'] = '../dmsfile/' +
                typeOfActivity.fields.Icon.value.get(0).uid;

        //Описание сущности, по которой происходит поиск обьектов
        def activityOfOrganizationEntity = [
                link: 'tehprisRSO_ActivityOfOrganization',
                data: [
                        TypeOfActivity:
                                [link: 'tehprisRSO_TypeOfActivity',
                                 data: [Code: typeOfActivityParamCode]]
                ]]

        // Список деятельностей организаций
        def activityOfOrganizationList = integrationsDataProvider.runSavedScript('tehprisRSO_getObjectEntity',
                [entity: activityOfOrganizationEntity])

        // Список организаций
        List<Integer> tenantsId = new ArrayList<Integer>();
        List<Integer> tenantsAllId = new ArrayList<Integer>();

        // Ищем все организации, которые входят в населенные пункты,
        List<String> selectLocalities = dashboardContext.context['selectLocalities'] ?
                dashboardContext.context['selectLocalities'] : new ArrayList<>()

        List<String> selectMunicipalities = dashboardContext.context['selectMunicipalities'] ?
                dashboardContext.context['selectMunicipalities'] : new ArrayList<>()

        // Получаем список id тенантов
        activityOfOrganizationList.each {
            def tenant = it.fields.Tenant.value;
            if (tenant) {
                def org = it.fields.Tenant.value
                List<DataObject> localities = it.fields.Locality?.value
                if (!selectLocalities.isEmpty() && localities != null) {
                    if (contains(localities, selectLocalities)) {
                        tenantsId.add(Integer.valueOf(org.id))
                    }
                } else if (!selectMunicipalities.isEmpty() && localities != null) {
                    for (locality in localities) {

                        if (selectMunicipalities.contains(locality.fields.Municipality.toString())) {

                            Integer id = Integer.valueOf(org.id)
                            if (!tenantsId.contains(id)) {
                                tenantsId.add(id)
                            }
                        }
                    }

                }
                tenantsAllId.add(Integer.valueOf(org.id))
            }
        }

        // Формируем список организаций из Каталога

        org.primefaces.context.RequestContext.getCurrentInstance().execute("console.log('" + tenantsAllId + "');");
        List<DataObject> list = new ArrayList<>();

        if (selectLocalities.isEmpty() && selectMunicipalities.isEmpty() && tenantsAllId.size() > 0) {
            list = filterOrganizationTenants(tenantsAllId)
        } else {
            list = filterOrganizationTenants(tenantsId)
        }

        dashboardContext.context['tehprisRSO_Organizations_OrganizationList'] = list
        dashboardContext.context['tehprisRSO_Organizations_OrganizationListActive'] =
                typeOfActivityParamCode != null ? true : false


    }
}



