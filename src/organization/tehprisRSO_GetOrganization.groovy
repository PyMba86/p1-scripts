// TypeOfActivity - Входной параметр. Тип деятельности организации (VO,GS,VS,TS...)

// Создаем описание для фильтра по муниципалитету
LinkedHashMap<String, Object> entityLocality = [
        link: 'tehprisRSO_ActivityOfOrganization',
        data: [
                TypeOfActivity: [link: 'tehprisRSO_TypeOfActivity',
                                 data: [Code: TypeOfActivity]]
        ]]

// Получаем обьекты по фильтру с помощью сторонего скрипта
List<DataObject> activityOfOrganization = integrationsDataProvider
        .runSavedScript('tehprisRSO_getObjectEntity', [entity: entityLocality])

def tenants = []
def localityForm = dataObjectController.instance.fields.Locality
def localityId = null
if (localityForm) {
    if (localityForm.value) {
        localityId = localityForm.value.id
    }
}
for (activity in activityOfOrganization) {
    def locality = activity.fields.Locality.value
    def tenantId   = new Integer(activity.fields.Tenant.value.id)
    if (localityId) {
        if (locality.id.contains(localityId)) {
            tenants.add(tenantId)

        }
    } else {
        tenants.add(tenantId)
    }
}

return tenants.isEmpty() ? [0] : externalDataSource.source.query()
        .select('select "Id" from "tehprisRSO_Organization" where "Status" = :Status AND "Tenant" IN (:Tenant)')
        .namedParams([Status:'A', Tenant: tenants])
        .listResult({rs->def result=rs.getObject(1)})
