//tehprisRSO_ReportList
//Реестр
//////////////////////////////////////////////////////////////////////
def data = [:]
data.info = [:]
//////////////////////////////////////////////////////////////////////
def form = inputData.form
def name = ''
if (inputData.fileName && inputData.form) {
    name = inputData.fileName.concat('(').concat(inputData.form).concat(')')
}
//////////////////////////////////////////////////////////////////////
def objects = externalDataSource.getObjectList(form, null)
outputData.put('headers':createHeaders(form),  'data' : createGridData(objects, form), 'name': name)
///////////////////////////////////////////////////////////////////////////////////////

List<Object> createHeaders(String form) {
    return externalDataSource
            .getDataEntity(form)
            .getAttributes()
            .findAll{it -> it.isActive()}
            .collect { it -> it.toString() }
}

List<List<Object>> createGridData(List<DataObject> objects, String form) {
    List<List<Object>> data = new ArrayList<>();
    for (DataObject object in objects) {
        data.add(convertDataObjectToList(object.getFields()));
    }
    return data;
}

List<Object> convertDataObjectToList(fields) {
    List<Object> list = new ArrayList<>();
    for(field in fields) {
        list.add(field.value ? field.value.toString() : "")
    }
    return list;
}



//////////////////////////////////////////////////////////////////////