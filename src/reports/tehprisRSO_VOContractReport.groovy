//tehprisRSO_VOContractReport
//Договор ТП (водоотведение)
import groovy.json.JsonSlurper
import fr.opensagres.xdocreport.template.formatter.FieldsMetadata
//////////////////////////////////////////////////////////////////////
def data = [:]
data.info = [:]
outputData.put(data)
//////////////////////////////////////////////////////////////////////
def Organization = inputData.Organization
def eventsId = new ArrayList<Integer>()
if (inputData.EventsId) {
    eventsId = new JsonSlurper().parseText(inputData.EventsId)
}
//////////////////////////////////////////////////////////////////////

data.Organization = inputData.Organization
data.CustomerBase = inputData.CustomerBase
data.HeadMiddleName = inputData.HeadMiddleName
data.TermUse = inputData.TermUse
data.ObjectName = inputData.ObjectName
data.Payment1ConstNDS = inputData.Payment1ConstNDS
data.planOnLand = inputData.planOnLand
data.typeObjectRight = inputData.typeObjectRight
data.Payment3ConstNDS = inputData.Payment3ConstNDS
data.HeadLastName = inputData.HeadLastName
data.HeadLastName = inputData.HeadLastName
data.HeadFirstName = inputData.HeadFirstName
data.Payment2ConstNDS = inputData.Payment2ConstNDS

data.dataMer2 = []
data.dataMer = []

int mer = 0
int mer2 = 0

def createEvent(obj, n) {
    def map = [:]
    map.eventName = obj.fields.Description.value
    map.execEventsLists = obj.fields.Inc.value
    map.deadline = obj.fields.FinishDate
    map.number = n
    return map
}

for (eventId in eventsId) {
    def event = externalDataSource.getObject('tehprisRSO_FormsEvents', eventId.toString())
    if (event.value.fields.TypeUser.value.fields.Code.value == 'org') {
        mer++
        data.dataMer.add(createEvent(event,mer))
    } else {
        mer2++
        data.dataMer2.add(createEvent(event,mer2))
    }
}


FieldsMetadata metadata = new FieldsMetadata();
metadata.addFieldAsList("dataMer.number");
metadata.addFieldAsList("dataMer.eventName");
metadata.addFieldAsList("dataMer.execEventsLists");
metadata.addFieldAsList("dataMer.deadline");
metadata.addFieldAsList("dataMer2.number");
metadata.addFieldAsList("dataMer2.eventName");
metadata.addFieldAsList("dataMer2.execEventsLists");
metadata.addFieldAsList("dataMer2.deadline");
data.meta = metadata

//////////////////////////////////////////////////////////////////////