//tehprisRSO_VOTUReport
//Тех.условия (водоотведение)
//////////////////////////////////////////////////////////////////////
def data = [:]
data.info = [:]
outputData.put(data)
//////////////////////////////////////////////////////////////////////
data.CustomerBase = inputData.CustomerBase
data.TermCondition = inputData.TermUse
data.StandartsWastewaterVolume = inputData.NormsWastewaterVolume
data.CadastralNumber = inputData.CadastralNumber
data.ReasonPetition = inputData.ReasonPetition
data.RedresentMiddleName = inputData.RedresentMiddleName
data.RedresentLastName = inputData.RedresentLastName
data.MarkingTrays = inputData.MarkingTrays
data.RequirementsProject = inputData.TechnicalRequirements
data.LimitsOperationalLiability = inputData.BoundariesOperationalResponsibility
data.DeviceRequirements = inputData.RequirementsDevices
data.PointConnect = inputData.TechnologicalConnectPoint
data.RedresentFirstName = inputData.RedresentFirstName
data.AttachedObject = inputData.ObjectName
data.RequirementsReducingDischargeSewage = inputData.RequirementsReducing
//////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////