//tehprisRSO_VOContractResource
//Договор ресурсоснабжения по водоотведению
import groovy.json.JsonSlurper
import fr.opensagres.xdocreport.template.formatter.FieldsMetadata
//////////////////////////////////////////////////////////////////////
def data = [:]
data.info = [:]
outputData.put(data)
//////////////////////////////////////////////////////////////////////
def Organization = inputData.Organization
def eventsId = new ArrayList<Integer>()
if (inputData.EventsId) {
    eventsId = new JsonSlurper().parseText(inputData.EventsId)
}
//////////////////////////////////////////////////////////////////////
data.DULnum = inputData.DULnum
data.lastName = inputData.lastName
data.firstName = inputData.firstName
data.CustomerBase = inputData.CustomerBase
data.Organization = inputData.Organization
data.HeadMiddleName = inputData.HeadMiddleName
data.middleName = inputData.middleName
data.HeadPostString = inputData.HeadPostString
data.HeadLastName = inputData.HeadLastName
data.DULser = inputData.DULser
data.HeadFirstName = inputData.HeadFirstName
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////