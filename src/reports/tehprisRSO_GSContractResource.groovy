//tehprisRSO_GSContractResource
//Договор ресурсоснабжения по газоснабжению
import groovy.json.JsonSlurper
import fr.opensagres.xdocreport.template.formatter.FieldsMetadata
//////////////////////////////////////////////////////////////////////
def data = [:]
data.info = [:]
outputData.put(data)
//////////////////////////////////////////////////////////////////////

def Organization = inputData.Organization
def eventsId = new ArrayList<Integer>()
if (inputData.EventsId) {
    eventsId = new JsonSlurper().parseText(inputData.EventsId)
}
//////////////////////////////////////////////////////////////////////
data.HeadPostString = inputData.HeadPostString
data.HeadLastName = inputData.HeadLastName
data.HeadFirstName = inputData.HeadFirstName
data.PlaceContractConclusion = inputData.PlaceContractConclusion
data.Organization = inputData.Organization
data.HeadMiddleName = inputData.HeadMiddleName

//////////////////////////////////////////////////////////////////////
