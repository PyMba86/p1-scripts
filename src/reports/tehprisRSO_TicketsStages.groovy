// Распределение технологических присоединений по этапам

def data = [:]
data.info = [:]

data.employees = []

def rsoActivity = [
        tehprisRSO_WaterSanitationPdk: "Водоотведение",
        tehprisRSO_ElectroSupplyPdk  : "Электроснабжение",
        tehprisRSO_HeatSupplyPdk     : "Теплоснабжение",
        tehprisRSO_GasSupplyPdk      : "Газоснабжение",
        tehprisRSO_WaterSupplyPdk    : "Водоснабжение",
]
def statisticResult = [:]

for (TypeOfActivity in rsoActivity) {
    // Получем ID всех заявок со статусом "A", относящихся к организации текущего авторизованного пользователя
    def ticketData = externalDataSource.source.query()
            .select('select \n' +
            '\t"tickets"."Id" as Id,\n' +
            '\t"tickets"."Organization" as OrganizationId,\n' +
            '\t"org"."Description" as Organization,\n' +
            '\t"tickets"."Locality" as LocalityId,\n' +
            '\t"locality"."Description" as Locality,\n' +
            '\t"mun"."Description" as Municipality,\n' +
            '\t"tickets"."_Status" as CurrentStatus,\n' +
            '\t"tickets"."SendDate" as SendDate\n' +
            'from "' + TypeOfActivity.key + '" "tickets"\n' +
            'INNER JOIN "tehprisRSO_Organization" "org" ON "org"."Id" = "tickets"."Organization"\n' +
            'INNER JOIN "tehprisRSO_Locality" "locality" ON "locality"."Id" = "tickets"."Locality"\n' +
            'INNER JOIN "tehprisRSO_Municipalities" "mun" ON "mun"."Id" = "locality"."Municipality"\n' +
            'where "tickets"."Status" = :Status and \n' +
            '\t"tickets"."_Status" != \'app_draft\''
    )
            .namedParams([Status: 'A', TypeOfActivity: TypeOfActivity.key])
            .listResult({
        rs ->
            [
                    Id           : rs.getObject('Id').toString(),
                    Organization : rs.getObject('Organization').toString(),
                    Locality     : rs.getObject('Locality').toString(),
                    Municipality : rs.getObject('Municipality').toString(),
                    CurrentStatus: rs.getObject('CurrentStatus').toString(),
                    SendDate     : rs.getObject('SendDate').toString()
            ]
    })


    def ticketIdList = ticketData.collect { element -> return element.Id }
    ticketData = ticketData.groupBy { it.Id }

    if (!ticketIdList.isEmpty()) {
        for (ticketId in ticketIdList) {

            // Формируем объект заявки
            def ticketObj = externalDataSource.getObject(TypeOfActivity.key, ticketId)

            //Возвращает true если процесс по заявке уже запущен
            if (workflowDataProvider.getProcessesForObject(ticketObj).findAll { it.completed == null }.size() >= 1) {
                // Получили все процессы данной заявки (массив)
                def proccesses = workflowDataProvider.getProcessesForObject(ticketObj)

                for (process in proccesses) {
                    // Получаем текущий статус
                    def latestStatus = statusesDataProvider.getLatestStatus(TypeOfActivity.key, ticketId)

                    // Получаем все статусы
                    def objStatuses = statusesDataProvider.getObjectStatuses(TypeOfActivity.key, ticketId)

                    // Возрващает все статусы для данного процесса
                    def allStatuses = workflowDataProvider.getDefinitionTaskStatuses(TypeOfActivity.key, process.getProcessDefinition().getCode())

                    data.employees.add([
                            Id            : ticketId,
                            SendDate      : ticketData[ticketId].SendDate[0],
                            type          : TypeOfActivity.value,
                            municipality  : ticketData[ticketId].Municipality[0],
                            locality      : ticketData[ticketId].Locality[0],
                            organization  : ticketData[ticketId].Organization[0],
                            processName   : process,
                            CurrentStatus : latestStatus.getStatus().toString(),
                            allStatuses   : allStatuses.size().toString(),
                            latestStatuses: objStatuses.size().toString(),

                    ])
                }
            }

        }

    }
}


outputData.put(data)

