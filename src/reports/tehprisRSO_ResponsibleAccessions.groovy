// Динамика подачи заявлений по месяцам
//Отчет по ответственным отделам и технологическим присоединениям
//////////////////////////////////////////////////////////////////////

def data = [:]
data.info = [:]

data.employees = []

def rsoActivity = [
        tehprisRSO_WaterSanitationPdk: "Водоотведение",
        tehprisRSO_ElectroSupplyPdk  : "Электроснабжение",
        tehprisRSO_HeatSupplyPdk     : "Теплоснабжение",
        tehprisRSO_GasSupplyPdk      : "Газоснабжение",
        tehprisRSO_WaterSupplyPdk    : "Водоснабжение",
]
def statisticResult = [:]

for (TypeOfActivity in rsoActivity) {
    // Получем ID всех заявок со статусом "A", относящихся к организации текущего авторизованного пользователя
    def ticketData = externalDataSource.source.query()
            .select('select \n' +
            '\t"tickets"."Id" as Id,\n' +
            '\t"tickets"."Organization" as OrganizationId,\n' +
            '\t"org"."Description" as Organization,\n' +
            '\t"tickets"."Locality" as LocalityId,\n' +
            '\t"locality"."Description" as Locality,\n' +
            '\t"mun"."Description" as Municipality,\n' +
            '\t"tickets"."_Status" as CurrentStatus,\n' +
            '\t"tickets"."SendDate" as SendDate\n' +
            'from "' + TypeOfActivity.key + '" "tickets"\n' +
            'INNER JOIN "tehprisRSO_Organization" "org" ON "org"."Id" = "tickets"."Organization"\n' +
            'INNER JOIN "tehprisRSO_Locality" "locality" ON "locality"."Id" = "tickets"."Locality"\n' +
            'INNER JOIN "tehprisRSO_Municipalities" "mun" ON "mun"."Id" = "locality"."Municipality"\n' +
            'where "tickets"."Status" = :Status and \n' +
            '\t"tickets"."_Status" != \'app_draft\''
    )
            .namedParams([Status: 'A', TypeOfActivity: TypeOfActivity.key])
            .listResult({
        rs ->
            [
                    Id           : rs.getObject('Id').toString(),
                    Organization : rs.getObject('Organization').toString(),
                    Locality     : rs.getObject('Locality').toString(),
                    Municipality : rs.getObject('Municipality').toString(),
                    CurrentStatus: rs.getObject('CurrentStatus').toString(),
                    SendDate     : rs.getObject('SendDate').toString()
            ]
    })


    def ticketIdList = ticketData.collect { element -> return element.Id }
    ticketData = ticketData.groupBy { it.Id }

    if (!ticketIdList.isEmpty()) {
        for (ticketId in ticketIdList) {
            // Фильтр принимает вот такие параметры:
            // processId
            // taskId
            // objectClass
            // objectId
            // Получаем все активные задания одной заявки
            def tasks = workflowDataProvider.getTasks(
                    [
                            objectClass: TypeOfActivity.key,
                            objectId   : ticketId
                    ],
                    null,
                    null,
                    null
            )

            def tasksDescription = tasks.collect { task -> [name: task.getName(), assignee: task.getAssignee()] }

            for (taskItem in tasksDescription) {
                if (taskItem.assignee) {
                    def userData = taskItem.assignee

                    data.employees.add([
                            SendDate     : ticketData[ticketId].SendDate[0],
                            type         : TypeOfActivity.value,
                            municipality : ticketData[ticketId].Municipality[0],
                            locality     : ticketData[ticketId].Locality[0],
                            organization : ticketData[ticketId].Organization[0],
                            CurrentStatus: statusesDataProvider.getLatestStatus(TypeOfActivity.key, ticketId).getStatus().toString(),
                            TaskName     : taskItem.name,
                            department   : userData.getTenant().toString()
                    ])
                }
            }

            //tasks[0].getAssignee() // Получить исполнителя текущей задачи
            //tasks[0].getDueDate() // Срок окончания
            //tasks[0].getName() // Название задачи
            // Если хочешь получить ФИО пользователя, то место task.getAssignee() подставь
            // tasks[0].getAssignee().metaClass.methods*.name.sort().unique()
            // getFirstName getLastName getMiddleName getLoginName
            //tasks[0].getAssignee().getLoginName()
        }

    }
}


outputData.put(data)
