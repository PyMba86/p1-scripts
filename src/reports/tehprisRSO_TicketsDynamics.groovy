import java.util.LinkedHashMap;

//////////////////////////////////////////////////////////////////////
// Динамика подачи заявлений по месяцам
//////////////////////////////////////////////////////////////////////

def data = [:]
data.info = [:]

data.employees = []

List<DataObject> activityOfOrganization = new ArrayList<>();

if (inputData.From == "Forms") {
    activityOfOrganization = externalDataSource
            .getObjectList('tehprisRSO_CatalogueServices', null, null, 0, null, 'Num asc', null)
} else {
    LinkedHashMap<String, Object> entityFrom = [
            link: 'tehprisRSO_CatalogueServices',
            data: [
                    Form: inputData.From
            ]
    ]

    activityOfOrganization = integrationsDataProvider
            .runSavedScript('tehprisRSO_getObjectEntity', [entity: entityFrom])
}

def tenantId = integrationsDataProvider.runSavedScript('tehprisRSO_UserTenant', null)
def tenantFilterQuery = ""
if (tenantId != null) {
    tenantFilterQuery = '\t and "org"."Tenant" = :Tenant'
}
for (activity in activityOfOrganization) {

    def ticketData = externalDataSource.source.query()
            .select('select \n' +
                    '\t"tickets"."Id" as Id,\n' +
                    '\t"tickets"."Organization" as OrganizationId,\n' +
                    '\t"org"."Description" as Organization,\n' +
                    '\t"tickets"."Locality" as LocalityId,\n' +
                    '\t"locality"."Description" as Locality,\n' +
                    '\t"mun"."Description" as Municipality,\n' +
                    '\t"tickets"."_Status" as CurrentStatus,\n' +
                    '\t"tickets"."SendDate" as SendDate\n' +
                    'from "' + activity.fields.Form.value + '" "tickets"\n' +
                    'INNER JOIN "tehprisRSO_Organization" "org" ON "org"."Id" = "tickets"."Organization"\n' +
                    'INNER JOIN "tehprisRSO_Locality" "locality" ON "locality"."Id" = "tickets"."Locality"\n' +
                    'INNER JOIN "tehprisRSO_Municipalities" "mun" ON "mun"."Id" = "locality"."Municipality"\n' +
                    'where "tickets"."Status" = :Status and \n' +
                    '\t"tickets"."_Status" != \'app_draft\'' +
                    tenantFilterQuery
            )
            .namedParams([Status: 'A', Tenant: tenantId])
            .listResult({
                rs ->
                    [
                            Id           : rs.getObject('Id').toString(),
                            Organization : rs.getObject('Organization').toString(),
                            Locality     : rs.getObject('Locality').toString(),
                            Municipality : rs.getObject('Municipality').toString(),
                            CurrentStatus: rs.getObject('CurrentStatus').toString(),
                            SendDate     : rs.getObject('SendDate').toString()
                    ]
            })


    def ticketIdList = ticketData.collect { element -> return element.Id }
    ticketData = ticketData.groupBy { it.Id }

    if (!ticketIdList.isEmpty()) {

        def ticketsActivity = entityManager
                .createQuery('select  new Map (\n' +
                        '\ts.status.code as Status,\n' +
                        '\ts.objectId as TicketId,\n' +
                        '\tto_char(s.begins, \'DD-MM-YYYY\') as DateBegins\n' +
                        ')\n' +
                        'from ObjectStatus s\n' +
                        'where s.objectId IN :ticketIdList\n' +
                        'order by 3 ASC')
                .setParameter('ticketIdList', ticketIdList)
                .resultList
                .groupBy { it.TicketId }

        for (ticket in ticketsActivity) {
            data.employees.add([
                    SendDate     : ticketData[ticket.key].SendDate[0],
                    type         : activity.fields.ServiceType.value.toString(),
                    municipality : ticketData[ticket.key].Municipality[0],
                    locality     : ticketData[ticket.key].Locality[0],
                    organization : ticketData[ticket.key].Organization[0],
                    CurrentStatus: ticketData[ticket.key].CurrentStatus[0],
                    AllStatuses  : ticket.value.collect { it -> return it.Status }.join(','),
                    send         : "0", //o.Send,
                    work         : "0", //o.Work,
                    complete     : "0", //o.Complete,
                    cancelled    : "0"  //o.Cancelled
            ])
        }
    }
}

data.employees = data.employees.sort { a, b ->
    aDate = Date.parse("yyyy-MM-dd", a.SendDate + "-" + a.SendDate)
    bDate = Date.parse("yyyy-MM-dd", b.SendDate + "-" + b.SendDate)
    return aDate <=> bDate
}

outputData.put(data)
