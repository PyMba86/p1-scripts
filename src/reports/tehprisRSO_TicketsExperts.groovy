// Отчет по текущей нагрузке специалистов (экспертов)
//////////////////////////////////////////////////////////////////////

def data = [:]
data.info = [:]

data.employees = []

def activityOfOrganization = externalDataSource
        .getObjectList('tehprisRSO_CatalogueServices', null, null, 0, null, 'Num asc', null)

def tenantId = integrationsDataProvider.runSavedScript('tehprisRSO_UserTenant', null)
def tenantFilterQuery = ""
if (tenantId != null) {
    tenantFilterQuery = '\t and "org"."Tenant" = :Tenant'
}

for (activity in activityOfOrganization) {

    def ticketData = externalDataSource.source.query()
            .select('select \n' +
            '\t"tickets"."Id" as Id,\n' +
            '\t"tickets"."Organization" as OrganizationId,\n' +
            '\t"org"."Description" as Organization,\n' +
            '\t"tickets"."Locality" as LocalityId,\n' +
            '\t"locality"."Description" as Locality,\n' +
            '\t"mun"."Description" as Municipality,\n' +
            '\t"tickets"."_Status" as CurrentStatus,\n' +
            '\t"tickets"."SendDate" as SendDate\n' +
            'from "' + activity.fields.Form.value + '" "tickets"\n' +
            'INNER JOIN "tehprisRSO_Organization" "org" ON "org"."Id" = "tickets"."Organization"\n' +
            'INNER JOIN "tehprisRSO_Locality" "locality" ON "locality"."Id" = "tickets"."Locality"\n' +
            'INNER JOIN "tehprisRSO_Municipalities" "mun" ON "mun"."Id" = "locality"."Municipality"\n' +
            'where "tickets"."Status" = :Status and \n' +
            '\t"tickets"."_Status" != \'app_draft\'' +
            tenantFilterQuery
    )
            .namedParams([Status: 'A', Tenant: tenantId])
            .listResult({
        rs ->
            [
                    Id           : rs.getObject('Id').toString(),
                    Organization : rs.getObject('Organization').toString(),
                    Locality     : rs.getObject('Locality').toString(),
                    Municipality : rs.getObject('Municipality').toString(),
                    CurrentStatus: rs.getObject('CurrentStatus').toString(),
                    SendDate     : rs.getObject('SendDate').toString()
            ]
    })


        for (ticket in ticketData) {
            // Фильтр принимает вот такие параметры:
            // processId
            // taskId
            // objectClass
            // objectId
            // Получаем все активные задания одной заявки
            def tasks = workflowDataProvider.getTasks(
                    [
                            objectClass: activity.fields.Form.value,
                            objectId   : ticket.Id
                    ],
                    null,
                    null,
                    null
            )

            def tasksDescription = tasks.collect { task -> [name: task.getName(), assignee: task.getAssignee()] }

            for (taskItem in tasksDescription) {
                if (taskItem.assignee) {
                    def userData = taskItem.assignee

                    data.employees.add([
                            SendDate     : ticket.SendDate,
                            type         : activity.fields.ServiceType.value.toString(),
                            municipality : ticket.Municipality,
                            locality     : ticket.Locality,
                            organization : ticket.Organization,
                            CurrentStatus: statusesDataProvider.getLatestStatus(activity.fields.Form.value, ticket.Id).getStatus().getName(),
                            TaskName     : taskItem.name,
                            CurrentUser  : "[" + userData.getLoginName() + "] " + userData.getFirstName() + " " + userData.getLastName() + " " + userData.getMiddleName()
                    ])
                }
            }

            //tasks[0].getAssignee() // Получить исполнителя текущей задачи
            //tasks[0].getDueDate() // Срок окончания
            //tasks[0].getName() // Название задачи
            // Если хочешь получить ФИО пользователя, то место task.getAssignee() подставь
            // tasks[0].getAssignee().metaClass.methods*.name.sort().unique()
            // getFirstName getLastName getMiddleName getLoginName
            //tasks[0].getAssignee().getLoginName()
        }

}

data.employees = data.employees.sort { a, b ->
    aDate = Date.parse("yyyy-MM-dd", a.SendDate + "-" + a.SendDate)
    bDate = Date.parse("yyyy-MM-dd", b.SendDate + "-" + b.SendDate)
    return aDate <=> bDate
}



outputData.put(data)
