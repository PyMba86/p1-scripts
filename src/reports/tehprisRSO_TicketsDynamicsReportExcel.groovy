import ru.osslabs.dms.utils.DmsUtils
import com.google.common.net.UrlEscapers
def zipFileName = 'Отчет.zip'

ZIP_CONTENT_TYPE = 'application/zip'
//////////////////////////////////////////////////////////////////////////////////////////
// from - Сущность заявки
//////////////////////////////////////////////////////////////////////////////////////////

//формирование списка файлов отчетов
def fileList = fMakeReports()

//для непосредственного скачивания архива байты нужно обернуть в streamedContent
//и положить в бин dataObjectActionsController для последующего скачивания
def encodedFileName = UrlEscapers.urlFragmentEscaper().escape(zipFileName)
dataObjectActionsController.downloadFile = DmsUtils.dmsFileListToZipStreamedContent(encodedFileName, fileList)
return dataObjectActionsController.getDownloadFile()



//////////////////////////////////////////////////////////////////////////////////////////
def fMakeReports() {
    //для примера параметры формирования отчетов определим как константы
    def uFileList = []

    //формируем 1-й отчет с переименованием выходного файла
    def excelReport = reportsDataProvider.createReport("tehprisRSO_TicketsDynamics")

    excelReport.reportFileName = 'Динамика подачи заявлений по месяцам'
    excelReport.dataObject.fields.From.value = from ? from : "Forms"

    reportsDataProvider.fillReportWithResult(excelReport)
    uFileList.add(excelReport.resultFile)

    return uFileList
}//fMakeReports



