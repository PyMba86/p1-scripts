//////////////////////////////////////////////////////////////////////
// Отчет по просроченным этапам
//////////////////////////////////////////////////////////////////////

def data = [:]
data.info = [:]

data.employees = []

def activityOfOrganization = externalDataSource
        .getObjectList('tehprisRSO_CatalogueServices', null, null, 0, null, 'Num asc', null)

def tenantId = integrationsDataProvider.runSavedScript('tehprisRSO_UserTenant', null)
def tenantFilterQuery = ""
if (tenantId != null) {
    tenantFilterQuery = '\t and "org"."Tenant" = :Tenant'
}

for (activity in activityOfOrganization) {

    def ticketData = externalDataSource.source.query()
            .select('select \n' +
                    '\t"tickets"."Id" as Id,\n' +
                    '\t"tickets"."Organization" as OrganizationId,\n' +
                    '\t"org"."Description" as Organization,\n' +
                    '\t"tickets"."Locality" as LocalityId,\n' +
                    '\t"locality"."Description" as Locality,\n' +
                    '\t"mun"."Description" as Municipality,\n' +
                    '\t"tickets"."_Status" as CurrentStatus,\n' +
                    '\t"tickets"."SendDate" as SendDate\n' +
                    'from "' + activity.fields.Form.value + '" "tickets"\n' +
                    'INNER JOIN "tehprisRSO_Organization" "org" ON "org"."Id" = "tickets"."Organization"\n' +
                    'INNER JOIN "tehprisRSO_Locality" "locality" ON "locality"."Id" = "tickets"."Locality"\n' +
                    'INNER JOIN "tehprisRSO_Municipalities" "mun" ON "mun"."Id" = "locality"."Municipality"\n' +
                    'where "tickets"."Status" = :Status and \n' +
                    '\t"tickets"."_Status" != \'app_draft\'' +
                    tenantFilterQuery
            )
            .namedParams([Status: 'A', Tenant: tenantId])
            .listResult({
                rs ->
                    [
                            Id           : rs.getObject('Id').toString(),
                            Organization : rs.getObject('Organization').toString(),
                            Locality     : rs.getObject('Locality').toString(),
                            Municipality : rs.getObject('Municipality').toString(),
                            CurrentStatus: rs.getObject('CurrentStatus').toString(),
                            SendDate     : rs.getObject('SendDate').toString()
                    ]
            })


    for (ticket in ticketData) {

        // Формируем объект заявки
        def ticketObj = externalDataSource.getObject(activity.fields.Form.value, ticket.Id)

        //Возвращает true если процесс по заявке уже запущен
        if (workflowDataProvider.getProcessesForObject(ticketObj).findAll { it.completed == null }.size() >= 1) {
            // Получили все процессы данной заявки (массив)
            def proccesses = workflowDataProvider.getProcessesForObject(ticketObj)

            for (process in proccesses) {
                // Получаем текущий статус
                def latestStatus = statusesDataProvider.getLatestStatus(activity.fields.Form.value, ticket.Id)

                def filter = [
                        objectClass: activity.fields.Form.value,
                        objectId   : ticket.Id
                ]
                def tasks = workflowDataProvider.getTasks(filter, null, null, null)

                for (task in tasks) {
                    def ololo = task.getAssignee()
                    if (ololo) {
                        String expired = "0"
                        if (ololo.getExpirationDate()) {
                            expired = ololo.getExpirationDate().toString()
                        }

                        data.employees.add([
                                Id           : ticket.Id,
                                SendDate     : ticket.SendDate,
                                type         : activity.fields.ServiceType.value.toString(),
                                municipality : ticket.Municipality,
                                locality     : ticket.Locality,
                                organization : ticket.Organization,
                                processName  : process.toString(),
                                CurrentStatus: latestStatus.getStatus().getName(),
                                Expired      : expired

                        ])
                    }
                }
            }
        }

    }
}

data.employees = data.employees.sort { a, b ->
    aDate = Date.parse("yyyy-MM-dd", a.SendDate + "-" + a.SendDate)
    bDate = Date.parse("yyyy-MM-dd", b.SendDate + "-" + b.SendDate)
    return aDate <=> bDate
}


outputData.put(data)

