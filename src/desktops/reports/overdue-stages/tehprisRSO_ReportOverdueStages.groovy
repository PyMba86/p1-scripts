import org.primefaces.model.chart.Axis
import org.primefaces.model.chart.AxisType
import org.primefaces.model.chart.BarChartModel
import org.primefaces.model.chart.ChartSeries

import java.time.LocalDate
import java.time.format.DateTimeFormatter
import java.time.format.DateTimeFormatterBuilder
import java.time.temporal.ChronoField
import java.util.stream.Collectors
import java.util.stream.LongStream

// Отчет по просроченным этапам
//////////////////////////////////////////////////////////////////////

def object = [:]
def activityOfOrganization = externalDataSource
        .getObjectList('tehprisRSO_CatalogueServices', null, null, 0, null, 'Num asc', null)

DateTimeFormatter fmt = new DateTimeFormatterBuilder()
        .appendPattern("MM-yyyy")
        .parseDefaulting(ChronoField.DAY_OF_MONTH, 1)
        .toFormatter();


def static getMonthStr(monthNumber) {
    switch (monthNumber) {
        case 1:
            return 'Январь'
        case 2:
            return 'Февраль'
        case 3:
            return 'Март'
        case 4:
            return 'Апрель'
        case 5:
            return 'Май'
        case 6:
            return 'Июнь'
        case 7:
            return 'Июль'
        case 8:
            return 'Август'
        case 9:
            return 'Сентябрь'
        case 10:
            return 'Октябрь'
        case 11:
            return 'Ноябрь'
        case 12:
            return 'Декабрь'
    }
}

def tenantId = integrationsDataProvider.runSavedScript('tehprisRSO_UserTenant', null)
def tenantFilterQuery = ""
if (tenantId != null) {
    tenantFilterQuery = '\t and "org"."Tenant" = :Tenant'
}

for (activity in activityOfOrganization) {

    def ticketData = externalDataSource.source.query()
            .select('select \n' +
                    '\t"tickets"."Id" as Id,\n' +
                    '\t"tickets"."Organization" as OrganizationId,\n' +
                    '\t"org"."Description" as Organization,\n' +
                    '\t"tickets"."Locality" as LocalityId,\n' +
                    '\t"locality"."Description" as Locality,\n' +
                    '\t"mun"."Description" as Municipality,\n' +
                    '\t"tickets"."_Status" as CurrentStatus,\n' +
                    '\t"tickets"."SendDate" as SendDate\n' +
                    'from "' + activity.fields.Form.value + '" "tickets"\n' +
                    'INNER JOIN "tehprisRSO_Organization" "org" ON "org"."Id" = "tickets"."Organization"\n' +
                    'INNER JOIN "tehprisRSO_Locality" "locality" ON "locality"."Id" = "tickets"."Locality"\n' +
                    'INNER JOIN "tehprisRSO_Municipalities" "mun" ON "mun"."Id" = "locality"."Municipality"\n' +
                    'where "tickets"."Status" = :Status and \n' +
                    '\t"tickets"."_Status" != \'app_draft\'' +
                    tenantFilterQuery
            )
            .namedParams([Status: 'A', Tenant: tenantId])
            .listResult({
                rs ->
                    [
                            Id           : rs.getObject('Id').toString(),
                            Organization : rs.getObject('Organization').toString(),
                            Locality     : rs.getObject('Locality').toString(),
                            Municipality : rs.getObject('Municipality').toString(),
                            CurrentStatus: rs.getObject('CurrentStatus').toString(),
                            SendDate     : rs.getObject('SendDate').toString()
                    ]
            })

    for (ticket in ticketData) {

        // Формируем объект заявки
        def ticketObj = externalDataSource.getObject(activity.fields.Form.value, ticket.Id)

        //Возвращает true если процесс по заявке уже запущен
        if (workflowDataProvider.getProcessesForObject(ticketObj).findAll { it.completed == null }.size() >= 1) {
            // Получили все процессы данной заявки (массив)
            def proccesses = workflowDataProvider.getProcessesForObject(ticketObj)

            for (process in proccesses) {

                def filter = [
                        objectClass: activity.fields.Form.value,
                        objectId   : ticket.Id
                ]

                def tasks = workflowDataProvider.getTasks(filter, null, null, null)

                for (task in tasks) {
                    def ololo = task.getAssignee()
                    if (ololo) {
                        def SendDate = LocalDate.parse(ticket.SendDate, DateTimeFormatter.ofPattern("yyyy-MM-dd"))

                        if (!object[ticket.SendDate]) {
                            object[ticket.SendDate] = [
                                    SendDate           : SendDate,
                                    Month              : SendDate.getMonthValue(),
                                    Year               : SendDate.getYear(),
                                    AllTicketsCount    : 0,
                                    ExpiredTicketsCount: 0
                            ]
                        }
                        object[ticket.SendDate].AllTicketsCount++

                        if (ololo.getExpirationDate()) {
                            object[ticket.SendDate].ExpiredTicketsCount++
                        }

                    }
                }
            }
        }

    }


}

ChartSeries AllTickets = new ChartSeries()
AllTickets.setLabel("Всего задач")
ChartSeries ExpiredTickets = new ChartSeries()
ExpiredTickets.setLabel("Задач просрочено")

class OverdueStages {
    public int AllTicketsCount;
    public int ExpiredTicketsCount;

    OverdueStages(int allTicketsCount, int expiredTicketsCount) {
        AllTicketsCount = allTicketsCount
        ExpiredTicketsCount = expiredTicketsCount
    }
}

result = object.values().sort { a, b ->
    aDate = a.SendDate
    bDate = b.SendDate
    return aDate <=> bDate
}


LocalDate now = LocalDate.now().withDayOfMonth(1);

TreeMap<LocalDate, OverdueStages> mouths = LongStream
        .iterate(12-1, { i -> i - 1 })
        .limit(12)
        .mapToObj { it -> now.minusMonths(it) }
        .collect(Collectors.toMap({ it -> it }, { it -> new OverdueStages(0, 0) }))


for (ticket in result) {

    LocalDate ticketDate = ticket.SendDate.withDayOfMonth(1);

    if (mouths.containsKey(ticketDate)) {
        OverdueStages overdueStages = mouths.get(ticketDate);
        overdueStages.AllTicketsCount = ticket.AllTicketsCount;
        overdueStages.ExpiredTicketsCount = ticket.ExpiredTicketsCount;
    }
}

def maxTickets = 0

for (mouth in mouths) {

    if (maxTickets < mouth.value.AllTicketsCount) {
        maxTickets = Math.ceil(mouth.value.AllTicketsCount / 10) * 10
    }

    def interval = getMonthStr(mouth.key.getMonthValue()) + " " + mouth.key.getYear()

    AllTickets.set(interval, mouth.value.AllTicketsCount)
    ExpiredTickets.set(interval, mouth.value.ExpiredTicketsCount)

}

BarChartModel model = new BarChartModel()

model.addSeries(AllTickets)
model.addSeries(ExpiredTickets)
model.setLegendPosition("ne")

Axis xAxis = model.getAxis(AxisType.X)
xAxis.setLabel("Время (мес.)")

Axis yAxis = model.getAxis(AxisType.Y)
yAxis.setLabel("Количество задач")
yAxis.setMin(0)
yAxis.setMax(maxTickets)

dashboardContext.context['tehprisRSO_bar'] = model

