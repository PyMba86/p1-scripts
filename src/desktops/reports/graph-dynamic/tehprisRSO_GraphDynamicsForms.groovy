import org.primefaces.model.chart.Axis
import org.primefaces.model.chart.AxisType
import org.primefaces.model.chart.BarChartModel
import org.primefaces.model.chart.ChartSeries
import java.time.LocalDate
import java.time.format.DateTimeFormatter
import java.time.format.DateTimeFormatterBuilder
import java.time.temporal.ChronoField
import java.util.stream.Collectors
import java.util.stream.LongStream;

def static getMonthStr(monthNumber) {
    switch (monthNumber) {
        case 1:
            return 'Январь'
        case 2:
            return 'Февраль'
        case 3:
            return 'Март'
        case 4:
            return 'Апрель'
        case 5:
            return 'Май'
        case 6:
            return 'Июнь'
        case 7:
            return 'Июль'
        case 8:
            return 'Август'
        case 9:
            return 'Сентябрь'
        case 10:
            return 'Октябрь'
        case 11:
            return 'Ноябрь'
        case 12:
            return 'Декабрь'
    }
}

BarChartModel model = new BarChartModel()

ChartSeries Send = new ChartSeries()
Send.setLabel("Подано заявлений")
ChartSeries Work = new ChartSeries()
Work.setLabel("Заявлений в работе")
ChartSeries Complete = new ChartSeries()
Complete.setLabel("Выполнено заявлений")
ChartSeries Cancelled = new ChartSeries()
Cancelled.setLabel("Отклонено заявлений")

def object = integrationsDataProvider.runSavedScript('tehprisRSO_GetTicketsReportData', null)

class TicketStatistics {
    public int Send;
    public int Work;
    public int Complete;
    public int Cancelled;

    TicketStatistics(int send, int work, int complete, int cancelled) {
        Send = send
        Work = work
        Complete = complete
        Cancelled = cancelled
    }
}

try {

    result = object.sort { a, b ->
        aDate = Date.parse("MM-yyyy", a.Month + "-" + a.Year)
        bDate = Date.parse("MM-yyyy", b.Month + "-" + b.Year)
        return aDate <=> bDate
    }

    LocalDate now = LocalDate.now().withDayOfMonth(1);

    TreeMap<LocalDate, TicketStatistics> mouths = LongStream
            .iterate(12-1, {i -> i - 1})
            .limit(12)
            .mapToObj { it -> now.minusMonths(it) }
            .collect(Collectors.toMap({it -> it}, {it ->  new TicketStatistics(0, 0, 0, 0)}))


    def maxTickets = 0

    for (ticket in result) {
        String ticketDateString = ticket.Month + "-" + ticket.Year

        DateTimeFormatter fmt = new DateTimeFormatterBuilder()
                .appendPattern("MM-yyyy")
                .parseDefaulting(ChronoField.DAY_OF_MONTH, 1)
                .toFormatter();

        LocalDate ticketDate =  LocalDate.parse(ticketDateString,fmt);

        if (mouths.containsKey(ticketDate)) {
            TicketStatistics statistics = mouths.get(ticketDate);
            statistics.Send = ticket.Send;
            statistics.Work = ticket.Work;
            statistics.Complete = ticket.Complete;
            statistics.Cancelled = ticket.Cancelled;
        }
    }

    for (mouth in mouths) {

        if (maxTickets < mouth.value.Send) {
            maxTickets = Math.ceil(mouth.value.Send / 10) * 10
        }

        def interval = getMonthStr(mouth.key.getMonthValue()) + " " + mouth.key.getYear()

        Send.set(interval, mouth.value.Send)
        Work.set(interval, mouth.value.Work)
        Complete.set(interval, mouth.value.Complete)
        Cancelled.set(interval, mouth.value.Cancelled)

    }

    model.addSeries(Send)
    model.addSeries(Work)
    model.addSeries(Complete)
    model.addSeries(Cancelled)


    model.setLegendPosition("ne")

    Axis xAxis = model.getAxis(AxisType.X)
    xAxis.setLabel("Время (мес.)")

    Axis yAxis = model.getAxis(AxisType.Y)
    yAxis.setLabel("Количество заявлений")
    yAxis.setMin(0)
    yAxis.setMax(maxTickets)


} catch (Exception e) {

    browserConsole.info(e.getMessage())
    throw e;

}
dashboardContext.context['tehprisRSO_bar'] = model

if (dashboardContext.context['tehrpisRSO_activeLocality'] == null) {
    dashboardContext.context['tehrpisRSO_activeLocality'] = false
}
