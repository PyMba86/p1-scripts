import org.primefaces.model.chart.Axis
import org.primefaces.model.chart.AxisType
import org.primefaces.model.chart.BarChartModel
import org.primefaces.model.chart.ChartSeries

import java.time.LocalDate
import java.time.format.DateTimeFormatter
import java.time.format.DateTimeFormatterBuilder
import java.time.temporal.ChronoField
import java.util.stream.Collectors
import java.util.stream.LongStream
import java.util.Calendar;

BarChartModel model = new BarChartModel()

ChartSeries Send = new ChartSeries()
Send.setLabel("Подано заявлений")
ChartSeries Work = new ChartSeries()
Work.setLabel("Заявлений в работе")
ChartSeries Complete = new ChartSeries()
Complete.setLabel("Выполнено заявлений")
ChartSeries Cancelled = new ChartSeries()
Cancelled.setLabel("Отклонено заявлений")

def object = integrationsDataProvider.runSavedScript('tehprisRSO_GetTicketsReportData',  [from: 'Forms'])

def maxTickets = 0

class TicketStatistics {
    public int Send;
    public int Work;
    public int Complete;
    public int Cancelled;

    TicketStatistics(int send, int work, int complete, int cancelled) {
        Send = send
        Work = work
        Complete = complete
        Cancelled = cancelled
    }
}

def static getQuarter(int month) {
    return (month >= Calendar.JANUARY && month <= Calendar.MARCH)     ? "Q1" :
            (month >= Calendar.APRIL && month <= Calendar.JUNE)        ? "Q2" :
                    (month >= Calendar.JULY && month <= Calendar.SEPTEMBER)    ? "Q3" :
                            "Q4";
}

try {

    result = object.sort { a, b ->
        aDate = Date.parse("MM-yyyy", a.Month + "-" + a.Year)
        bDate = Date.parse("MM-yyyy", b.Month + "-" + b.Year)
        return aDate <=> bDate
    }

    LocalDate now = LocalDate.now().withDayOfMonth(1);

    TreeMap<LocalDate, TicketStatistics> mouths = LongStream
            .iterate(12-1, {i -> i - 1})
            .limit(12)
            .mapToObj { it -> now.minusMonths(it) }
            .collect(Collectors.toMap({ it -> it}, { it ->  new TicketStatistics(0, 0, 0, 0)}))

    for (ticket in result) {
        String ticketDateString = ticket.Month + "-" + ticket.Year

        DateTimeFormatter fmt = new DateTimeFormatterBuilder()
                .appendPattern("MM-yyyy")
                .parseDefaulting(ChronoField.DAY_OF_MONTH, 1)
                .toFormatter();

        LocalDate ticketDate =  LocalDate.parse(ticketDateString,fmt);

        if (mouths.containsKey(ticketDate)) {
            TicketStatistics statistics = mouths.get(ticketDate);
            statistics.Send = ticket.Send;
            statistics.Work = ticket.Work;
            statistics.Complete = ticket.Complete;
            statistics.Cancelled = ticket.Cancelled;
        }
    }

    LinkedHashMap<String, TicketStatistics> ticketDateStatistics = new LinkedHashMap<>();

    for (mouth in mouths) {

        if (maxTickets < mouth.value.Send) {
            maxTickets = Math.ceil(mouth.value.Send / 10) * 10
        }

        String  interval = getQuarter(mouth.key.getMonthValue()) + " " + mouth.key.getYear()
        ticket = ticketDateStatistics.getOrDefault(interval, new TicketStatistics(0,0,0,0))

        ticket.Send += mouth.value.Send;
        ticket.Work += mouth.value.Work;
        ticket.Complete += mouth.value.Complete;
        ticket.Cancelled += mouth.value.Cancelled;

        ticketDateStatistics.put(interval, ticket);

    }

    for (ticket in ticketDateStatistics) {

        Send.set(ticket.key, ticket.value.Send)
        Work.set(ticket.key, ticket.value.Work)
        Complete.set(ticket.key, ticket.value.Complete)
        Cancelled.set(ticket.key, ticket.value.Cancelled)
    }


    model.addSeries(Send)
    model.addSeries(Work)
    model.addSeries(Complete)
    model.addSeries(Cancelled)

    model.setLegendPosition("ne")

    Axis xAxis = model.getAxis(AxisType.X)
    xAxis.setLabel("Время (мес.)")

    Axis yAxis = model.getAxis(AxisType.Y)
    yAxis.setLabel("Количество заявлений")
    yAxis.setMin(0)
    yAxis.setMax(maxTickets)

} catch (Exception e) {

    browserConsole.info(e.getMessage())
    throw e;

}
dashboardContext.context['tehprisRSO_bar'] = model

// Список деятельностей организаций
//def activityOfOrganizationList = integrationsDataProvider.runSavedScript('tehprisRSO_getObjectEntity', [entity: tehprisRSO_WaterSanitationPdkEntity])

// вернет все объекты из сущности
//WaterTickets = externalDataSource.getObjectList("tehprisRSO_WaterSanitationPdk", null)

if (dashboardContext.context['tehrpisRSO_activeLocality'] == null) {
    dashboardContext.context['tehrpisRSO_activeLocality'] = false
}


