/*
В этом фильтре собраны все подписанные договора на тех прис.
Поля таблицы вывода:
наименование лица (ФИО физ лица или наименование юр лица с которым был договор),
дата подписания,
срок исполнения (сколько прошло от момента подачи заявки до подписания),
статус завершения в срок (если был этап не в срок, значит не в срок),
стоимость подключения,
статус исполнен или нет (исполнен если поступила оплата 3 счета и подписан акт),
тип присоединения
*/

// Функция кооторая принимает название сущности, с описанием какие аттрибуты от куда брать.
// Класс который описывает данные для таблицы
// Функция, которая добавляет данные к таблице
// Класс, который управляет таблицой
// Фильтр по колонкам
// Сотрировка по колонкам


import javax.faces.component.UIComponent;
import java.util.Date;
import java.util.concurrent.TimeUnit;
import ru.osslabs.model.common.filter.FilterElement;
import ru.osslabs.model.common.filter.FilterGroup;

// Модель колонки
class ColumnTable {

    //Название колонки
    private String header;
    // Ключ колонки
    private String property;

    public ColumnTable(String header, String property) {
        this.header = header;
        this.property = property;
    }

    public String getHeader() {
        return header;
    }

    public String getProperty() {
        return property;
    }
}

class Link {
    private String name;
    private String link;

    Link(String name, String link) {
        this.name = name
        this.link = link
    }

    String getName() {
        return name
    }

    void setName(String name) {
        this.name = name
    }

    String getLink() {
        return link
    }

    void setLink(String link) {
        this.link = link
    }
}

class Form {
    private String uniqueId;

    Form(String uniqueId) {
        this.uniqueId = uniqueId
    }

    String getUniqueId() {
        return uniqueId
    }

    void setUniqueId(String uniqueId) {
        this.uniqueId = uniqueId
    }
}


interface DataTable {

    void setData(String name, Object value);

    Object getData(String name);

}


abstract class DataHandler<T> {

    private int total;
    private int filtered;
    protected LinkedHashMap<String, DataObjectField> fields;
    protected DataObject object;

    T fill(DataObject object, LinkedHashMap<String, DataObjectField> fields) {
        this.fields = fields;
        this.object = object

        T data = data();

        if (data != null) {
            return data
        } else {
            filtered++
        }
        total++;
        return null
    }

    protected abstract T data();

    protected DataObjectField get(String name) {
        return fields.get(name)
    }
}

// Получить значения поля по имени
class StringFieldDataHandler extends DataHandler<String> {

    String field;

    StringFieldDataHandler(String field) {
        this.field = field
    }

    String data() {
        return get(field).getValue()
    }
}

class IdLinkFieldDataHandler extends DataHandler<Form> {

    String field;
    String form;

    IdLinkFieldDataHandler(String field, String form) {
        this.field = field
        this.form = form
    }

    Form data() {
        return new Form(object.getUniqueId())

    }
}

class FileLinkFieldDataHandler extends DataHandler<Link> {

    String field;
    String name;

    FileLinkFieldDataHandler(String name, String field) {
        this.field = field
        this.name = name
    }

    Link data() {
        if (get(field).getValue()) {
            return new Link(name, "/platform/dmsfile/" + get(field).getValue().get(0).uid)
        }
        else {
            return new Link(name,"#")
        }

    }
}


class ApplicantFieldDataHandler extends DataHandler<String> {

    String data() {
        def type = fields.ApplicantType.value?.fields.Code?.value
        if (type && type.equals('FL') || fields.type.equals('IP')) {
            def lastName = fields.lastName.value
            def firstName = fields.firstName.value
            def middleName = fields.middleName.value
            return (lastName && firstName && middleName) ? lastName.concat(' ').concat(firstName.substring(0, 1)).concat('. ')
                    .concat(middleName.substring(0, 1)).concat('.') : 'Не определен'
        } else if (type.equals('UL')) {
            return fields.nameOrg ? fields.nameOrg : 'Не определен'
        }
        return null
    }
}


class EntityDataTable implements DataTable {

    DataObject object;
    HashMap<String, Object> data;

    EntityDataTable(DataObject object) {
        this.object = object
        this.data = new HashMap<>()
    }

    void setData(String name, DataHandler handler) {
        setData(name, handler.fill(object, object.getFields()))
    }

    void setData(String name, Object value) {
        data.put(name, value)
    }

    Object getData(String name) {
        return data.get(name)
    }
}


class Table {

    ArrayList<DataTable> data = new ArrayList<>();
    ArrayList<ColumnTable> column = new ArrayList<>()

    Table() {
        column.add(new ColumnTable("Наименование лица", "name"))
        column.add(new ColumnTable("Дата подписания", "signing_date"))
        column.add(new ColumnTable("Тип присоединения", "type"))
    }

    void addData(DataTable data) {
        for (ColumnTable col in column) {
            // Если одной из колонки нет в данных
            // Не добавляем
            Object value = data.getData(col.getProperty())
            if (value == null) {
                return;
            }
        }
        this.data.add(data)
    }

    ArrayList<DataTable> getData() {
        return this.data
    }

    ArrayList<ColumnTable> getColumn() {
        return column
    }


}


Table table = new Table()



// Создаем описание для фильтра по муниципалитету
LinkedHashMap<String, Object> objectsEntity = [
        link: 'tehprisRSO_WaterSanitationPdk',
        data: [
                Organization:
                        [link: 'tehprisRSO_Organization',
                         data: [Sys_owner: securityDataProvider.loggedInUser.getTenant()]]
        ]]

// Получаем обьекты по фильтру с помощью сторонего скрипта
List<DataObject> objects = integrationsDataProvider
        .runSavedScript('tehprisRSO_getObjectEntity', [entity: objectsEntity])


// Получить значения поля по имени
class StringConstFieldDataHandler extends DataHandler<String> {

    String value;

    StringConstFieldDataHandler(String value) {
        this.value = value
    }

    String data() {
        return value
    }
}

class TermDateFieldDataHandler extends DataHandler<Long> {

    String start;
    String finish;
    TimeUnit timeUnit;

    TermDateFieldDataHandler(String start, String finish, TimeUnit timeUnit) {
        this.start = start;
        this.finish = finish;
        this.timeUnit = timeUnit;
    }

    Long data() {
        if (get(finish).getValue() && get(start).getValue()) {
            long diffInMillies = get(finish).getValue() - get(start).getValue();
            return timeUnit.convert(diffInMillies, timeUnit);
        } else {
            return null
        }
    }
}

// Получить значения поля по имени
class PriceFieldDataHandler extends DataHandler<Long> {

    String payment1;
    String payment2;
    String payment3;

    PriceFieldDataHandler(String payment1, String payment2, String payment3) {
        this.payment1 = payment1
        this.payment2 = payment2
        this.payment3 = payment3

    }

    Long data() {
        if (get(payment1).getValue() && get(payment2).getValue() && get(payment3).getValue()) {
            return get(payment1).getValue() + get(payment2).getValue() + get(payment3).getValue()
        } else {
            return null
        }
    }
}


for (DataObject object in objects) {
    // Водоотведение - описываем модель данных сущности
    EntityDataTable wtPdkDataTable = new EntityDataTable(object)
    wtPdkDataTable.setData("name", new ApplicantFieldDataHandler())
    wtPdkDataTable.setData("id", new IdLinkFieldDataHandler("Id", "tehprisRSO_WaterSanitationPdk"))
    wtPdkDataTable.setData("idAppeal", new StringFieldDataHandler("IdAppeal"))
    wtPdkDataTable.setData("signing_date", new StringFieldDataHandler("DataSigninTU"))
    wtPdkDataTable.setData("type", new StringConstFieldDataHandler("Водоотведение"))
    wtPdkDataTable.setData("tu", new FileLinkFieldDataHandler("Скачать ТУ", "TemplateTU"))
    table.addData(wtPdkDataTable)
}

dashboardContext.context['tehprisRSO_TUTable'] = table
