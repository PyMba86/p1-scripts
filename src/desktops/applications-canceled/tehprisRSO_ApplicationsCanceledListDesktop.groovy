import javax.faces.component.UIComponent;
import java.util.Date;
import java.util.concurrent.TimeUnit;
import ru.osslabs.model.common.filter.FilterElement;
import ru.osslabs.model.common.filter.FilterGroup;
import ru.osslabs.model.workflow.dao.TaskInstance

// Модель колонки
class ColumnTable {

    //Название колонки
    private String header;
    // Ключ колонки
    private String property;

    public ColumnTable(String header, String property) {
        this.header = header;
        this.property = property;
    }

    public String getHeader() {
        return header;
    }

    public String getProperty() {
        return property;
    }
}


interface DataTable {

    void setData(String name, Object value);

    Object getData(String name);

}


abstract class DataHandler<T> {

    private int total;
    private int filtered;
    protected TaskInstance task;

    T fill(TaskInstance task) {
        this.task = task;

        T object = data();

        if (object != null) {
            return object
        } else {
            filtered++
        }
        total++;
        return null
    }

    protected abstract T data();

    protected DataObjectField get(String name) {
        return task.getDataObject().getFields().get(name)
    }
}

class StringFieldDataHandler extends DataHandler<String> {

    String field;

    StringFieldDataHandler(String field) {
        this.field = field
    }

    String data() {
        def value = get(field).getValue()
        return value ? value : 'Не определен'
    }
}

class EmployeeFieldDataHandler extends DataHandler<String> {

    String data() {
        return task.getAssignee().toString()
    }
}

class TaskFieldDataHandler extends DataHandler<String> {

    String data() {
        return task.getName().toString()
    }
}

class TermTaskFieldDataHandler extends DataHandler<String> {

    String data() {
        def term = task.getDueDate()
        return term ? term.toString() : 'Не определен'
    }
}

class TypeTaskFieldDataHandler extends DataHandler<String> {

    String data() {
        return task.getDataObject().getMetaObject().getName().toString()
    }
}

class TaskDataTable implements DataTable {

    TaskInstance task;
    HashMap<String, Object> data;

    TaskDataTable(TaskInstance task) {
        this.task = task
        this.data = new HashMap<>()
    }

    void setData(String name, DataHandler handler) {
        setData(name, handler.fill(task))
    }

    void setData(String name, Object value) {
        data.put(name, value)
    }

    Object getData(String name) {
        return data.get(name)
    }
}


class Table {

    ArrayList<DataTable> data = new ArrayList<>();
    ArrayList<ColumnTable> column = new ArrayList<>()

    Table() {
        column.add(new ColumnTable("Тип процесса", "type"))
        column.add(new ColumnTable("Наименование лица", "name"))
        column.add(new ColumnTable("Задача", "task"))
        column.add(new ColumnTable("Сотрудник", "employee"))
        column.add(new ColumnTable("Срок выполения", "term"))

    }

    void addData(DataTable data) {
        for (ColumnTable col in column) {
            // Если одной из колонки нет в данных
            // Не добавляем
            Object value = data.getData(col.getProperty())
            if (value == null) {
                return;
            }
        }
        this.data.add(data)
    }

    ArrayList<DataTable> getData() {
        return this.data
    }

    ArrayList<ColumnTable> getColumn() {
        return column
    }


}

// Фильтруем задания по названию сущности
def filter = [objectClass: 'tehprisRSO_WaterSanitationPdk']
// Получаем все задания организации
def tasks = workflowDataProvider.getTasks(filter, null, null, null).findAll
        { (it.getAssignee() != null) &&
                (it.getAssignee().getTenant() == securityDataProvider.loggedInUser.getTenant()) }


// tasks[2].getAssignee() // Получает текущая пользователя, который назначен на данную задачу
// tasks[2].getCreated() // Получить дату создания задачи
// tasks[2].getDataObject() // Получить обьект свазянных с задачей
// tasks[2].getDueDate()  // Срок до какого нужно выполнить
// tasks[2].getDuration() // Сколько времени потрачено
// tasks[2].getName() // Получить название задачи
// tasks[2].getOverdueState() // Получить  статус "просрочено" если время вышло
// tasks[3].getAssignee().getTenant() // Получить код организации, к которой привязан пользователь
// tasks[2].getDataObject().getMetaObject().getName() // Получить название сущности


Table table = new Table()

for (TaskInstance task in tasks) {
    TaskDataTable wtPdkDataTable = new TaskDataTable(task)
    wtPdkDataTable.setData("employee", new EmployeeFieldDataHandler())
    wtPdkDataTable.setData("name", new StringFieldDataHandler("Description"))
    wtPdkDataTable.setData("task", new TaskFieldDataHandler())
    wtPdkDataTable.setData("term", new TermTaskFieldDataHandler())
    wtPdkDataTable.setData("type", new TypeTaskFieldDataHandler())
    org.primefaces.context.RequestContext.getCurrentInstance().execute("console.log('" + wtPdkDataTable.getData("type") + "');");
    table.addData(wtPdkDataTable)
}

dashboardContext.context['tehprisRSO_ApplicationsTable'] = table
