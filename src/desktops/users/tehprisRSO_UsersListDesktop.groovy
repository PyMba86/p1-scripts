import javax.faces.component.UIComponent;
import java.util.Date;
import java.util.concurrent.TimeUnit;
import ru.osslabs.model.common.filter.FilterElement;
import ru.osslabs.model.common.filter.FilterGroup;
import ru.osslabs.model.workflow.dao.TaskInstance;
import ru.osslabs.model.security.tenancy.Tenant;
import ru.osslabs.model.security.identity.agents.PlatformUser;

// Модель колонки
class ColumnTable {

    //Название колонки
    private String header;
    // Ключ колонки
    private String property;

    public ColumnTable(String header, String property) {
        this.header = header;
        this.property = property;
    }

    public String getHeader() {
        return header;
    }

    public String getProperty() {
        return property;
    }
}

class Link {
    private String name;
    private String link;

    Link(String name, String link) {
        this.name = name
        this.link = link
    }

    String getName() {
        return name
    }

    void setName(String name) {
        this.name = name
    }

    String getLink() {
        return link
    }

    void setLink(String link) {
        this.link = link
    }
}

class Form {
    private String uniqueId;

    Form(String uniqueId) {
        this.uniqueId = uniqueId
    }

    String getUniqueId() {
        return uniqueId
    }

    void setUniqueId(String uniqueId) {
        this.uniqueId = uniqueId
    }
}


interface DataTable {

    void setData(String name, Object value);

    Object getData(String name);

}


abstract class DataHandler<T> {

    private int total;
    private int filtered;
    protected TaskInstance task;
    protected DataObject object;
    protected LinkedHashMap<String, DataObjectField> fields;

    T fill(TaskInstance task) {
        this.task = task;
        this.object = task.getDataObject()
        this.fields = task.getDataObject().getFields();

        T data = data();

        if (data != null) {
            return data
        } else {
            filtered++
        }
        total++;
        return null
    }

    protected abstract T data();

    protected DataObjectField get(String name) {
        return task.getDataObject().getFields().get(name)
    }
}

class StringFieldDataHandler extends DataHandler<String> {

    String field;

    StringFieldDataHandler(String field) {
        this.field = field
    }

    String data() {
        def fieldObject = get(field);
        if (fieldObject) {
            def value = fieldObject.getValue()
            return value ? value : 'Не определен'
        } else {
            return 'Не определен';
        }
    }
}





class Table {

    ArrayList<DataTable> data = new ArrayList<>();
    ArrayList<ColumnTable> column = new ArrayList<>()

    Table() {
        column.add(new ColumnTable("Имя", "firstName"))
        column.add(new ColumnTable("Фамилия", "lastName"))
        column.add(new ColumnTable("Отчество", "middleName"))
        column.add(new ColumnTable("E-Mail", "email"))
        column.add(new ColumnTable("Организационная единица", "tenant"))
    }

    void addData(DataTable data) {

        this.data.add(data)
    }

    ArrayList<DataTable> getData() {
        return this.data
    }

    ArrayList<ColumnTable> getColumn() {
        return column
    }


}



class UserDataTable implements DataTable {

    PlatformUser user;
    HashMap<String, Object> data;

    UserDataTable(PlatformUser user) {
        this.user = user
        this.data = new HashMap<>()
    }

    void setData(String name, DataHandler handler) {
        setData(name, handler.fill(task))
    }

    void setData(String name, Object value) {
        data.put(name, value)
    }

    Object getData(String name) {
        return data.get(name)
    }
}


// Получить организационную единицу по его коду
Tenant getTenant(String tenantCode) {
    if (tenantCode == null)
        return null;
    return entityManager.find(Tenant.class, tenantCode);
}

// Собрать все ОЕ которые наследуются от переданной ОЕ
void buildTenantList(List<String> list, Tenant tenant) {
    list.add(tenant.getId());
    for (Tenant child : tenant.getChildren())
        buildTenantList(list, child);
}

// Получить пользователей организационной единицы
List<PlatformUser> getTenantUsers(String tenantCode, boolean withSubTenants) {
    Tenant tenant = getTenant(tenantCode);
    List<String> tenants = new ArrayList<>();
    if (withSubTenants)
        buildTenantList(tenants, tenant);
    else
        tenants.add(tenant.getId());
    return securityDataProvider.getUsers(
            ['tenant': tenants],
            userSearch.getFirstResult(),
            userSearch.getMaxResults(),
            userSearch.getOrderColumn() == null ? null : userSearch.getOrderColumn() + " " + userSearch.getOrderDirection())

}

List<PlatformUser> users = new ArrayList<>();
def currentTenant = securityDataProvider.getCurrentTenant();

if (currentTenant!= null) {
    users =  getTenantUsers(currentTenant, true)
}

// Заполняем таблицу пользователями
try {
    Table table = new Table()

    for (PlatformUser user in users) {
        UserDataTable wtPdkDataTable = new UserDataTable(user)
        wtPdkDataTable.setData("login", user.getLoginName())
        wtPdkDataTable.setData("firstName", user.getFirstName())
        wtPdkDataTable.setData("lastName", user.getLastName())
        wtPdkDataTable.setData("middleName", user.getMiddleName())
        wtPdkDataTable.setData("email", user.getEmail())
        wtPdkDataTable.setData("tenant", user.getTenant())
        table.addData(wtPdkDataTable)
    }
    org.primefaces.context.RequestContext.getCurrentInstance().execute("console.log('" + users.size + "');");
    dashboardContext.context['tehprisRSO_UsersListDesktop'] = table
} catch (Exception ex) {
    org.primefaces.context.RequestContext.getCurrentInstance().execute("console.log('" + ex.getStackTrace() + "');");
}

