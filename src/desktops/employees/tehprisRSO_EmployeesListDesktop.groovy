import javax.faces.component.UIComponent;
import java.util.Date;
import java.util.concurrent.TimeUnit;
import ru.osslabs.model.common.filter.FilterElement;
import ru.osslabs.model.common.filter.FilterGroup;
import ru.osslabs.model.workflow.dao.TaskInstance;

// Модель колонки
class ColumnTable {

    //Название колонки
    private String header;
    // Ключ колонки
    private String property;

    public ColumnTable(String header, String property) {
        this.header = header;
        this.property = property;
    }

    public String getHeader() {
        return header;
    }

    public String getProperty() {
        return property;
    }
}

class Link {
    private String name;
    private String link;

    Link(String name, String link) {
        this.name = name
        this.link = link
    }

    String getName() {
        return name
    }

    void setName(String name) {
        this.name = name
    }

    String getLink() {
        return link
    }

    void setLink(String link) {
        this.link = link
    }
}

class Form {
    private String uniqueId;

    Form(String uniqueId) {
        this.uniqueId = uniqueId
    }

    String getUniqueId() {
        return uniqueId
    }

    void setUniqueId(String uniqueId) {
        this.uniqueId = uniqueId
    }
}


interface DataTable {

    void setData(String name, Object value);

    Object getData(String name);

}


abstract class DataHandler<T> {

    private int total;
    private int filtered;
    protected TaskInstance task;
    protected DataObject object;
    protected LinkedHashMap<String, DataObjectField> fields;

    T fill(TaskInstance task) {
        this.task = task;
        this.object = task.getDataObject()
        this.fields = task.getDataObject().getFields();

        T data = data();

        if (data != null) {
            return data
        } else {
            filtered++
        }
        total++;
        return null
    }

    protected abstract T data();

    protected DataObjectField get(String name) {
        return task.getDataObject().getFields().get(name)
    }
}

class StringFieldDataHandler extends DataHandler<String> {

    String field;

    StringFieldDataHandler(String field) {
        this.field = field
    }

    String data() {
        def fieldObject = get(field);
        if (fieldObject) {
            def value = fieldObject.getValue()
            return value ? value : 'Не определен'
        } else {
            return 'Не определен';
        }
    }
}

class EmployeeFieldDataHandler extends DataHandler<String> {

    String data() {
        org.primefaces.context.RequestContext.getCurrentInstance().execute("console.log('" + task.getAssignee().toString() + "');");
        return task.getAssignee().toString()
    }
}

class TaskFieldDataHandler extends DataHandler<String> {

    String data() {
        return task.getName().toString()
    }
}

class TermTaskFieldDataHandler extends DataHandler<String> {

    String data() {
        def term = task.getDueDate()
        return term ? term.toString() : 'Не определен'
    }
}

class TypeTaskFieldDataHandler extends DataHandler<String> {

    String data() {
        return task.getDataObject().getMetaObject().getName().toString()
    }
}

class IdLinkFieldDataHandler extends DataHandler<Form> {

    String field;
    String form;

    IdLinkFieldDataHandler(String field, String form) {
        this.field = field
        this.form = form
    }

    Form data() {
        return new Form(object.getUniqueId())

    }
}

class FileLinkFieldDataHandler extends DataHandler<Link> {

    String field;
    String name;

    FileLinkFieldDataHandler(String name, String field) {
        this.field = field
        this.name = name
    }

    Link data() {
        if (get(field).getValue()) {
            return new Link(name, "/platform/dmsfile/" + get(field).getValue().get(0).uid)
        } else {
            return new Link(name, "#")
        }

    }
}


class ApplicantFieldDataHandler extends DataHandler<String> {

    String data() {
        if (fields.ApplicantType) {
            def ApplicantType = fields.ApplicantType.value
            if (ApplicantType) {
                def type = ApplicantType.fields.Code
                if (type && type.value.equals('FL') || type.value.equals('IP')) {
                    def lastName = fields.lastName.value
                    def firstName = fields.firstName.value
                    def middleName = fields.middleName.value
                    return (lastName && firstName && middleName) ? lastName.concat(' ').concat(firstName.substring(0, 1)).concat('. ')
                            .concat(middleName.substring(0, 1)).concat('.') : 'Не определен'
                } else if (type.value.equals('UL')) {
                    return fields.nameOrg ? fields.nameOrg : 'Не определен'
                }
            }
        } else {
            return null
        }
    }
}

class TaskDataTable implements DataTable {

    TaskInstance task;
    HashMap<String, Object> data;

    TaskDataTable(TaskInstance task) {
        this.task = task
        this.data = new HashMap<>()
    }

    void setData(String name, DataHandler handler) {
        setData(name, handler.fill(task))
    }

    void setData(String name, Object value) {
        data.put(name, value)
    }

    Object getData(String name) {
        return data.get(name)
    }
}


class Table {

    ArrayList<DataTable> data = new ArrayList<>();
    ArrayList<ColumnTable> column = new ArrayList<>()

    Table() {
        column.add(new ColumnTable("Сотрудник", "employee"))
        column.add(new ColumnTable("Наименование лица", "name"))
        column.add(new ColumnTable("Задача", "task"))
        column.add(new ColumnTable("Срок выполения", "term"))
        column.add(new ColumnTable("Тип процесса", "type"))
    }

    void addData(DataTable data) {
        for (ColumnTable col in column) {
            // Если одной из колонки нет в данных
            // Не добавляем
            Object value = data.getData(col.getProperty())
            if (value == null) {
                return;
            }
        }
        this.data.add(data)
    }

    ArrayList<DataTable> getData() {
        return this.data
    }

    ArrayList<ColumnTable> getColumn() {
        return column
    }


}

// Получаем список описаний процессов текущего приложения
def processDefinitionList = workflowDataProvider.getProcessDefinitionListActual().findAll{
    definition -> definition.getApplication() == currentApplication.getName()
};
def objectClassList = [];
for (processDefinition in processDefinitionList) {

    // Получаем список классов которые используют описание процесса
    def classesForProcessDefinition = workflowDataProvider
            .getClassesForProcessDefinition(processDefinition)
            .collect { definition -> return definition.getObjectClass() }

    // Добавляем список классов
    objectClassList.addAll(classesForProcessDefinition)
}

// Список текущих заданий пользователей
def tasks = [];
for (objectClass in objectClassList.toSet()) {
    // Фильтруем задания по названию сущности
    def filter = [objectClass: objectClass]
// Получаем все задания организации
    tasks.addAll(workflowDataProvider.getTasks(filter, null, null, null).findAll
            {
                (it.getAssignee() != null) &&
                        (it.getAssignee().getTenant() == securityDataProvider.loggedInUser.getTenant())
            })
}

// tasks[2].getAssignee() // Получает текущая пользователя, который назначен на данную задачу
// tasks[2].getCreated() // Получить дату создания задачи
// tasks[2].getDataObject() // Получить обьект свазянных с задачей
// tasks[2].getDueDate()  // Срок до какого нужно выполнить
// tasks[2].getDuration() // Сколько времени потрачено
// tasks[2].getName() // Получить название задачи
// tasks[2].getOverdueState() // Получить  статус "просрочено" если время вышло
// tasks[3].getAssignee().getTenant() // Получить код организации, к которой привязан пользователь
// tasks[2].getDataObject().getMetaObject().getName() // Получить название сущности


try {
    Table table = new Table()

    for (TaskInstance task in tasks) {
        TaskDataTable wtPdkDataTable = new TaskDataTable(task)
        wtPdkDataTable.setData("employee", new EmployeeFieldDataHandler())
        wtPdkDataTable.setData("name", new ApplicantFieldDataHandler())
        wtPdkDataTable.setData("id", new IdLinkFieldDataHandler("Id", "tehprisRSO_WaterSanitationPdk"))
        wtPdkDataTable.setData("idAppeal", new StringFieldDataHandler("IdAppeal"))
        wtPdkDataTable.setData("task", new TaskFieldDataHandler())
        wtPdkDataTable.setData("term", new TermTaskFieldDataHandler())
        wtPdkDataTable.setData("type", new TypeTaskFieldDataHandler())
        table.addData(wtPdkDataTable)
    }
    org.primefaces.context.RequestContext.getCurrentInstance().execute("console.log('" + 'ok load' + "');");
    dashboardContext.context['tehprisRSO_EmployeesTable'] = table
} catch (Exception ex) {
    org.primefaces.context.RequestContext.getCurrentInstance().execute("console.log('" + ex.getStackTrace() + "');");
}

