import java.net.URL
import org.postgis.PGgeometry
import ru.osslabs.model.common.filter.FilterGroup

// Изменения:
// - Добавлен tenant на запись (28.06.18)

// Являются данные сущностью
def isDataEntity(object) {
    return object.containsKey('link') && object.containsKey('data')
}

//Создание новой геометрии типа
def createGeometry(value) {
    return new PGgeometry(value)
}

// Получение данных в зависимости от типа
def getData(type, value) {
    switch (type) {
        case 'ReferenceType':
            return getObjectEntity(value).id.toInteger()
            break
        case 'GeometryType':
            return createGeometry(value)
            break
        default:
            return value
            break
    }
}

// Заполнить поля объекта
def fillFieldsObject(entity, data) {
    if (data != null && data.size() > 0 && entity != null) {
        if (isDataEntity(data)) {
            data = data.data
        }
        def fields = [:]
        for (field in data) {
            def typeAttribute = entity.getAttribute(field.key)?.type
            fields[field.key] = getData(typeAttribute, field.value)
        }
        return fields
    }
    return null
}

// Получить элемент сущности по фильтру
def findObjectEntity(name, filter) {
    return externalDataSource.getObjectList(name, null, filter,
            null, null, null, null).getAt(0)
}

// Создание фильтра для поиска элемента (key = value)
def makeFilterFieldEntity(data) {
    def namesField = []
    def valuesField = []
    // Фромирование строки фильтра
    data.eachWithIndex { key, value, index ->
        namesField.add("$key = ?$index")
        valuesField.add(value)
    }
    return new FilterGroup(namesField.join(' and '), valuesField)
}

// Добавление нового элемента
def createObjectEntity(link, fields, tenant) {
    // Создаем новый элемент
    def object = externalDataSource.getNewObject(link)
    updateDataEntity(object, fields)
    // В случае если указан tenant
    if (tenant) {
        object.tenant = tenant
    }
    // Сохраняем в базе
    externalDataSource.updateObject(object)
    return object;
}

def updateDataEntity(object, fields) {
    // Заполняем данными
    fields.each { name, value ->
        object.fields[name]?.value = value
    }
}

def fillDataEntity(entity, data) {
    def dataEntity = externalDataSource.getDataEntity(entity.link)

    return fillFieldsObject(dataEntity, data)
}

// Заполнить поля данными
def fillFieldsData(entity) {
    if (isDataEntity(entity)) {
        return fillDataEntity(entity, entity.data)
    } else {

        // В случае если это массив обьектов
        def fields = [:]
        entity.each { key, value ->
            def fieldObject = getObjectEntity(value)
            fields.put(key, fieldObject.id.toInteger())

        }
        return fields
    }
}

// Получить объект по данным
def getObjectEntity(entity) {
    def fields = fillFieldsData(entity)
    def object = findObjectEntity(entity.link, makeFilterFieldEntity(fields))
    return object != null ? object : createObjectEntity(entity.link, fields, entity.tenant)
}


interface FieldHandler {
    def execute(value)
}

class ReferenceFieldHandler implements FieldHandler {
    String link

    ReferenceFieldHandler(String link) {
        this.link = link
    };

    @Override
    def execute(value) {
        return [
                link: link,
                data: [Description: value.toString()]
        ]
    }
}

class DoubleFieldHandler implements FieldHandler {

    @Override
    def execute(value) {
        return Double.valueOf(value)
    }
}

class IntegerFieldHandler implements FieldHandler {

    @Override
    def execute(value) {
        return Integer.valueOf(value)
    }
}

class StringFieldHandler implements FieldHandler {

    @Override
    def execute(value) {
        return String.valueOf(value)
    }
}

class DateFieldHandler implements FieldHandler {

    @Override
    def execute(value) {
        return value
    }
}

class FieldObject {

    FieldObject(int num, String name, String sv_name, FieldHandler handler) {
        this.num = num
        this.name = name
        this.sv_name = sv_name
        this.handler = handler
    }
    int num;
    String name;
    String sv_name;
    FieldHandler handler;

    def fill(value) {
        return handler.execute(value)
    }
}

def convertObjectListToArray(list, List<FieldObject> fields) {
    def objects = []
    for (object in list) {
        Map<String, String> result = new HashMap<String, String>();
        for (field in fields) {
            def value = object.fields[field.sv_name]?.value
            if (value != null) {
                result.put(field.name, field.fill(value))
            }
        }
        objects.add(result)
    }
    return objects
}




def createObject(data) {
    return [
            link: 'tehprisRSO_ESSupplyCenter',
            tenant: 'AO_Tyumenenergo',
            data: data
    ]
}

// Поиск точек в електросети - тюмень энерго
def contentEntity = [
        link: 'supplyCenter',
        data: [
                Region: [link: 'Regions',
                         data: [Id: '2178']],
                DZO   : [link: 'Organization',
                         data: [Id: '3649']]
        ]]

def periods = integrationsDataProvider.runSavedScript('tehprisRSO_getObjectEntity', [entity: contentEntity])


// Добавляем список полей, который должны быть скопированы из другой сущности
List<FieldObject> availableFields = new ArrayList<FieldObject>();
availableFields.add(new FieldObject(1, 'Latitude', 'longitude', new StringFieldHandler()))
availableFields.add(new FieldObject(1, 'Longitude', 'latitude', new StringFieldHandler()))
availableFields.add(new FieldObject(1, 'Description', 'Description', new StringFieldHandler()))
availableFields.add(new FieldObject(4, 'IndexPlans', 'indexPlans', new ReferenceFieldHandler('tehprisRSO_ref_SupplyCenterIndexPlans')))
availableFields.add(new FieldObject(4, 'IndexLoad', 'indexLoad', new ReferenceFieldHandler('tehprisRSO_ref_SupplyCenterIndexLoad')))
availableFields.add(new FieldObject(1, 'StartExploitation', 'startExploitation', new StringFieldHandler()))
availableFields.add(new FieldObject(1, 'GeoServId', 'geoServId', new IntegerFieldHandler()))
availableFields.add(new FieldObject(1, 'GeoServPg', 'geoServPg', new IntegerFieldHandler()))
availableFields.add(new FieldObject(4, 'CPStatus', 'AssoCPStatus', new ReferenceFieldHandler('tehprisRSO_ref_CPStatus')))

availableFields.add(new FieldObject(4, 'DateOfActual', 'DateOfActual', new DateFieldHandler()))

availableFields.add(new FieldObject(4, 'Number', 'Number', new StringFieldHandler()))
//availableFields.add(new FieldObject(4, 'Zone', 'expZone', new StringFieldHandler()))

availableFields.add(new FieldObject(4, 'ZoneRadius', 'expZone_Radius', new IntegerFieldHandler()))
availableFields.add(new FieldObject(4, 'T1_CapacityInstalled', 't1_CapacityInstalled', new DoubleFieldHandler()))
availableFields.add(new FieldObject(4, 'T1_CapacityReconstruction', 't1_CapacityReconstruction', new DoubleFieldHandler()))
availableFields.add(new FieldObject(4, 'T1_LoadSummer', 't1_LoadSummer', new DoubleFieldHandler()))
availableFields.add(new FieldObject(4, 'T1_LoadWinter', 't1_LoadWinter', new DoubleFieldHandler()))
availableFields.add(new FieldObject(4, 'T2_CapacityInstalled', 't2_CapacityInstalled', new DoubleFieldHandler()))
availableFields.add(new FieldObject(4, 'T2_CapacityReconstruction', 't2_CapacityReconstruction', new DoubleFieldHandler()))
availableFields.add(new FieldObject(4, 'T2_LoadSummer', 't2_LoadSummer', new DoubleFieldHandler()))
availableFields.add(new FieldObject(4, 'T2_LoadWinter', 't2_LoadWinter', new DoubleFieldHandler()))
availableFields.add(new FieldObject(4, 'T3_LoadWinter', 't3_LoadWinter', new DoubleFieldHandler()))
availableFields.add(new FieldObject(4, 'T3_CapacityInstalled', 't3_CapacityInstalled', new DoubleFieldHandler()))
availableFields.add(new FieldObject(4, 'T3_CapacityReconstruction', 't3_CapacityReconstruction', new DoubleFieldHandler()))
availableFields.add(new FieldObject(4, 'T3_LoadSummer', 't3_LoadSummer', new DoubleFieldHandler()))
availableFields.add(new FieldObject(4, 'T4_CapacityInstalled', 't4_CapacityInstalled', new DoubleFieldHandler()))
availableFields.add(new FieldObject(4, 'T4_CapacityReconstruction', 't4_CapacityReconstruction', new DoubleFieldHandler()))
availableFields.add(new FieldObject(4, 'T4_LoadSummer', 't4_LoadSummer', new DoubleFieldHandler()))
availableFields.add(new FieldObject(4, 'T4_LoadWinter', 't4_LoadWinter', new DoubleFieldHandler()))
availableFields.add(new FieldObject(4, 'T5_CapacityInstalled', 't5_CapacityInstalled', new DoubleFieldHandler()))
availableFields.add(new FieldObject(4, 'T5_LoadSummer', 't5_LoadSummer', new DoubleFieldHandler()))
availableFields.add(new FieldObject(4, 'T5_CapacityReconstruction', 't5_CapacityReconstruction', new DoubleFieldHandler()))
availableFields.add(new FieldObject(4, 'T5_LoadWinter', 't5_LoadWinter', new DoubleFieldHandler()))
availableFields.add(new FieldObject(4, 't6_CapacityReconstruction', 't6_CapacityReconstruction', new DoubleFieldHandler()))
availableFields.add(new FieldObject(4, 'T6_CapacityInstalled', 't6_CapacityInstalled', new DoubleFieldHandler()))
availableFields.add(new FieldObject(4, 'T6_LoadSummer', 't6_LoadSummer', new DoubleFieldHandler()))
availableFields.add(new FieldObject(4, 'T6_LoadWinter', 't6_LoadWinter', new DoubleFieldHandler()))
availableFields.add(new FieldObject(4, 'LoadSummerDate', 'loadSummerDate', new DateFieldHandler()))
availableFields.add(new FieldObject(4, 'MaxSummerDate', 'MaxSummerDate', new DateFieldHandler()))
availableFields.add(new FieldObject(4, 'LoadWinterDate', 'loadWinterDate', new DateFieldHandler()))
availableFields.add(new FieldObject(4, 'MaxWinterDate', 'MaxWinterDate', new DateFieldHandler()))
availableFields.add(new FieldObject(4, 'PowerSurplusDeficit', 'powerSurplusDeficit_MVA', new DoubleFieldHandler()))
availableFields.add(new FieldObject(4, 'PowerSurplusDeficitPercent', 'powerSurplusDeficit_percent', new StringFieldHandler()))
availableFields.add(new FieldObject(4, 'PowerClaimTc', 'powerClaimTc', new DoubleFieldHandler()))
availableFields.add(new FieldObject(4, 'PowerContractTc', 'powerContractTc', new DoubleFieldHandler()))
availableFields.add(new FieldObject(4, 'PowerRedistribution', 'powerRedistribution', new DoubleFieldHandler()))
availableFields.add(new FieldObject(4, 'VoltageLevelGIS', 'VoltageLevelGIS', new ReferenceFieldHandler('tehprisRSO_ref_VoltageLevelGIS')))
availableFields.add(new FieldObject(4, 'T1_VoltageClasses', 't1_VoltageClasses', new StringFieldHandler()))
availableFields.add(new FieldObject(4, 'T2_VoltageClasses', 't2_VoltageClasses', new StringFieldHandler()))
availableFields.add(new FieldObject(4, 'T3_VoltageClasses', 't3_VoltageClasses', new StringFieldHandler()))
availableFields.add(new FieldObject(4, 'T4_VoltageClasses', 't4_VoltageClasses', new StringFieldHandler()))
availableFields.add(new FieldObject(4, 'T5_VoltageClasses', 't5_VoltageClasses', new StringFieldHandler()))
availableFields.add(new FieldObject(4, 'T6_VoltageClasses', 't6_VoltageClasses', new StringFieldHandler()))
availableFields.add(new FieldObject(4, 'BandwidthN1', 'BandwidthN1', new StringFieldHandler()))
availableFields.add(new FieldObject(4, 'CurrentPowerReserve', 'CurrentPowerReserve', new DoubleFieldHandler()))
availableFields.add(new FieldObject(4, 'T1_TransformerId', 't1_TransformerId', new StringFieldHandler()))
availableFields.add(new FieldObject(4, 'T2_TransformerId', 't2_TransformerId', new StringFieldHandler()))
availableFields.add(new FieldObject(4, 'T3_TransformerId', 't3_TransformerId', new StringFieldHandler()))
availableFields.add(new FieldObject(4, 'T4_TransformerId', 't4_TransformerId', new StringFieldHandler()))
availableFields.add(new FieldObject(4, 'T5_TransformerId', 't5_TransformerId', new StringFieldHandler()))
availableFields.add(new FieldObject(4, 'T6_TransformerId', 't6_TransformerId', new StringFieldHandler()))
availableFields.add(new FieldObject(4, 'T1_PlannedVoltageClasses', 't1_PlannedVoltageClasses', new StringFieldHandler()))
availableFields.add(new FieldObject(4, 'T2_PlannedVoltageClasses', 't2_PlannedVoltageClasses', new StringFieldHandler()))
availableFields.add(new FieldObject(4, 'T3_PlannedVoltageClasses', 't3_PlannedVoltageClasses', new StringFieldHandler()))
availableFields.add(new FieldObject(4, 'T4_PlannedVoltageClasses', 't4_PlannedVoltageClasses', new StringFieldHandler()))
availableFields.add(new FieldObject(4, 'T5_PlannedVoltageClasses', 't5_PlannedVoltageClasses', new StringFieldHandler()))
availableFields.add(new FieldObject(4, 'T6_PlannedVoltageClasses', 't6_PlannedVoltageClasses', new StringFieldHandler()))
availableFields.add(new FieldObject(4, 'T6_MaxWinter', 't6_MaxWinter', new DoubleFieldHandler()))
availableFields.add(new FieldObject(4, 'T5_MaxWinter', 't5_MaxWinter', new DoubleFieldHandler()))
availableFields.add(new FieldObject(4, 'T4_MaxWinter', 't4_MaxWinter', new DoubleFieldHandler()))
availableFields.add(new FieldObject(4, 'T3_MaxWinter', 't3_MaxWinter', new DoubleFieldHandler()))
availableFields.add(new FieldObject(4, 'T2_MaxWinter', 't2_MaxWinter', new DoubleFieldHandler()))
availableFields.add(new FieldObject(4, 'T1_MaxWinter', 't1_MaxWinter', new DoubleFieldHandler()))
availableFields.add(new FieldObject(4, 'T6_MaxSummer', 't6_MaxSummer', new DoubleFieldHandler()))
availableFields.add(new FieldObject(4, 'T5_MaxSummer', 't5_MaxSummer', new DoubleFieldHandler()))
availableFields.add(new FieldObject(4, 'T4_MaxSummer', 't4_MaxSummer', new DoubleFieldHandler()))
availableFields.add(new FieldObject(4, 'T3_MaxSummer', 't3_MaxSummer', new DoubleFieldHandler()))
availableFields.add(new FieldObject(4, 'T2_MaxSummer', 't2_MaxSummer', new DoubleFieldHandler()))
availableFields.add(new FieldObject(4, 'T1_MaxSummer', 't1_MaxSummer', new DoubleFieldHandler()))
availableFields.add(new FieldObject(4, 'ReserveOrDeficitByContractPercent', 'ReserveOrDeficitByContractPerc', new StringFieldHandler()))
availableFields.add(new FieldObject(4, 'ReserveOrDeficitByContract', 'ReserveOrDeficitByContractMVA', new StringFieldHandler()))




def rows = convertObjectListToArray(periods, availableFields)

for (row in rows) {

//def row = rows[10]
    def code = java.util.UUID.randomUUID().toString()
    org.primefaces.context.RequestContext.getCurrentInstance().execute("console.log('" + row.Latitude + "');");
    def geo = "SRID=4269;POINT(" + row.Latitude + " " +  row.Longitude + ")"
    row.put('Tenant', [link: 'Tenant', data: [Id: '686342']])
    row.put('Organization', [link: 'tehprisRSO_Organization', data: [Id: '688782']])
    row.put('Locality', [link: 'tehprisRSO_Locality', data: [Description: 'Ханты-Мансийск']])
    row.put('Code', code)
    row.put('CSCName', 'АО "Тюменьэнерго"')
    row.put('CSCAddress', 'Ханты-Мансийский Автономный округ - Югра')
    row.put('CSCPhone', '88005535551')
    row.put('Inactive', false)
    row.put('Geom',geo)
// Добавление новой записи
    def object = createObject(row)
// Добавляем тарифную зону как костанту поле

    def res =  getObjectEntity(object)
    res.setDirty(true)
    externalDataSource.updateObject(res)
}
