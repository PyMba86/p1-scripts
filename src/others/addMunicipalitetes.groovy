import java.net.URL
import org.postgis.PGgeometry
import ru.osslabs.model.common.filter.FilterGroup

// Являются данные сущностью
def isDataEntity(object) {
    return object.containsKey('link') && object.containsKey('data')
}

//Создание новой геометрии типа
def createGeometry(value) {
    return new PGgeometry(value)
}

// Получение данных в зависимости от типа
def getData(type, value) {
    switch (type) {
        case 'ReferenceType':
            return getObjectEntity(value).id.toInteger()
            break
        case 'GeometryType':
            return createGeometry(value)
            break
        default:
            return value
            break
    }
}

// Заполнить поля объекта
def fillFieldsObject(entity, data) {
    if (data != null && data.size() > 0 && entity != null) {
        if (isDataEntity(data)) {
            data = data.data
        }
        def fields = [:]
        for (field in data) {
            def typeAttribute = entity.getAttribute(field.key)?.type
            fields[field.key] = getData(typeAttribute, field.value)
        }
        return fields
    }
    return null
}

// Получить элемент сущности по фильтру
def findObjectEntity(name, filter) {
    return externalDataSource.getObjectList(name, null, filter,
            null, null, null, null).getAt(0)
}

// Создание фильтра для поиска элемента (key = value)
def makeFilterFieldEntity(data) {
    def namesField = []
    def valuesField = []
    // Фромирование строки фильтра
    data.eachWithIndex { key, value, index ->
        namesField.add("$key = ?$index")
        valuesField.add(value)
    }
    return new FilterGroup(namesField.join(' and '), valuesField)
}

// Добавление нового элемента
def createObjectEntity(link, fields) {
    // Создаем новый элемент
    def object = externalDataSource.getNewObject(link)
    updateDataEntity(object, fields)
    // Сохраняем в базе
    externalDataSource.updateObject(object)
    return object;
}

def updateDataEntity(object, fields) {
    // Заполняем данными
    fields.each { name, value ->
        object.fields[name]?.value = value
    }
}

def fillDataEntity(entity, data) {
    def dataEntity = externalDataSource.getDataEntity(entity.link)

    return fillFieldsObject(dataEntity, data)
}

// Заполнить поля данными
def fillFieldsData(entity) {
    if (isDataEntity(entity)) {
        return fillDataEntity(entity, entity.data)
    } else {

        // В случае если это массив обьектов
        def fields = [:]
        entity.each { key, value ->
            def fieldObject = getObjectEntity(value)
            fields.put(key, fieldObject.id.toInteger())

        }
        return fields
    }
}

// Получить объект по данным
def getObjectEntity(entity) {
    def fields = fillFieldsData(entity)
    def object = findObjectEntity(entity.link, makeFilterFieldEntity(fields))
    return object != null ? object : createObjectEntity(entity.link, fields)
}

// Обновление данных обьекта
def updateObjectEntity(entity, update) {
    def object = getObjectEntity(entity)
    def fields = fillDataEntity(entity, update)
    // Обновляем данные
    updateDataEntity(object, fields)
    // Сохраняем в базе
    externalDataSource.updateObject(object)
}


def createObject(codes, data) {
    return [
            link: 'tehprisRSO_Municipalities',
            data: [
                    Code       : codes.get(data[1]),
                    Description: data[1]
            ]]
}


def createUpdateObject(data) {
    return [
            link: 'RSO_Organization',
            data: [
                    Locality: [link: 'RSO_Locality',
                               data: [Description: data]]
            ]]
}

class FieldObject {

    FieldObject(int num, String name) {
        this.num = num
        this.name = name
    }
    int num;
    String name;
}

def convertObjectListToArray(list, List<FieldObject> fields) {
    def objects = []
    for (object in list) {
        def result = []
        for (field in fields) {
            def value = object.fields[field.name]?.value
            result.add(field.num, value)
        }
        objects.add(result)
    }
    return objects
}

// Получаем список старых обьектов из сущности
def municipalites = externalDataSource.getObjectList('RSO_Municipalities', null, null, 0, null, null, null)

// Добавляем список полей, который должны быть скопированы из другой сущности
List<FieldObject> availableFields = new ArrayList<FieldObject>();
availableFields.add(new FieldObject(0, 'Code'))
availableFields.add(new FieldObject(1, 'Description'))

def rows = convertObjectListToArray(municipalites, availableFields)

// Список ОКТМО
HashMap<String, String> codes = new HashMap<String, String>();
codes.put("Белоярский район", "71811000");
codes.put("Берёзовский район", "71812000");
codes.put("Когалым", "71883000");
codes.put("Кондинский район", "71816000");
codes.put("Лангепас", "71872000");
codes.put("Мегион", "71873000");
codes.put("Нефтеюганск", "71874000");
codes.put("Нефтеюганский район", "71818000");
codes.put("Нижневартовск", "71875000");
codes.put("Нижневартовский район", "71819000");
codes.put("Нягань", "71879000");
codes.put("Октябрьский район", "71821000");
codes.put("Покачи", "71884000");
codes.put("Пыть-Ях", "71885000");
codes.put("Радужный", "71877000");
codes.put("Советский район", "71824000");
codes.put("Сургут", "71876000");
codes.put("Сургутский район", "71826000");
codes.put("Урай", "71878000");
codes.put("Ханты-Мансийск", "71871000");
codes.put("Ханты-Мансийский район", "71829000");
codes.put("Югорск", "71887000");

for (row in rows) {
    // Обновление записи в сущности
    //def locality = createOrganizationLocality(row[0])
    //def organization = createOrganization(row[1]])
    //updateObjectEntity(organization, locality)*/

    // Добавление новой записи
    def object = createObject(codes, row)
    getObjectEntity(object)

}
