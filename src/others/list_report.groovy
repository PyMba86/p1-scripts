import java.io.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;

def form = inputData.form
///////////////////////////////////////////////////////////////////////////////////////
def objects = externalDataSource.getObjectList(form, null)
outputData.put(createGridData(objects, form))
///////////////////////////////////////////////////////////////////////////////////////

List<Object> createHeaders(String form) {
    return externalDataSource
            .getDataEntity(form)
            .getAttributes()
            .filter { it -> it.isActive() }
            .collect { it -> it.toString() }
}

List<List<Object>> createGridData(List<DataObject> objects, String form) {
    List<List<Object>> data = new ArrayList<>();
    data.add(createHeaders(form))
    for (DataObject object in objects) {
        data.add(convertDataObjectToList(object.getFields()));
    }
    return data;
}

List<Object> convertDataObjectToList(fields) {
    List<Object> list = new ArrayList<>();
    for (field in fields) {
        list.add(field.value.toString())
    }
    return list;
}

/*

objects[0].getFields().getClass()

objects[0].metaClass.methods*.name.sort().unique()

//Название таблицы
externalDataSource.getDataEntity('tehprisRSO_Organization').getName()

// Получить код таблицы
externalDataSource.getDataEntity('tehprisRSO_Organization').getCode()

// Получить название приложения
externalDataSource.getDataEntity('tehprisRSO_Organization').getApplication()

// Получить атрибуты таблицы
externalDataSource.getDataEntity('tehprisRSO_Organization').getAttributes()

//getName, getCode, getCondition, getIndex, getType, isActive
externalDataSource.getDataEntity('tehprisRSO_Organization').getAttributes()[5].toString()

*/

outputData.put(data)
//////////////////////////////////////////////////////////////////////////////////////