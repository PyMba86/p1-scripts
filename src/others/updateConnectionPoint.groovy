import java.net.URL

//  Обновление точки подключения

def createDataObjectEntity(point) {
    return [
            link: 'RSO_ConnectionPoint',
            data: [
                    Description           : point.name,
                    StatusObject          : [link: 'RSO_StatusObject',
                                             data: [Description: point.status]],
                    ActivityOfOrganization: [link: 'RSO_ActivityOfOrganization',
                                             data: [Organization  :
                                                            [link: 'RSO_Organization',
                                                             data: [Description: point.organization]],
                                                    TypeOfActivity:
                                                            [link: 'RSO_TypeOfActivity',
                                                             data: [Description: point.type]]]],
                    VoltageClass          : [link: 'RSO_VoltageClass',
                                             data: [Description: point.voltage, Type: point.voltageType]],
                    Location              : sprintf('SRID=%1$s;%2$s(%3$s)', [point.location.id, point.location.type, point.location.data.join(' ')]),
                    Engine                : point.engine,
                    EngineReserve         : point.engineReserve,
                    EngineShortage        : point.engineShortage,
                    EngineReserveConnect  : point.engineReserve,
                    EngineShortageConnect : point.engineShortage,
                    EngineType            : point.engineType,
            ]]
}

// Загрузить данные которые находятся удаленно
def loadRemoteData(url) {
    return new URL(url).getText()
}

// Форматирование контента строки
def parseCSVContent(data) {
    return data.split('\n').collect { it.split(';') }
}

// Форматирование локации
def formatLocation(data) {
    def coordinates = data.replace(',', '').split(' ')
    return [id: 4269, type: 'POINT', data: [coordinates[1], coordinates[0]]]
}

// Форматирование строки в вещ. число
def formatDouble(data) {
    return Double.parseDouble(data.replace(',', '.'))
}

def createPoint(data) {
    return [name                 : data[0],
            type                 : data[4],
            organization         : data[2],
            location             : formatLocation(data[5]),
            status               : data[6],
            voltage              : data[7].replace(' ',''),
            voltageType          : data[8],
            engine               : formatDouble(data[9]),
            engineReserve        : formatDouble(data[10]),
            engineShortage       : formatDouble(data[11]),
            engineReserveConnect : formatDouble(data[12]),
            engineShortageConnect: formatDouble(data[13]),
            engineType           : data[14]

    ]
}


// Добавление точки подключения сущности
def updateObjectEntity(point, update) {
    def dataEntity = createDataObjectEntity(point)
    return integrationsDataProvider.runSavedScript('RSO_addObjectEntity',
            [command: 'update', entity: dataEntity, update: update])
}

def createData(data) {
    return [Locality: data[3]]
}

// Добавление точек из удаленного файла
def rows = parseCSVContent(loadRemoteData('http://ugraphic.ru/ListES_remote_22_01.csv'))
for (row in rows) {
    def point = createPoint(row)
    def updateData = createData(row)
    // В случае если данные не валидны
    if (updateData.containsValue(null)) {
        org.primefaces.context.RequestContext.getCurrentInstance().execute("console.log('Не валидные данные: " + point.name + "');");
    } else
    {
        updateObjectEntity(point,updateData)
        org.primefaces.context.RequestContext.getCurrentInstance().execute("console.log('" + point.name + "');");
    }
}
