import java.net.URL

// Добавление населенного пункта

def createDataObjectEntity(point) {
    return [
            link: 'RSO_ConnectionPoint',
            data: [
                    Description           : point.name,
                    StatusObject          : [link: 'RSO_StatusObject',
                                             data: [Description: point.status]],
                    ActivityOfOrganization: [link: 'RSO_ActivityOfOrganization',
                                             data: [Organization  :
                                                            [link: 'RSO_Organization',
                                                             data: [Description: point.organization]],
                                                    TypeOfActivity:
                                                            [link: 'RSO_TypeOfActivity',
                                                             data: [Description: point.type]]]],
                    VoltageClass          : [link: 'RSO_VoltageClass',
                                             data: [Description: point.voltage, Type: point.voltageType]],
                    Location              : sprintf('SRID=%1$s;%2$s(%3$s)', [point.location.id, point.location.type, point.location.data.join(' ')]),
                    Engine                : point.engine,
                    EngineReserve         : point.engineReserve,
                    EngineShortage        : point.engineShortage,
                    EngineReserveConnect  : point.engineReserve,
                    EngineShortageConnect : point.engineShortage,
                    EngineType            : point.engineType,
            ]]
}

// Загрузить данные которые находятся удаленно
def loadRemoteData(url) {
    return new URL(url).getText()
}

// Форматирование контента строки
def parseCSVContent(data) {
    return data.split('\n').collect { it.split(';') }
}

def createLocality(data) {
    return [
            municipality: data[0],
            name        : data[1],
            type        : data[2]
    ]
}

// Добавление точки подключения сущности
def addObjectEntity(locality, index) {
    def dataEntity = createDataObjectEntity(locality, index)
    return integrationsDataProvider.runSavedScript('RSO_addObjectEntity', [entity: dataEntity])
}

// Добавление точек из удаленного файла
def rows = parseCSVContent(loadRemoteData('http://ugraphic.ru/RSO_Locality_remote_22_01.csv'))
def index = 1
for (row in rows) {
    def locality = createLocality(row)
    addObjectEntity(locality, index.toString())
    index++
    org.primefaces.context.RequestContext.getCurrentInstance().execute("console.log('" + locality.name + "');");
}

