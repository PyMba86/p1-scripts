import java.net.URL
import org.postgis.PGgeometry
import ru.osslabs.model.common.filter.FilterGroup

// Являются данные сущностью
def isDataEntity(object) {
    return object.containsKey('link') && object.containsKey('data')
}

//Создание новой геометрии типа
def createGeometry(value) {
    return new PGgeometry(value)
}

// Получение данных в зависимости от типа
def getData(type, value) {
    switch (type) {
        case 'ReferenceType':
            return getObjectEntity(value).id.toInteger()
            break
        case 'GeometryType':
            return createGeometry(value)
            break
        default:
            return value
            break
    }
}

// Заполнить поля объекта
def fillFieldsObject(entity, data) {
    if (data != null && data.size() > 0 && entity != null) {

        if (isDataEntity(data)) {
            data = data.data
        }
        def fields = [:]
        for (field in data) {
            def typeAttribute = entity.getAttribute(field.key)?.type
            fields[field.key] = getData(typeAttribute, field.value)
        }
        return fields
    }
    return null
}

// Получить элемент сущности по фильтру
def findObjectEntity(name, filter) {
    return externalDataSource.getObjectList(name, null, filter,
            null, null, null, null).getAt(0)
}

// Создание фильтра для поиска элемента (key = value)
def makeFilterFieldEntity(data) {
    def namesField = []
    def valuesField = []
    // Фромирование строки фильтра
    data.eachWithIndex { key, value, index ->
        namesField.add("$key = ?$index")
        valuesField.add(value)
    }
    return new FilterGroup(namesField.join(' and '), valuesField)
}

// Добавление нового элемента
def createObjectEntity(link, fields, tenant) {
    // Создаем новый элемент
    def object = externalDataSource.getNewObject(link)
    updateDataEntity(object, fields)
    // В случае если указан tenant
    if (tenant) {
        object.tenant = tenant
    }
    // Сохраняем в базе
    externalDataSource.updateObject(object)
    return object;
}

def updateDataEntity(object, fields) {
    // Заполняем данными
    fields.each { name, value ->
        object.fields[name]?.value = value
    }
}

def fillDataEntity(entity, data) {
    def dataEntity = externalDataSource.getDataEntity(entity.link)

    return fillFieldsObject(dataEntity, data)
}

// Заполнить поля данными
def fillFieldsData(entity) {
    if (isDataEntity(entity)) {
        return fillDataEntity(entity, entity.data)
    } else {

        // В случае если это массив обьектов
        def fields = [:]
        entity.each { key, value ->
            def fieldObject = getObjectEntity(value)
            fields.put(key, fieldObject.id.toInteger())

        }
        return fields
    }
}

// Получить объект по данным
def getObjectEntity(entity) {
    def fields = fillFieldsData(entity)
    def object = findObjectEntity(entity.link, makeFilterFieldEntity(fields))
    return object != null ? object : createObjectEntity(entity.link, fields, entity.tenant)
}

def createObject(data) {
    return [
            link: 'tehprisRSO_CalcRateScientificCalculator',
            tenant: null,
            data: data
    ]
}

interface FieldHandler {
    def execute(value)
}

class ReferenceFieldHandler implements FieldHandler {
    String link

    ReferenceFieldHandler(String link) {
        this.link = link
    };

    @Override
    def execute(value) {
        return [
                link: link,
                data: [Description: value.toString()]
        ]
    }
}

class DoubleFieldHandler implements FieldHandler {

    @Override
    def execute(value) {
        return Double.valueOf(value)
    }
}

class StringFieldHandler implements FieldHandler {

    @Override
    def execute(value) {
        return value
    }
}

class FieldObject {

    FieldObject(int num, String name, String sv_name, FieldHandler handler) {
        this.num = num
        this.name = name
        this.sv_name = sv_name
        this.handler = handler
    }
    int num;
    String name;
    String sv_name;
    FieldHandler handler;

    def fill(value) {
        return handler.execute(value)
    }
}

def convertObjectListToArray(list, List<FieldObject> fields) {
    def objects = []
    for (object in list) {
        Map<String, String> result = new HashMap<String, String>();
        for (field in fields) {
            def value = object.fields[field.sv_name]?.value
            if (value != null) {
                result.put(field.name, field.fill(value))
            }
        }
        objects.add(result)
    }
    return objects
}

// Получаем список старых обьектов из сущности
def entityCatalogs = [
        link: 'calcRate',
        data: [
                tariffZone: [link: 'tariffZones',
                             data: [Description: 'Ханты-Мансийский автономный округ - Югра (АО "Тюменьэнерго")']]
        ]]

def periods = integrationsDataProvider.runSavedScript('RSO_getObjectEntity', [entity: entityCatalogs])

// Добавляем список полей, который должны быть скопированы из другой сущности
List<FieldObject> availableFields = new ArrayList<FieldObject>();
availableFields.add(new FieldObject(0, 'Code', 'Code', new StringFieldHandler()))
availableFields.add(new FieldObject(1, 'Description', 'Description', new StringFieldHandler()))
availableFields.add(new FieldObject(4, 'RateType', 'rateType', new ReferenceFieldHandler('tehprisRSO_RateTypeScientificCalculator')))
availableFields.add(new FieldObject(4, 'RateMethodCalc', 'rateMethodCalc', new ReferenceFieldHandler('tehprisRSO_ref_CalcMethod')))
availableFields.add(new FieldObject(4, 'RateChange', 'rateChange', new StringFieldHandler()))
availableFields.add(new FieldObject(4, 'RateStandYear', 'rateStandYear', new StringFieldHandler()))
availableFields.add(new FieldObject(4, 'RateStandCurrent', 'rateStandCurrent', new StringFieldHandler()))
availableFields.add(new FieldObject(4, 'RatePowerYear', 'ratePowerYear', new StringFieldHandler()))
availableFields.add(new FieldObject(4, 'RatePowerCurrent', 'ratePowerCurrent', new StringFieldHandler()))
availableFields.add(new FieldObject(4, 'RateRangeMin', 'rateRangeMin', new StringFieldHandler()))
availableFields.add(new FieldObject(4, 'RateRangeMax', 'rateRangeMax', new StringFieldHandler()))
availableFields.add(new FieldObject(4, 'Multiplier', 'multiplier', new DoubleFieldHandler()))
availableFields.add(new FieldObject(4, 'RateYear', 'rateYear', new ReferenceFieldHandler('tehprisRSO_ref_CheckYear')))
availableFields.add(new FieldObject(4, 'RateRangeMinSign', 'rateRangeMinSign', new ReferenceFieldHandler('tehprisRSO_ref_RangeMinSign')))
availableFields.add(new FieldObject(4, 'RateRangeMaxSign', 'rateRangeMaxSign', new ReferenceFieldHandler('tehprisRSO_ref_RangeMaxSign')))
availableFields.add(new FieldObject(4, 'ObjectConstruction', 'objectConstruction', new ReferenceFieldHandler('tehprisRSO_ref_ObjectConstruction')))
availableFields.add(new FieldObject(4, 'ObjectType', 'objectType', new ReferenceFieldHandler('tehprisRSO_ref_CalcObjectType')))
availableFields.add(new FieldObject(4, 'RatePeriodYear', 'ratePeriodYea', new ReferenceFieldHandler('tehprisRSO_ref_CheckYear')))
availableFields.add(new FieldObject(4, 'RatePeriodQuarte', 'ratePeriodQuarte', new ReferenceFieldHandler('tehprisRSO_ref_TypePeriod')))


def rows = convertObjectListToArray(periods, availableFields)

for (row in rows) {

    row.put('TariffZone', [link: 'tehprisRSO_TariffZonesScientificCalculator', data: [Id: '684531']])
    // Добавление новой записи
    def object = createObject(row)
    // Добавляем тарифную зону как костанту поле

    getObjectEntity(object)

}

