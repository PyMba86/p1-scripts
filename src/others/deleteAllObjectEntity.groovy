// Скрипт для удаление всех записей сущности

//  Пример однократного вызова
/*
integrationsDataProvider.runSavedScript('RSO_deleteAllObjectEntity', ['link'])
*/

externalDataSource.getObjectList(link, null).each {
    externalDataSource.deleteObject(it)
}