import java.net.URL
import org.postgis.PGgeometry
import ru.osslabs.model.common.filter.FilterGroup

// Являются данные сущностью
def isDataEntity(object) {
    return object.containsKey('link') && object.containsKey('data')
}

//Создание новой геометрии типа
def createGeometry(value) {
    return new PGgeometry(value)
}

// Получение данных в зависимости от типа
def getData(type, value) {
    switch (type) {
        case 'ReferenceType':
            return getObjectEntity(value).id.toInteger()
            break
        case 'GeometryType':
            return createGeometry(value)
            break
        default:
            return value
            break
    }
}

// Заполнить поля объекта
def fillFieldsObject(entity, data) {
    if (data != null && data.size() > 0 && entity != null) {
        if (isDataEntity(data)) {
            data = data.data
        }
        def fields = [:]

        for (field in data) {
            def typeAttribute = entity.getAttribute(field.key)?.type
            fields[field.key] = getData(typeAttribute, field.value)
        }
        return fields
    }
    return null
}

// Получить элемент сущности по фильтру
def findObjectEntity(name, filter) {
    return externalDataSource.getObjectList(name, null, filter,
            null, null, null, null).getAt(0)
}

// Создание фильтра для поиска элемента (key = value)
def makeFilterFieldEntity(data) {
    def namesField = []
    def valuesField = []
    // Фромирование строки фильтра
    data.eachWithIndex { key, value, index ->
        namesField.add("$key = ?$index")
        valuesField.add(value)
    }
    return new FilterGroup(namesField.join(' and '), valuesField)
}

// Добавление нового элемента
def createObjectEntity(link, fields, tenant) {
    // Создаем новый элемент
    def object = externalDataSource.getNewObject(link)
    updateDataEntity(object, fields)
    // В случае если указан tenant
    if (tenant) {
        object.tenant = tenant
    }
    // Сохраняем в базе
    externalDataSource.updateObject(object)
    return object;
}

def updateDataEntity(object, fields) {
    // Заполняем данными
    fields.each { name, value ->
        object.fields[name]?.value = value
    }
}

def fillDataEntity(entity, data) {
    def dataEntity = externalDataSource.getDataEntity(entity.link)

    return fillFieldsObject(dataEntity, data)
}

// Заполнить поля данными
def fillFieldsData(entity) {
    if (isDataEntity(entity)) {
        return fillDataEntity(entity, entity.data)
    } else {

        // В случае если это массив обьектов
        def fields = [:]
        entity.each { key, value ->
            def fieldObject = getObjectEntity(value)
            fields.put(key, fieldObject.id.toInteger())

        }
        return fields
    }
}

// Получить объект по данным
def getObjectEntity(entity) {
    def fields = fillFieldsData(entity)
    def object = findObjectEntity(entity.link, makeFilterFieldEntity(fields))
    return object
}

// Обновление данных обьекта
def updateObjectEntity(entity, update) {
    def object = getObjectEntity(entity)
    if (object) {
        def fields = fillDataEntity(entity, update)
        // Обновляем данные
        updateDataEntity(object, fields)
        // Сохраняем в базе
        externalDataSource.updateObject(object)
    } else {
        org.primefaces.context.RequestContext.getCurrentInstance().execute("console.log(' Нет записи: " + entity + "');");
    }
}


interface FieldHandler {
    def execute(value)
}

class ReferenceFieldHandler implements FieldHandler {
    String link

    ReferenceFieldHandler(String link) {
        this.link = link
    };

    @Override
    def execute(value) {
        return [
                link: link,
                data: [Description: value.toString()]
        ]
    }
}

class StringFieldHandler implements FieldHandler {

    @Override
    def execute(value) {
        return value.toString()
    }
}

class DoubleFieldHandler implements FieldHandler {

    @Override
    def execute(value) {
        return Double.valueOf(value)
    }
}

class FieldObject {

    FieldObject(int num, String name, FieldHandler handler) {
        this.num = num
        this.name = name
        this.handler = handler
    }
    int num;
    String name;
    FieldHandler handler;

    def fill(value) {
        return handler.execute(value)
    }
}

def convertObjectListToArray(list, List<FieldObject> fields) {
    def objects = []
    for (object in list) {
        Map<String, String> result = new HashMap<String, String>();
        for (field in fields) {
            def value = object.fields[field.name]?.value
            if (value != null) {
                result.put(field.name, field.fill(value))
            }
        }
        objects.add(result)
    }
    return objects
}


def createObject(data) {
    return [
            link: 'RSO_ConnectionPoint',
            data: [
                    Description: data.Description,

            ]
    ]
}


def createUpdateObject(data) {
    return [
            link: 'RSO_ConnectionPoint',
            data: [
                    startExploitation          : data.startExploitation,
                    powerSurplusDeficit_MVA    : data.powerSurplusDeficit_MVA,
                    powerSurplusDeficit_percent: data.powerSurplusDeficit_percent,
                    t1_CapacityInstalled       : data.t1_CapacityInstalled,
                    t2_CapacityInstalled       : data.t2_CapacityInstalled,
                    t3_CapacityInstalled       : data.t3_CapacityInstalled,
                    t4_CapacityInstalled       : data.t4_CapacityInstalled,
                    t5_CapacityInstalled       : data.t5_CapacityInstalled,
                    t6_CapacityInstalled       : data.t6_CapacityInstalled,
                    indexLoad                  : data.indexLoad,
                    indexPlans                 : data.indexPlans,
                    orgUnit                    : data.orgUnit

            ]]
}

// Получаем список старых обьектов из сущности
def filter = FilterGroup.single('Region', FilterElement.FilterOperator.equals, Collections.singletonList('2178'))
def periods = externalDataSource.getObjectList('supplyCenter', null, filter, 0, null, null, null)

// Добавляем список полей, который должны быть скопированы из другой сущности
List<FieldObject> availableFields = new ArrayList<FieldObject>();
availableFields.add(new FieldObject(0, 'Code', new StringFieldHandler()))
availableFields.add(new FieldObject(1, 'Description', new StringFieldHandler()))
availableFields.add(new FieldObject(1, 'startExploitation', new StringFieldHandler()))
availableFields.add(new FieldObject(1, 'powerSurplusDeficit_MVA', new DoubleFieldHandler()))
availableFields.add(new FieldObject(1, 'powerSurplusDeficit_percent', new StringFieldHandler()))
availableFields.add(new FieldObject(1, 't1_CapacityInstalled', new DoubleFieldHandler()))
availableFields.add(new FieldObject(1, 't2_CapacityInstalled', new DoubleFieldHandler()))
availableFields.add(new FieldObject(1, 't3_CapacityInstalled', new DoubleFieldHandler()))
availableFields.add(new FieldObject(1, 't4_CapacityInstalled', new DoubleFieldHandler()))
availableFields.add(new FieldObject(1, 't5_CapacityInstalled', new DoubleFieldHandler()))
availableFields.add(new FieldObject(1, 't6_CapacityInstalled', new DoubleFieldHandler()))
availableFields.add(new FieldObject(1, 'orgUnit', new StringFieldHandler()))
availableFields.add(new FieldObject(2, 'indexLoad', new ReferenceFieldHandler('RSO_ref_SupplyCenterIndexLoad')))
availableFields.add(new FieldObject(3, 'indexPlans', new ReferenceFieldHandler('RSO_ref_SupplyCenterIndexPlans')))


def rows = convertObjectListToArray(periods, availableFields)

for (row in rows) {

    // Обновление записи в сущности
    def object = createObject(row)
    def update = createUpdateObject(row)
    updateObjectEntity(object, update)

}