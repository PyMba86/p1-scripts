import org.postgis.PGgeometry
import ru.osslabs.model.common.filter.FilterGroup

// Скрипт для добавления объекта сущности

//  Пример однократного вызова
/*
def entity = [
        link: 'RSO_ConnectionPoint',
        data: [
                Description           : 'Тестовая',
                StatusObject          : [link: 'RSO_StatusObject',
                                         data: [Description: 'Действующий']],
                ActivityOfOrganization: [link: 'RSO_ActivityOfOrganization',
                                         data: [Organization  :
                                                        [link: 'RSO_Organization',
                                                         data: [Description: 'ОАО "ЮТЭК-Региональные сети"']],
                                                TypeOfActivity:
                                                        [link: 'RSO_TypeOfActivity',
                                                         data: [Description: 'Электроснабжение']]]],
                VoltageClass          : [link: 'RSO_VoltageClass',
                                         data: [Description: 'Действующий']],
                Location              : 'SRID=4269;POINT(37.18145802 55.71919545)',
                Status                : 'N',
                Engine                : 8.0,
                EngineReserve         : 8.0,
                EngineShortage        : 8.0,
                EngineReserveConnect  : 8.0,
                EngineShortageConnect : 8.0,
        ]]
integrationsDataProvider.runSavedScript('RSO_addObjectEntity', [command: 'add' ,entity, update])
*/

// Являются данные сущностью
def isDataEntity(object) {
    return object.containsKey('link') && object.containsKey('data')
}

//Создание новой геометрии типа
def createGeometry(value) {
    return new PGgeometry(value)
}

// Получение данных в зависимости от типа
def getData(type, value) {
    switch (type) {
        case 'ReferenceType':
            return getObjectEntity(value).id.toInteger()
            break
        case 'GeometryType':
            return createGeometry(value)
            break
        default:
            return value
            break
    }
}

// Заполнить поля объекта
def fillFieldsObject(entity, data) {
    if (data != null && data.size() > 0 && entity != null) {
        def fields = [:]
        for (field in data) {
            def typeAttribute = entity.getAttribute(field.key)?.type
            fields[field.key] = getData(typeAttribute, field.value)
        }
        return fields
    }
    return null
}

// Получить элемент сущности по фильтру
def findObjectEntity(name, filter) {
    return externalDataSource.getObjectList(name, null, filter,
            null, null, null, null).getAt(0)
}

// Создание фильтра для поиска элемента (key = value)
def makeFilterFieldEntity(data) {
    def namesField = []
    def valuesField = []
    // Фромирование строки фильтра
    data.eachWithIndex { key, value, index ->
        namesField.add("$key = ?$index")
        valuesField.add(value)
    }
    return new FilterGroup(namesField.join(' and '), valuesField)
}

// Добавление нового элемента
def createObjectEntity(link, fields) {
    // Создаем новый элемент
    def object = externalDataSource.getNewObject(link)
    updateDataEntity(object, fields)
    // Сохраняем в базе
    externalDataSource.updateObject(object)
    return object;
}

def updateDataEntity(object, fields) {
    // Заполняем данными
    fields.each { name, value ->
        object.fields[name]?.value = value
    }
}

def fillDataEntity(entity, data) {
    def dataEntity = externalDataSource.getDataEntity(entity.link)
    return fillFieldsObject(dataEntity, data)
}

// Заполнить поля данными
def fillFieldsData(entity) {
    if (isDataEntity(entity)) {
        return fillDataEntity(entity, entity.data)
    } else {
        // В случае если это массив обьектов
        def fields = [:]
        entity.each { key, value ->
            def fieldObject = getObjectEntity(value)
            fields.put(key, fieldObject.id.toInteger())
        }
        return fields
    }
}

// Получить объект по данным
def getObjectEntity(entity) {
    def fields = fillFieldsData(entity)
    def object = findObjectEntity(entity.link, makeFilterFieldEntity(fields))
    org.primefaces.context.RequestContext.getCurrentInstance().execute("console.log('" + object + "');");
    return object != null ? object : createObjectEntity(entity.link, fields)
}

// Обновление данных обьекта
def updateObjectEntity(entity) {
    def object = getObjectEntity(entity)
    def fields = fillDataEntity(entity, entity.update)

    // Обновляем данные
    updateDataEntity(object, fields)
    // Сохраняем в базе
    externalDataSource.updateObject(object)
}


switch (command) {
    case 'add':
        if (isDataEntity(entity))
            return getObjectEntity(entity)
        break
    case 'update':
        return updateObjectEntity(entity)
        break
}
