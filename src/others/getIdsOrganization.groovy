import ru.osslabs.model.datasource.DataObject;
import ru.osslabs.model.datasource.DataObjectField;
import ru.osslabs.model.datasource.DataObjectList;
import java.util.LinkedHashMap;

// Входные параметры
// locality - Населенный пункт
// typeOfActivity - Тип деятельности

// Обработчик по получению данных из DataObject
// Получает обьект, Заполняет новый обьект по определенным полям
abstract class DataHandler<T> {

    private final Callback<T> callback;
    private DataProvider dataProvider;
    private int total;
    private int filtered;
    private LinkedHashMap<String, DataObjectField> fields;
    private String id;

    DataHandler(Callback<T> callback) {
        this.callback = callback;
    }

    void fill(LinkedHashMap<String, DataObjectField> fields) {
        this.fields = fields;

        T object = data();

        if (object != null) {
            callback.data(object)
        } else {
            filtered++
        }
        total++;
    }

    protected abstract T data();

    protected DataObjectField get(String name) {
        return fields.get(name)
    }

    String getId() {
        return id
    }

    void setId(String id) {
        this.id = id
    }

    void setDataProvider(DataProvider dataProvider) {
        this.dataProvider = dataProvider
    }

    DataProvider getDataProvider() {
        return dataProvider
    }

    static interface Callback<T> {
        void data(T object)
    }
}

// Обработчик по организациям
class OrganizationDataHandler extends DataHandler<String> {

    private String localityId

    OrganizationDataHandler(Callback<String> callback, String localityId) {
        super(callback)
        this.localityId = localityId
    }

    boolean containsLocality(DataObjectList localityDataObjectList) {
        for (localityDataObject in localityDataObjectList) {
            if (localityId.equals(localityDataObject.id)) return true;
        }
        return false;
    }

    String data() {
        // Принимает ActivityOfOrganization
        DataObject orgDataObject = get('Organization').getValue()
        DataObjectList localityDataObject = get('Locality').getValue()
        if (orgDataObject && containsLocality(localityDataObject)) {
            return orgDataObject.id;

        } else {
            return null
        }

    }
}

/////////////////////////////////////////////////////////////////////////////////////////////////
// Поставшик данных

class DataProvider {

    def externalDatasource;
    def integrationsDataProvider;

    void getData(DataObject dataObject, DataHandler handler) {
        // Передаем поставшика для того чтобы можно было получать данные из других сущностей
        handler.setDataProvider(this)
        // Добоавляем id записи
        handler.setId(dataObject.getId())
        // Вызывает кэлбэк после заполнения обьекта
        handler.fill(dataObject.getFields())
    }

// Получаем данные из списка обьектов с обработкой их в хэндлере
    void getData(List<DataObject> dataObjectList, DataHandler handler) {
        for (DataObject dataObject : dataObjectList) {
            getData(dataObject, handler)
        }
    }

// Получаем данные из сущности с обработкой их в хэнделере
    void getData(String entityName, DataHandler handler) {
        this.getData(this.externalDataSource.getObjectList(entityName, null)
                as List<DataObject>, handler)
    }

    void getData(LinkedHashMap<String, Object> entity, DataHandler handler) {
        // Получаем обьекты по фильтру
        List<DataObject> objectList = integrationsDataProvider
                .runSavedScript('RSO_getObjectEntity', [entity: entity])
        // Заполняем список
        this.getData(objectList, handler)
    }

    void setExternalDataSource(externalDatasource) {
        this.externalDatasource = externalDatasource
    }

    def getExternalDataSource() {
        return this.externalDatasource;
    }

    void setIntegrationsDataProvider(integrationsDataProvider) {
        this.integrationsDataProvider = integrationsDataProvider
    }

}

List<String> resultSelectLocalities = lines.stream()
        .filter({ selectLocality -> localities.containsKey(selectLocality) })
        .collect(Collectors.toList());

// Получить организации по населенному пункту
ArrayList<String> getOrganization(DataProvider dataProvider, String localityId) {
    final ArrayList<String> organizationIds = new ArrayList<>();

    // Создаем описание для фильтра по муниципалитету
    LinkedHashMap<String, Object> activityOfOrganizationEntity = [
            link: 'RSO_ActivityOfOrganization',
            data: [
                    TypeOfActivity:
                            [link: 'RSO_TypeOfActivity',
                             data: [Id: typeOfActivity]]
            ]]

    // Получаем обьекты по фильтру с помощью сторонего скрипта
    List<DataObject> dataOrganizationObjectList = integrationsDataProvider
            .runSavedScript('RSO_getObjectEntity', [entity: activityOfOrganizationEntity])

    // Заполняем список населенных пунктов
    dataProvider.getData(dataOrganizationObjectList,
            new OrganizationDataHandler(
                    { organization -> organizationIds.add(organization.id) }, localityId))
    return organizations;
}

// Создаем поставшика и добавляем источник данных
DataProvider dataProvider = new DataProvider()
dataProvider.setExternalDataSource(externalDataSource)
dataProvider.setIntegrationsDataProvider(integrationsDataProvider)

// Заполняем список организаций по населенному пункту
return getOrganization(dataProvider, locality)
649721 649605
662264 641506
currentProcessController.dataObject.fields.Locality.value.id
currentProcessController.dataObject.fields.TypeOfActivity.value.id
integrationsDataProvider.runSavedScript('RSO_getIdsOrganization', {
    'locality': currentProcessController.dataObject.fields.Locality.value.id, 'typeOfActivity':
    currentProcessController.dataObject.fields.TypeOfActivity.value.id
})

