/**
 * Получить исполнителей мероприятий
 */

// Получить исполнителей мероприятий по заявке
// Входные параметры
// events - сущность RSO_FormsEvents тип DataObjectList

import ru.osslabs.model.datasource.DataObject;
import ru.osslabs.model.datasource.DataObjectList;
import org.apache.commons.lang3.StringUtils;


ArrayList<String> executors = new ArrayList<String>()

DataObjectList events = currentProcessFields.EventsT
for (DataObject object in events) {
    String login = object.value.fields.loginName.value
    // Если нет в списке, только уникальные логины
    if (!executors.contains(login)) {
        executors.add(login)
    }
}

return executors
