/**
 * Получение ссылки на файл библиотеки
 */

import ru.osslabs.model.datasource.DataObject;

// Описываем сущность
def entity = [
        link: 'tehprisRSO_LibFiles',
        data: [Code :  name]
]

DataObject object = integrationsDataProvider.runSavedScript('tehprisRSO_getObjectEntity', [entity: entity]).getAt(0)

return '../dmsfile/' + object.fields.File.value.get(0).uid