/**
 * Получение объекта сущности
 */

import org.postgis.PGgeometry
import ru.osslabs.model.common.filter.FilterGroup

// Скрипт для добавления объекта сущности

//  Пример однократного вызова
/*
def entity = [
        link: 'RSO_ConnectionPoint',
        data: [
                Description           : 'Тестовая',
                StatusObject          : [link: 'RSO_StatusObject',
                                         data: [Description: 'Действующий']],
                ActivityOfOrganization: [link: 'RSO_ActivityOfOrganization',
                                         data: [Organization  :
                                                        [link: 'RSO_Organization',
                                                         data: [Description: 'ОАО "ЮТЭК-Региональные сети"']],
                                                TypeOfActivity:
                                                        [link: 'RSO_TypeOfActivity',
                                                         data: [Description: 'Электроснабжение']]]],
                VoltageClass          : [link: 'RSO_VoltageClass',
                                         data: [Description: 'Действующий']],
                Location              : 'SRID=4269;POINT(37.18145802 55.71919545)',
                Status                : 'N',
                Engine                : 8.0,
                EngineReserve         : 8.0,
                EngineShortage        : 8.0,
                EngineReserveConnect  : 8.0,
                EngineShortageConnect : 8.0,
        ]]
integrationsDataProvider.runSavedScript('RSO_addObjectEntity', [entity])
*/

// Являются данные сущностью
def isDataEntity(object) {
    return object.containsKey('link') && object.containsKey('data')
}

//Создание новой геометрии типа
def createGeometry(value) {
    return new PGgeometry(value)
}

// Получение данных в зависимости от типа
def getData(type, value) {
    switch (type) {
        case 'ReferenceType':
            return getObjectEntity(value).id.toInteger()
            break
        case 'GeometryType':
            return createGeometry(value)
            break
        default:
            return value
            break
    }
}

// Заполнить поля объекта
def fillFieldsObject(entity, data) {
    if (data != null && data.size() > 0 && entity != null) {
        def fields = [:]
        for (field in data) {
            def typeAttribute = entity.getAttribute(field.key)?.type
            fields[field.key] = getData(typeAttribute, field.value)
        }
        return fields
    }
    return null
}

// Получить элемент сущности по фильтру
def findObjectEntity(name, filter) {
    if (name == entity.link)
    {
        return externalDataSource.getObjectList(name, null, filter,
                null, null, null, null)
    } else {
        return externalDataSource.getObjectList(name, null, filter,
                null, null, null, null).getAt(0)
    }
}

// Создание фильтра для поиска элемента (key = value)
def makeFilterFieldEntity(data) {
    def namesField = []
    def valuesField = []
    // Фромирование строки фильтра
    data.eachWithIndex { key, value, index ->
        namesField.add("$key = ?$index")
        valuesField.add(value)
    }
    return new FilterGroup(namesField.join(' and '), valuesField)
}

// Добавление нового элемента
def createObjectEntity(link, fields) {
    // Создаем новый элемент
    def object = externalDataSource.getNewObject(link)

    // Заполняем данными
    fields.each { name, value ->
        object.fields[name]?.value = value
    }

    // Сохраняем в базе
    externalDataSource.updateObject(object)
    return object;
}

// Получить объект по данным
def getObjectEntity(entity) {
    def fields = [:]
    //  В случае если это обьект
    if (isDataEntity(entity)) {
        def dataEntity = externalDataSource.getDataEntity(entity.link)
        fields = fillFieldsObject(dataEntity, entity.data)
    } else {
        // В случае если это массив обьектов
        entity.each { key, value ->
            def fieldObject = getObjectEntity(value)
            fields.put(key, fieldObject.id.toInteger())
        }
    }
    def object = findObjectEntity(entity.link, makeFilterFieldEntity(fields))
    return object
}

return getObjectEntity(entity)