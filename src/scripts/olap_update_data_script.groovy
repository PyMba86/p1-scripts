/**
 * Обновление данных в построителе запросов
 */

import mondrian.olap.Connection;
import org.olap4j.OlapDataSource;
import org.olap4j.OlapConnection;
import org.olap4j.CellSet;
import org.olap4j.mdx.parser.MdxParser;
import org.olap4j.mdx.parser.MdxParserFactory;
import org.olap4j.OlapStatement

import javax.xml.bind.JAXBContext
import  ru.osslabs.olap.xml.Schema
def app =  applicationHolder.application
olapController.getCubeNames(app).each{ cubeName->
    olapController.clearCache(cubeName)
}
renewCubes('all', app)

void renewCubes(String cubeName, app) {
    OlapConnection connection = getConnection();
    def reports=reportRepository.getFiles(reportRepository.getRoot())
    if(app==null){
        reports = reports.findAll {it.application == null}
    } else {
        reports = reports.findAll {it.application.equals(app.name)}
    }

    reports.each { report ->
        println report
        def file=report.content
        try {
            def xml = new XmlSlurper().parseText(file)
            def query = xml.model.mdx.text()
            def from = getContainsParam(['FROM', 'from', 'From'], query)
            def where = getContainsParam(['WHERE', 'where', 'Where'], query)
            def condition = where == null ?
                    (query.split(from)[1].contains('[' + cubeName + ']')) :
                    query.split(from)[1].split(where)[0].contains('[' + cubeName + ']')
            if ('all'.equals(cubeName) || (condition)) {
                def time = System.currentTimeMillis()
                CellSet cellSet = getResultSet(connection, query);
                time = System.currentTimeMillis() - time
                println(query)
                println('time: ' + time)

            }
        } catch (Exception e){
            println('Ошибка парсинга файла отчета :' + file)
        }
    }
}

def getContainsParam(List<String> paramsList,String query){
    def result=null
    paramsList.each{
        param->
            if (query.contains(param)){
                result=param
            }
    }
    return result
}
OlapConnection getConnection(){
    OlapDataSource dataSource=olapDataProvider.getDataSource();
    return dataSource.getConnection();
}

CellSet getResultSet(OlapConnection connection,String query){
    MdxParserFactory parserFactory = connection.getParserFactory();
    MdxParser parser = parserFactory.createMdxParser(connection);
    CellSet cellSet=null
    OlapStatement statement = connection.createStatement();
    try {
        cellSet = statement.executeOlapQuery(query);
    } catch (def e){
        println('query parse error:'+query)
    }

    return cellSet;
}