// Получаем название орг. единицы
def tenant = securityDataProvider.loggedInUser.getTenant()

// Если у пользователя есть орг. единица, возвращаем id орг. единицы
if (tenant != null) {
    // Фильтруем список орг.ед(аттрибутов)
    def filter = FilterGroup.single('Sys_owner', FilterElement.FilterOperator.equals, Collections.singletonList(tenant))
    def data = externalDataSource.getObjectList('Tenant',null, filter, 0, null, null, null)

    // Получаем id орг.единицы по Sys_owner
    if (!data.isEmpty()) {
        def tenantFields = data.first()
        return Integer.parseInt(tenantFields.id)
    }
} else {
    return null
}
