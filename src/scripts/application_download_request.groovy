/**
 * application_download_request
 */

def objHist = createObjHist()
def procDef = workflowDataProvider.getProcessDefinitionByCodeActual('requestUnloadApplication')
applicationController.instance.dataObject.fields.requestUnloadHistory.value.add(objHist)
externalDataSource.updateObject(applicationController.instance.dataObject)
workflowDataProvider.startProcess(procDef, objHist)
/**
 автор (author): заполнять ФИО залогиненного пользователя
 логин (author_login): заполнять логином залогиненного пользователя (для дальнейшей выборки e-mail) для отправки письма)
 версия (versionApp): заполнять значением версии приложения, по которой сделан запрос;
 приложение (codeApp): заполнить значением кода приложения, по которому сделан запрос;
 дата (dateRequest): заполнять значением текущей даты и времени;
 Description (Description): заполнять значением <наименование приложения>. Версия <версия приложения>.
 */

def createObjHist(){
    def obj = externalDataSource.getNewObject('requestUnloadHistory')
    obj.fields.author.value = securityDataProvider.getLoggedInUser().toString()
    obj.fields.author_login.value = securityDataProvider.getLoggedInLoginName()
    obj.fields.versionApp.value = version.version
    obj.fields.codeApp.value = applicationController.instance.name
    obj.fields.dateRequest.value =  new Date()
    obj.fields.Description.value = applicationController.instance.label + '. Версия ' + obj.fields.versionApp.value
    externalDataSource.updateObject(obj)
    obj
}