/**
 * Отправить приглашение
 */

def obj = dataObjectController.instance
if (workflowDataProvider.getProcessesForObject(obj).findAll { it.completed == null }.size() >= 1)
    throw new Exception('Приглашение уже отправлено')
//Запускаем процесс
def procDef = workflowDataProvider.getProcessDefinitionByCodeActual('tehprisRSO_VideConferenceProcess')
workflowDataProvider.startProcess(procDef, obj)
messages.info('Приглашение отправлено')
