/**
 * Скрипт для обработки процесса авторизации пользователей через ЕСИА
 */

import org.apache.commons.lang3.RandomStringUtils
import org.picketlink.idm.credential.Password
import ru.osslabs.model.security.identity.agents.PlatformUser
import ru.osslabs.model.security.tenancy.Tenant
import ru.osslabs.security.services.esia.EsiaOAuth2Client
import ru.osslabs.security.services.esia.constant.Scope
import ru.osslabs.security.services.esia.endpoint.user.dto.UntrustedUserException
import ru.osslabs.security.services.esia.utils.EsiaUtils

//  Скрипт (настройки-скрипты) взаимодействия с ЕСИА. Код скрипта указать в параметре esia_process_user_login_script

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
println 'inside custom script'
try {
    //выполнение проверок.  В ЕСИА уже обратились (внутри платформы), получен токен, маркер, данные - в полученной от ЕСИА конструкции esiaConfig.
    def user = createOrUpdatePlatformUser(esiaConfig)
    //авторизация в системе после успешных проверок
    securityDataProvider.loginAs(user.loginName, false)
} catch (UntrustedUserException ex) {
    //Ошибки или отрицательный результат проверок, возвращает признак и сообщение для вывода текста в окне авторизации
    sessionManager.failed = true   //true - нужен, чтобы было выведено сообщение в окне авторизации
    sessionManager.lastMessage = ex.message
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
def createOrUpdatePlatformUser(esiaConfig) {
    //создание комплексного объекта на стороне платформы, через процедуру АПИ платформы (EsiaOAuth2Client)
    def client = new EsiaOAuth2Client(esiaConfig)
    //маркер доступа ЕСИА для последующегоо использования в обращениях к ЕСИА за какими-либо данными
    def accessMarker = esiaConfig.accessMarker
    //отдельная переменная с ID пользователя ЕСИА
    def userId = getUserId(accessMarker)
    //получение данных пользователя в переменную userData, структура как в таблице 6 МР2.18 ЕСИА.
    //  здесь и далее client.userCalls() - вызовы данных из ЕСИА, урлы формируютя по принципу, указанному в таблице №6 МР в.2.18 ЕСИА
    def userData = client.userCalls().getUserData(userId)
    //проверка наличия признака потвержденной учетной записи ЕСИА
    def trustedUser = userData.trusted
    if (!trustedUser) {
        //Отрицательный результат проверки подтвержденной записи ЕСИА (trusted=false), возврат сообщения о неуспешной авторизации
        def message = settingsDataProvider.getSetting('esia_nottrasted_msg', messages.get('untrusted_user'))
        throw new UntrustedUserException(message)
    }
    //набор скоупов ЕСИА, через запятую. Набор скоупов - см. в таблице 12 МР 2.18 ЕСИА.
    def scope = esiaConfig.scope
    //создан список контактов (пока пустой)
    def userContacts = []
    //  если в параметре scope есть класс Scope.USR_INF., то можно получать контактные данные дальше
    if (scope == Scope.USR_INF.toString()) {
        userContacts = getUserContacts(userId, client)
    }
    // обращения в ЕСИА завершены
    //
    // дальше работа с данными платформы
    // проверка наличия пользователя с userId в атрибуте externalId

    def user = findUser(userId)

    if (!user) {
        // нет пользователя, создаем (в памяти)
        user = new PlatformUser(userId)
        //присваем идентификатор ЕСИА пользователю
        user.externalId = userId

        //ищем организацию по умолчанию для пользователей ЕСИА
        //если не находим, то создаём её
        //присваиваем её пользователю
        /*def esiaOrg = getOrCreateEsiaOrg()
        if (esiaOrg) {
            user.tenant = esiaOrg.id
        }*/
        //процедура наполнения переменной непосредственно данными. Процедура кастомная, ниже.
        fillUserFields(user, userData, userContacts)
        //добавление пользователя в БД
        securityDataProvider.add(user)
        //генерируем случайный пароль
        def stringGenerator = new RandomStringUtils()
        def passwordString = stringGenerator.randomAlphanumeric(8)
        def password = new Password(passwordString)
        //присваимаем его пользователю
        securityDataProvider.updatePassword(user, password)
    } else {
        //пользователь есть
        if (!user.externalId) {
            user.externalId = userId
        }

        //процедура наполнения переменной непосредственно данными. Процедура кастомная, ниже.
        fillUserFields(user, userData, userContacts)
        //обновлени данных ползователя в его профиле в БД
        securityDataProvider.update(user)
        securityDataProvider.grantRole(userId,'declarant',false)
    }
    //возвращает текущего пользователя
    user
}
//////////////////////////////////////////////////////////////////////////////////
def findUser(userId) {

    //ищем пользователей с идентификатором ЕСИА в атрибуте externalId
    def filter = [:]
    def user = null
    filter.externalId = userId
    def users = securityDataProvider.getUsers(filter, null, null, null)
    if (users) {
        //если нашли, берём первого
        user = users[0]
    } else {
        // не нашли
        //ищем пользователя по логину равному идентификатору ЕСИА
        user = securityDataProvider.getUser(userId)
    }
    //возвращаем найденного пользователя
    user
}
//////////////////////////////////////////////////////////////////////////////////
def getUserContacts(userId, client) {
    //userCalls.getUserContactsInfo - зашитая процедура обращения к ЕСИА за контактными данными
    // сначала получаются из ЕСИА коды контактных данных
    def userContactsInfo = client.userCalls().getUserContactsInfo(userId)
    // затем по каждому коду дополнительными запросами в ЕСИА запрашиваются сами данные и это набор возвращается
    userContactsInfo.elements.collect { EsiaUtils.getIdFromElement(it) }
            .collect { elementId -> client.userCalls().getUserContact(userId, elementId) }
}
//////////////////////////////////////////////////////////////////////////////////
// получение ID пользователя ЕСИА
def getUserId(accessMarker) {
    EsiaUtils.getUserId(accessMarker.getAccessToken())
}
//////////////////////////////////////////////////////////////////////////////////
def fillUserFields(user, userData, userContacts) {
    if (userData) {
        user.firstName = userData.firstName
        user.middleName = userData.middleName
        user.lastName = userData.lastName
    }
    if (userContacts) {
        //перебор контактных данных и вытаскивание e-mail (здесь первый попавшийся), чтобы заполнить в отдельное поле профиля пользователя
        def email = userContacts.find { contact -> contact.type == UserContactType.EMAIL }
                .collect { it.value }
        if (email) {
            user.email = email
        }
    }
}
//////////////////////////////////////////////////////////////////////////////////
/*def getOrCreateEsiaOrg() {
    def orgCode = 'esia'
    //ищем организацю с кодом 'esia'
    def esiaOrg = securityDataProvider.getTenant(orgCode)
    if (!esiaOrg) {
        //не нашли - создаём
        esiaOrg = new Tenant()
        esiaOrg.id = orgCode
        esiaOrg.name = 'ЕСИА'
        //и сохраняем
        entityManager.persist(esiaOrg)
    }
    //возвращаем полученную организацию
    esiaOrg
}*/