import javax.servlet.http.HttpServletRequest;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;
import groovy.json.JsonSlurper;

HttpServletRequest servletRequest = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
def captcha = servletRequest.getParameter("g-recaptcha-response");

if (captcha) {
    // Делаем на верификацию полученного токена
    def urlGoogleApi = "https://www.google.com/recaptcha/api/siteverify";
    def secretKey = "6LdAnW8UAAAAACJY2_a-02Hgxj6q9ZrNXnE1cT30";
    def query = urlGoogleApi + '?secret=' + secretKey + '&response=' + captcha;
    def response = new URL(query).getText()
    // Проверяем на содержание ответа - success
    def jsonSlurper = new JsonSlurper()
    def object = jsonSlurper.parseText(response)
    if (object.containsKey('success')) {
        def obj = dataObjectController.instance
        externalDataSource.updateObject(obj)
        //Запускаем процесс
        def procDef = workflowDataProvider.getProcessDefinitionByCodeActual('tehprisRSO_ContactFormProcess')
        workflowDataProvider.startProcess(procDef, obj)
        messages.info('Сообщение отправлено')
    } else {
        messages.error('Извините но похоже вы робот')
    }
} else {
    messages.error('Вы не прошли валидацию reCaptcha')
}

