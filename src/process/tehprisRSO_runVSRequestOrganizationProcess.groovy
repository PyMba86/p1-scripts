def obj = dataObjectController.instance
if (workflowDataProvider.getProcessesForObject(obj).findAll{it.completed == null}.size() >= 1)
    throw new Exception('Процесс по заявке уже запущен')
//Запускаем процесс
def procDef = workflowDataProvider.getProcessDefinitionByCodeActual('Process_tehprisRSO_tehprisRSO_VSRequestOrganization')
workflowDataProvider.startProcess(procDef, obj)
messages.info('Процесс запущен')