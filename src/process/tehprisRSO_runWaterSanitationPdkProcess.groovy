def obj = dataObjectController.instance
if (workflowDataProvider.getProcessesForObject(obj).findAll{it.completed == null}.size() >= 1)
    throw new Exception('Процесс по заявке уже запущен')
//Запускаем процесс
def procDef = workflowDataProvider.getProcessDefinitionByCodeActual('process_tehprisRSO_WaterSanitationPdk')
workflowDataProvider.startProcess(procDef, obj)
messages.info('Процесс запущен')