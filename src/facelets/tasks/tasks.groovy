if (!securityDataProvider.hasPermission(ru.osslabs.model.security.SecurityDataProvider.AvailableResources.tasks.name(),ru.osslabs.model.security.SecurityDataProvider.AvailablePermissions.manage.name()))
    if (tasksSearch.getSystemFilter() == null)
        tasksSearch.setSystemFilter(ru.osslabs.model.common.filter.Filter.single(ru.osslabs.model.workflow.WorkflowConstants.TASK_LOGIN_NAME_ATTRIBUTE, FilterElement.FilterOperator.equals, Collections.singletonList(securityDataProvider.getLoggedInLoginName())));
