if(dashboardContext.context['typeField_list'] == null) {
    //typeField_list = externalDataSource.getObjectList("tehprisEE_voltageLevel", null)
    typeField_list = externalDataSource.getObjectList("RSO_TypeOfActivity", null)
    listTypeNew = []
    listTypeFiels = []
    typeField_list.each{
        listTypeFiels.add(it.id)
        listTypeNew.add([it.id,it.fields.Description.value])
    }
    dashboardContext.context['typeField_list'] = listTypeNew
    dashboardContext.context['typeField'] = listTypeFiels.toArray()
}
if (dashboardContext.context['typeField'] != null) {
    typeField = org.apache.commons.lang3.StringUtils.join(dashboardContext.context['typeField'],',')
}

if(dashboardContext.context['layerField_list'] == null) {
    listLayerNew = [
            //['tehprisEE_centrPit','Cлой 1'],
            //['tehprisEE_setOrg','Cлой 2'],
            //['RSO_mapOtopleniePoints','Слой точек подключения отопления'],
            ['RSO_OrganizationPolygonMain','Полигоны организаций'],
            ['RSO_OfficeOfOrganization','Офисы организаций'],
            ['RSO_Points','Точки подключения']
    ]
    listLayerFiels = [
            'RSO_OrganizationPolygonMain',
            'RSO_OfficeOfOrganization',
            'RSO_Points'
    ]
    dashboardContext.context['layerField_list'] = listLayerNew
    dashboardContext.context['layerField'] = listLayerFiels.toArray()
}

if (dashboardContext.context['layerField'] != null) {
    layerField = org.apache.commons.lang3.StringUtils.join(dashboardContext.context['layerField'],',')
}