import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.UploadedFile;

//  Массовая загрузка данных из Excel


public class FileUploadView {

    public String fileName;
    public String fileContent;

    public void handleUpload(FileUploadEvent event) {

        FacesMessage message = new FacesMessage("Succesful", event.getFile().getFileName() + " is uploaded.");
        FacesContext.getCurrentInstance().addMessage(null, message);

        UploadedFile file = event.getFile();
        byte[] contents = file.getContents();
        fileContent = new String(contents);
        fileName = file.getFileName();
    }

    String getFileName() {
        return fileName
    }

    String getFileContent() {
        return fileContent
    }
}

dashboardContext.context['FileUploadView'] = new FileUploadView()